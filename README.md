# Digiscope
By Team 34

The project is built using Gradle (or in this case a gradle wrapper to ensure that we all use
the same version of Gradle to build, launch and deploy the application)

## Launch application

### Option 1: Inside your IDE (with a gradle plugin)
Run Task 'launchDigiscope'

### Option 2: Using command line
1. cd into the root folder
2. ./gradlew launchDigiscope



## Deploy application

### Step 1: Generate a folder containing a .bat script and all the libraries
#### Option 1: Inside your IDE (with a gradle plugin)
Run Task 'installDist'

#### Option 2: Using command line
1. cd into the root folder
2. ./gradlew installDist

### Step 2: Convert .bat script into an executable .exe
1. Go into root folder -> build -> install -> digiscope -> bin
2. Convert digiscope.bat to an executable (for example using http://www.battoexeconverter.com)
3. Remove digiscope.sh (auto-generated) and digiscope.bat scripts from /bin folder

### Step 3: Create installer using NSIS 
1. Download and install NSIS (http://nsis.sourceforge.net/Download)
2. Copy the folder digiscope (root folder -> build -> install -> digiscope) to your desktop
3. Launch NSIS
4. Locate the .nsi script in the root folder installer-script.nsi
5. Run the script in NSIS

