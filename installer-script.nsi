; The name of the installer
Name "Digiscope Team 34"

; The file to write
OutFile "Digiscope_Team34_installer.exe"

; The default installation directory
InstallDir $PROGRAMFILES\Digiscope-Team34

; The text to prompt the user to enter a directory
DirText "Choose a directory"

;--------------------------------

Section ""

; Set output path to the installation directory.
SetOutPath $INSTDIR


; Add all files to the installation directory (contain bin and lib subdirectories)
File /r "digiscope\*"


; Create Menu directory and shortcut
CreateDirectory "$SMPROGRAMS\Digiscope Team 34"
CreateShortCut "$SMPROGRAMS\Digiscope Team 34\Run Digiscope.lnk" "$INSTDIR\bin\digiscope.exe"

; Create Desktop shortcut
CreateShortCut "$DESKTOP\Digiscope_Team34.lnk" "$INSTDIR\bin\digiscope.exe"


; Write uninstaller
WriteUninstaller $INSTDIR\bin\Uninstall.exe

; Create Menu shortcut to uninstall file
CreateShortCut "$SMPROGRAMS\Digiscope Team 34\Uninstall.lnk" "$INSTDIR\bin\Uninstall.exe"


SectionEnd ; end the section

; The uninstall section
Section "Uninstall"

Delete $INSTDIR\bin\Uninstall.exe
Delete $INSTDIR\bin\digiscope.exe
Delete $INSTDIR\lib\digiscope-digiscope-team34.jar
Delete $INSTDIR\lib\commons-math3-3.6.jar
Delete $INSTDIR\lib\commons-lang3-3.4.jar
Delete $INSTDIR\lib\jtransforms-2.4.0.jar
Delete $INSTDIR\lib\junit-4.8.2.jar

RMDir $INSTDIR\lib
RMDir $INSTDIR\bin
RMDir $INSTDIR

Delete "$DESKTOP\Digiscope_Team34.lnk"

Delete "$SMPROGRAMS\Digiscope Team 34\Run Digiscope.lnk"
Delete "$SMPROGRAMS\Disgiscope Team 34\Uninstall.lnk"
RMDIR "$SMPROGRAMS\Digiscope Team 34"

SectionEnd