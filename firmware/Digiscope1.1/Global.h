/*
 *    ======== Glboal.h ========
 *
 *	This file contains the global variables and type defs for the Digiscope
 *
 *  Created on: May 16, 2016
 *      Author: Jeff
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#define SPI_MSG_LENGTH    26

/*****************************************
 * 		Touchscreen Enums
 ****************************************/

//Enumeration for current PWM duty state (i.e. current brightness level)
typedef enum
	{
		DUTY0,
		DUTY25,
		DUTY50,
		DUTY75,
		DUTY100
	 } duty;


//Enumeration for current active screen
typedef enum
	{
		_main,
		_functionGen,
		_trigger
	} screen;


/*****************************************
 * 		Function Generator Enums
 ****************************************/

//Enumeration for function generator waveform
typedef enum
	{
		_sine,
		square,
		triangle,
		ramp,
		noise
	} functionWavetype;

//Enumeration for function generator peak to peak voltage
typedef enum
	{
		_100mVpp,
		_200mVpp,
		_500mVpp,
		_1Vpp,
		_2Vpp
	} functionPPV;

//Enumeration for function generator status. i.e. on or off
typedef enum
	{
		ON,
		OFF
	} functionStatus;


/*****************************************
 * 		Trigger Enums
 ****************************************/

//Enumeration for trigger state
typedef enum
	{
		armed,
		triggered,
		stop,
	} triggerState;

//Enumeration for trigger mode
typedef enum {
	  Auto,
	  Normal,
	  Single
	} trigger_mode;

//Enumeration for trigger type
typedef enum
	{
	  Rising,
	  Falling,
	  Level
	} trigger_type;


/*****************************************
 * 		Signal Enums
 ****************************************/

//Enumeration for channel
typedef enum {
		  CHA,
		  CHB
		} channel;

//Enumeration for coupling
typedef enum {
		  AC,
		  DC
		} couplingType;

//Enumeration for vertical divisions
typedef enum {
		  _20mV,
		  _50mV,
		  _100mV,
		  _200mV,
		  _500mV,
		  _1V
		} vertical_divisions;

//Enumeration for horizontal divisions
typedef enum {
		  _1us,
		  _2us,
		  _5us,
		  _10us,
		  _20us,
		  _50us,
		  _100us,
		  _200us,
		  _500us,
		  _1ms,
		  _2ms,
		  _5ms,
		  _10ms,
		  _20ms,
		  _50ms,
		  _100ms,
		  _200ms,
		  _500ms,
		  _1s
		} horizontal_division;

//Enumeration for sampling resolution
typedef enum {
		  _8_Bit,
		  _12_Bit
		} resolution;

/*****************************************
 * 		Change Setting Enum
 ****************************************/

//Enumeration for setting change table. i.e. if one of these enums is current, code will change the setting
typedef enum {
		  _device_status,		//0
		  _sampling_rate,		//1
		  _channel_offset,		//2
		  _vertical_range,		//3
		  _horizontal_range,	//4
		  _trigger_mode,		//5
		  _trigger_type,		//6
		  _trigger_threshold,	//7
		  _force_trigger,		//8
		  _rearm_trigger,		//9
		  _no_of_samples,		//10
		  _funcgen_output,		//11
		  _funcgen_waveform,	//12
		  _funcgen_p2p,			//13
		  _funcgen_offset,		//14
		  _funcgen_freq,		//15
		  _bit_mode,			//16
		  _channel_coupling,	//17
		  _idle					//18
		} setting;



/*****************************************
 * 		Global Variables
 ****************************************/

// Setting Global Variables
volatile setting changeSetting;			//current setting
volatile int changeSettingValue;		// setting value to change
volatile char changeSettingCommand;		// the setting type to change

//Touchscreen Global Variables
volatile screen currentScreen;			//current active screen

//Function Generator Variables
volatile functionWavetype currentWaveform;	//current function gen waveform
volatile functionPPV currentfunctionPPV;	//current function gen peak to peak voltage
volatile functionStatus currentfunctionStatus;	//current function gen status. i.e on or off
volatile int currentFrequency = 0;					//
volatile int currentVOffset= 0;


//Signal Global Variables
volatile channel currentChannel = CHA;			//current channel selected
volatile resolution currentResolution = _12_Bit;		//current ADC resolution
volatile vertical_divisions vDiv = _1V;			//current vertical divisions setting
volatile horizontal_division hDiv = _1ms;			//current horizontal divisions setting
volatile couplingType currentCoupling = AC;			//current coupling (DC/AC)
volatile int samplecount = 0;				//current sample count.  Initially set to 0
volatile int DCOffset = 0;					//current DC offset
volatile unsigned int numberOfSamples = 25000;		//current number of samples aquired

//Triggering Global Variables
volatile triggerState currentTriggerState = armed;			//current trigger state.  Default ARMED
volatile trigger_mode tMode = Auto;								//current trigger mode
volatile trigger_type tType = Rising;								//current trigger type
volatile int triggerThreshold = 3071;  //set default to be approxiately 3/4 of total voltage range

//Global Flag Variables
volatile int setting_change_flag = 0;				//Flag to signify that a setting has been changed
volatile int setting_sent_flag = 0;					//Flag to signify that a setting change has been sent
volatile int sample_ready_flag = 0;					//Flag to signify that a sampling sequence is ready
volatile int sample_sent_flag = 0;					//Flag to signify that a sample sequence has been sent
volatile int connected_flag = 0;					//Flag to signify that a connection to server has been made
volatile int start_SPI_flag = 0;					//Flag to signify that SPI transfer is ready to begin

//Global SPI Variables
extern char masterRxBuffer[SPI_MSG_LENGTH];
extern char masterTxBuffer[SPI_MSG_LENGTH];


//Global Semaphores
volatile Semaphore_Struct semStruct;
volatile Semaphore_Handle semHandle;

//Global Sample Storage
volatile uint16_t samples[25088] = {0};

uint32_t time;

//Brightness Global Variables
volatile int brightness = 5; //default max brightness on init
volatile int screen_on = 1;  //default on init
volatile int coupling_mode = 1;  //default on init
volatile duty dutyCycles;
volatile uint32_t Period, dutyCycle;

#endif /* GLOBAL_H_ */
