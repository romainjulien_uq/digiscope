/* This preamble is required by TI to use their functions.
 *
 *
 * Copyright (c) 2014-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    ======== touchscreen.c ========
 *
 *	This file contains the functions that handle the LCD and Touchscreen operations
 *
 *  	Created on: Apr 17, 2016
 *      Author: JRF
 */

#define pgm_read_byte(data) *data
#define pgm_read_word(data) *data

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "stdlib.h"
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
//#include "driverlib/sysctl.h"
//#include "driverlib/sysctl.c"
#include "driverlib/gpio.h"

/* Board Header file */
#include "Board.h"

/*Global Header file */
#include "Global.h"


//Pin Definitions				//Board Pinouts
#define DB0_PIN GPIO_PIN_1		// PF1
#define DB0_OFFSET 1

#define DB1_PIN GPIO_PIN_2		// PF2
#define DB1_OFFSET 2

#define DB2_PIN GPIO_PIN_3		// PF3
#define DB2_OFFSET 3

#define DB3_PIN GPIO_PIN_0		// PG0
#define DB3_OFFSET 0

#define DB4_PIN GPIO_PIN_4		// PL4
#define DB4_OFFSET 4

#define DB5_PIN GPIO_PIN_5		// PL5
#define DB5_OFFSET 5

#define DB6_PIN GPIO_PIN_0		// PL0
#define DB6_OFFSET 0

#define DB7_PIN GPIO_PIN_1		// PL1
#define DB7_OFFSET 1

#define DB8_PIN GPIO_PIN_2		// PL2
#define DB8_OFFSET 2

#define DB9_PIN GPIO_PIN_2		// PH2
#define DB9_OFFSET 2

#define DB10_PIN GPIO_PIN_3		// PH3
#define DB10_OFFSET 3

#define DB11_PIN GPIO_PIN_1		// PD1
#define DB11_OFFSET 1

#define DB12_PIN GPIO_PIN_0		// PD0
#define DB12_OFFSET 0

#define DB13_PIN GPIO_PIN_2		// PN2
#define DB13_OFFSET 2

#define DB14_PIN GPIO_PIN_3		// PN3
#define DB14_OFFSET 3

#define DB15_PIN GPIO_PIN_2		// PP2
#define DB15_OFFSET 2

#define RST_PIN GPIO_PIN_7			// PC7

#define CS_PIN GPIO_PIN_2			// PB2

#define RS_PIN GPIO_PIN_6			// PA6

#define WR_PIN GPIO_PIN_4			// PM4

#define RD_PIN GPIO_PIN_5			// PM5

#define T_CLK_PIN GPIO_PIN_4 		// PE4

#define T_CS_PIN GPIO_PIN_4 		// PC4

#define T_DIN_PIN GPIO_PIN_5 		// PC5

#define T_DOUT_PIN GPIO_PIN_5 		// PE5

#define T_IRQ_PIN GPIO_PIN_3 		// PB3

//PORT Definitions
#define DB0_PORT GPIO_PORTF_BASE		// PF1
#define DB1_PORT GPIO_PORTF_BASE		// PF2
#define DB2_PORT GPIO_PORTF_BASE		// PF3
#define DB3_PORT GPIO_PORTG_BASE		// PG0
#define DB4_PORT GPIO_PORTL_BASE		// PL4
#define DB5_PORT GPIO_PORTL_BASE		// PL5
#define DB6_PORT GPIO_PORTL_BASE		// PL0
#define DB7_PORT GPIO_PORTL_BASE		// PL1
#define DB8_PORT GPIO_PORTL_BASE		// PL2
#define DB9_PORT GPIO_PORTH_BASE		// PH2
#define DB10_PORT GPIO_PORTH_BASE		// PH3
#define DB11_PORT GPIO_PORTD_BASE		// PD1
#define DB12_PORT GPIO_PORTD_BASE		// PD0
#define DB13_PORT GPIO_PORTN_BASE		// PN2
#define DB14_PORT GPIO_PORTN_BASE		// PN3
#define DB15_PORT GPIO_PORTP_BASE		// PP2

#define RST_PORT GPIO_PORTC_BASE		// PC7
#define CS_PORT GPIO_PORTB_BASE			// PB2
#define RS_PORT GPIO_PORTA_BASE			// PA6
#define WR_PORT GPIO_PORTM_BASE			// PM4
#define RD_PORT GPIO_PORTM_BASE			// PM5

#define T_CLK_PORT GPIO_PORTE_BASE 		// PE4
#define T_CS_PORT GPIO_PORTC_BASE 		// PC4
#define T_DIN_PORT GPIO_PORTC_BASE 		// PC5
#define T_DOUT_PORT GPIO_PORTE_BASE 	// PE5
#define T_IRQ_PORT GPIO_PORTB_BASE 		// PB3

#define X_CONST 239
#define Y_CONST 319
#define PixSizeX	13.78 //size of x pixel on screen
#define PixOffsX	411	//x pixel offset

#define PixSizeY	11.01  //size of y pixel on the screen
#define PixOffsY	378    //y pixel offset


#define PREC_TOUCH_CONST 2000				//precision constant
#define PREC_TOUCH_CONST_BUTTON 3			//precusion constant button

// LCD color defines from UTFT Library
#define WHITE          0xFFFF
#define BLACK          0x0000
#define BLUE           0x001F
#define BLUE2          0x051F
#define RED            0xF800
#define MAGENTA        0xF81F
#define GREEN          0x07E0
#define CYAN           0x7FFF
#define YELLOW         0xFFE0

//Position defines from UTFT library
#define LEFT 0
#define RIGHT 9999
#define CENTER 9998
#define PORTRAIT 0    //orientation flags
#define LANDSCAPE 1

#define MAX_BUTTONS	20	// Maximum number of buttons available at one time

// Define presets for button status from UTFT library
#define BUTTON_DISABLED			0x0001
#define BUTTON_SYMBOL			0x0002
#define BUTTON_SYMBOL_REP_3X	0x0004
#define BUTTON_BITMAP			0x0008
#define BUTTON_NO_BORDER		0x0010
#define BUTTON_UNUSED			0x8000

/**********************************************
Val Zone
**********************************************/
int TP_X,TP_Y;    //touch position storage ints
unsigned int bit;	//bit shift variable


//font struct.  Adapted from UTFT library
typedef struct {
	uint8_t* font;
	uint8_t x_size;
	uint8_t y_size;
	uint8_t offset;
	uint8_t numchars;
}font;


//Button Struct.  Adapted from UTFT libary
typedef struct
{
  uint16_t			pos_x, pos_y, width, height;
  uint16_t			flags;
  char				*label;
  unsigned short	*data;
} button_type;

font currentFont;						// current font storage variable
button_type buttons[MAX_BUTTONS];		// button storage array
uint8_t orient = LANDSCAPE; 			// landscape orientation default
int	touchcount[MAX_BUTTONS];			// array to store the touch count for buttons

unsigned int forecolour = WHITE;  //default white
unsigned int backcolour = BLACK;  //default black

/* Button Variables */
int increaseBrightnessButton, decreaseBrightnessButton, powerButton;  // brightness control buttons
int backButton;  //button to return to main screen
int couplingButton, verticalRangeButton, horizontalRangeButton, voltageOffsetButton;  //Main Screen buttons
int triggerSettingsButton, triggerModeButton, triggerThresholdButton, triggerTypeButton, forceTriggerButton, rearmTriggerButton;   //Trigger Screen buttons
int functionGenButton, functionStatusButton, functionPPVButton, functionWaveformButton,  frequencyButton;  //Function Generator Buttons
int pressed_button;  //variable to store most recent pressed button
int btcnt = 0;  // number of active buttons

/*
 * External Variables
 */

// Fonts bit mapping
extern uint8_t Sinclair_S[];

// Images bit mapping - Refer associated *.c files (filename same as variable name)
extern unsigned short plus[];
extern unsigned short bright[];
extern unsigned short minus[];
extern unsigned short power[];
extern unsigned short coupling[];
extern unsigned short vrange[];
extern unsigned short hrange[];
extern unsigned short triggerthesh[];
extern unsigned short triggertype[];
extern unsigned short triggermode[];
extern unsigned short voffset[];
extern unsigned short wavetype[];
extern unsigned short funcppv[];
extern unsigned short funcOnOff[];
extern unsigned short frequency[];
extern unsigned short forcetrigger[];
extern unsigned short back[];
extern unsigned short functiongen[];
extern unsigned short rearmtrigger[];
extern unsigned short triggersettings[];


/**********************************************
Helper functions zone
**********************************************/


/*
 *  ======== delay ========
 *  Does nothing for the specified amount of cycles
 */
void delay(uint32_t cycles)
{
	unsigned long i;
	for(i=0; i < cycles; i++){
       // do nothing for some cycles
	}
}

/*
 *  ======== swap ========
 *  Performs memory swap of two unsigned integer inputs
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void swap(unsigned int*  x1, unsigned int* x2)
{
	unsigned int temp;
	temp = *x1;    //store x1 pointer in temp
	*x1 = *x2;		//store x2 in x1
	*x2 = temp;     //store x1 in X2
}

/**********************************************
LCD functions zone
**********************************************/


/*
 *  ======== lcdWritePins ========
 *  Sets data pins according to input data
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void lcdWritePins(unsigned int c)
{
	//write to data pins DB0 --> DB15
	GPIOPinWrite(DB0_PORT, DB0_PIN,  (((c >> 0 ) & 0x1) << DB0_OFFSET));
	GPIOPinWrite(DB1_PORT, DB1_PIN,  ((c >> 1 ) & 0x1) << DB1_OFFSET);
	GPIOPinWrite(DB2_PORT, DB2_PIN,  ((c >> 2 ) & 0x1) << DB2_OFFSET);
	GPIOPinWrite(DB3_PORT, DB3_PIN,  ((c >> 3 ) & 0x1) << DB3_OFFSET);
	GPIOPinWrite(DB4_PORT, DB4_PIN,  ((c >> 4 ) & 0x1) << DB4_OFFSET);
	GPIOPinWrite(DB5_PORT, DB5_PIN,  ((c >> 5 ) & 0x1) << DB5_OFFSET);
	GPIOPinWrite(DB6_PORT, DB6_PIN,  ((c >> 6 ) & 0x1) << DB6_OFFSET);
	GPIOPinWrite(DB7_PORT, DB7_PIN,  ((c >> 7 ) & 0x1) << DB7_OFFSET);
	GPIOPinWrite(DB8_PORT, DB8_PIN,  ((c >> 8 ) & 0x1) << DB8_OFFSET);
	GPIOPinWrite(DB9_PORT, DB9_PIN,  ((c >> 9 ) & 0x1) << DB9_OFFSET);
	GPIOPinWrite(DB10_PORT, DB10_PIN,  ((c >> 10 ) & 0x1) << DB10_OFFSET);
	GPIOPinWrite(DB11_PORT, DB11_PIN,  ((c >> 11 ) & 0x1) << DB11_OFFSET);
	GPIOPinWrite(DB12_PORT, DB12_PIN,  ((c >> 12 ) & 0x1) << DB12_OFFSET);
	GPIOPinWrite(DB13_PORT, DB13_PIN,  ((c >> 13 ) & 0x1) << DB13_OFFSET);
	GPIOPinWrite(DB14_PORT, DB14_PIN,  ((c >> 14 ) & 0x1) << DB14_OFFSET);
	GPIOPinWrite(DB15_PORT, DB15_PIN,  ((c >> 15 ) & 0x1) << DB15_OFFSET);
}


/*
 *  ======== writeCommand ========
 *  Sends command bytes to touchscreen
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void writeCommand(unsigned int c)
{
	GPIOPinWrite(CS_PORT, CS_PIN,0);
	GPIOPinWrite(RS_PORT, RS_PIN,0);

	lcdWritePins(c);

	GPIOPinWrite(WR_PORT, WR_PIN,0);
	GPIOPinWrite(WR_PORT, WR_PIN,WR_PIN);
	GPIOPinWrite(CS_PORT, CS_PIN,CS_PIN);
}


/*
 *  ======== writeData ========
 *  Sends data bytes to touchscreen
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void writeData(unsigned int c)
{
	GPIOPinWrite(CS_PORT, CS_PIN,0);
	GPIOPinWrite(RS_PORT, RS_PIN,RS_PIN);

	lcdWritePins(c);

	GPIOPinWrite(WR_PORT, WR_PIN,0);
	GPIOPinWrite(WR_PORT, WR_PIN,WR_PIN);
	GPIOPinWrite(CS_PORT, CS_PIN,CS_PIN);
}


/*
 *  ======== writeCommandData ========
 *  Sends command and data bytes to touchscreen
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void writeCommandData(unsigned int cmd,unsigned int dat)
{
	writeCommand(cmd);
	writeData(dat);
}


/*
 *  ======== lcdInit ========
 *  Sends command and data bytes to touchscreen for initial configuration
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void lcdInit()
{
    System_printf("Entering Initalisation\n");
    /* SysMin will only print to the console when you call flush or exit */
    System_flush();

    //CONTROL PINS
    GPIOPinTypeGPIOOutput(RS_PORT, RS_PIN);
    GPIOPinTypeGPIOOutput(WR_PORT, WR_PIN);
    GPIOPinTypeGPIOOutput(CS_PORT, CS_PIN);
    GPIOPinTypeGPIOOutput(RST_PORT, RST_PIN);
    GPIOPinTypeGPIOOutput(RD_PORT, RD_PIN);

    //DATA PINS
    GPIOPinTypeGPIOOutput(DB0_PORT, DB0_PIN);
    GPIOPinTypeGPIOOutput(DB1_PORT, DB1_PIN);
    GPIOPinTypeGPIOOutput(DB2_PORT, DB2_PIN);
    GPIOPinTypeGPIOOutput(DB3_PORT, DB3_PIN);
    GPIOPinTypeGPIOOutput(DB4_PORT, DB4_PIN);
    GPIOPinTypeGPIOOutput(DB5_PORT, DB5_PIN);
    GPIOPinTypeGPIOOutput(DB6_PORT, DB6_PIN);
    GPIOPinTypeGPIOOutput(DB7_PORT, DB7_PIN);
    GPIOPinTypeGPIOOutput(DB8_PORT, DB8_PIN);
    GPIOPinTypeGPIOOutput(DB9_PORT, DB9_PIN);
    GPIOPinTypeGPIOOutput(DB10_PORT, DB10_PIN);
    GPIOPinTypeGPIOOutput(DB11_PORT, DB11_PIN);
    GPIOPinTypeGPIOOutput(DB12_PORT, DB12_PIN);
    GPIOPinTypeGPIOOutput(DB13_PORT, DB13_PIN);
    GPIOPinTypeGPIOOutput(DB14_PORT, DB14_PIN);
    GPIOPinTypeGPIOOutput(DB15_PORT, DB15_PIN);

    GPIOPinWrite(RD_PORT, RD_PIN, RD_PIN);

    GPIOPinWrite(RST_PORT, RST_PIN, RST_PIN);
	delay(160000);  // ~1 ms
	GPIOPinWrite(RST_PORT, RST_PIN, 0);
	delay(16000000); // ~100 ms

	GPIOPinWrite(RST_PORT, RST_PIN, RST_PIN);
	GPIOPinWrite(CS_PORT, CS_PIN, CS_PIN);
	GPIOPinWrite(WR_PORT, WR_PIN, WR_PIN);
	delay(32000000); // ~200 ms



	writeCommandData(0x0000,0x0001);  // set oscillator on The oscillator will be turned on when OSCEN = 1, off when OSCEN = 0.
	writeCommandData(0x0003,0xA8A4);  // Power control(1) : fosc / 4, VCIX2 x 2 + VCI , fosc / 4 , Small to medium (OP-AMP Power)
	writeCommandData(0x000C,0x0000);	// Power control(2) : VCIX2 = 5.1V
	writeCommandData(0x000D,0x080C);  // Power control(3) : VLCD63 = Vref x 2.500
	writeCommandData(0x000E,0x2B00);	// Power control(4) : VcomL GPIO_CFG_OUTPUT is fixed to Hi-z level, VCIM GPIO_CFG_OUTPUT for VcomL power supply stops, and the instruction (VDV4-0) becomes unavailable
	writeCommandData(0x001E,0x00B7);	// Power control(5) : VCM[5:0] on , VcomH = VLCD63 x 0.42
	writeCommandData(0x0001,0x2B3F);	// Driver GPIO_CFG_OUTPUT control : S719 shifts to S0, reversal, Cs on common, <B><G><R> color is assigned from S0, G0 shifts to G319, 127 lines
	writeCommandData(0x0002,0x0600);  // LCD drive AC control/LCD-Driving-Waveform Control :
	writeCommandData(0x0010,0x0000);  // Sleep mode : Off
	writeCommandData(0x0011,0x6070);	// Entry mode :
	writeCommandData(0x0005,0x0000);	// Compare register (1)
	writeCommandData(0x0006,0x0000);	// Compare register (2)
	writeCommandData(0x0016,0xEF1C);	// Horizontal porch
	writeCommandData(0x0017,0x0003);	// Vertical porch
	writeCommandData(0x0007,0x0233);	// Display control
	writeCommandData(0x000B,0x0000);	// Frame cycle control
	writeCommandData(0x000F,0x0000);	// Gate scan start position
	writeCommandData(0x0041,0x0000);	// Vertical scroll control (1)
	writeCommandData(0x0042,0x0000);	// Vertical scroll control (2)
	writeCommandData(0x0048,0x0000);	// First window start
	writeCommandData(0x0049,0x013F);	// First window end
	writeCommandData(0x004A,0x0000);	// Second window start
	writeCommandData(0x004B,0x0000);	// Second window end
	writeCommandData(0x0044,0xEF00);	// Horizontal RAM address position
	writeCommandData(0x0045,0x0000);  // Vertical RAM address start position
	writeCommandData(0x0046,0x013F);	// Vertical RAM address end position
	writeCommandData(0x0030,0x0707);	// Gamma control (1)
	writeCommandData(0x0031,0x0204);	// Gamma control (2)
	writeCommandData(0x0032,0x0204);	// Gamma control (3)
	writeCommandData(0x0033,0x0502);	// Gamma control (4)
	writeCommandData(0x0034,0x0507);	// Gamma control (5)
	writeCommandData(0x0035,0x0204);	// Gamma control (6)
	writeCommandData(0x0036,0x0204);	// Gamma control (7)
	writeCommandData(0x0037,0x0502);	// Gamma control (8)
	writeCommandData(0x003A,0x0302);	// Gamma control (9)
	writeCommandData(0x003B,0x0302);	// Gamma control (10)
	writeCommandData(0x0023,0x0000);	// RAM write data mask (1)
	writeCommandData(0x0024,0x0000);	// RAM write data mask (2)
	//writeCommandData(0x0025,0x8000);	// This does nothing
	writeCommandData(0x004f,0x0000);	// Set GDDRAM Y address counter
	writeCommandData(0x004e,0x0000);	// Set GDDRAM X address counter
	writeCommand(0x0022);	// RAM Data read/write?

	System_printf("Setup Complete\n");
	    /* SysMin will only print to the console when you call flush or exit */
	    System_flush();

}

/*
 *  ======== setXY ========
 *  Sets the X and Y boundaries for screen scanning
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void setXY(unsigned int x0,unsigned int y0,unsigned int x1,unsigned int y1)
{
	if (orient==LANDSCAPE)
	{
		swap(&x0, &y0);
		swap(&x1, &y1);
		y0=Y_CONST-y0;
		y1=Y_CONST-y1;
		swap(&y0, &y1);
	}

	writeCommandData(0x0044,(x1<<8)+x0); 	// Horizontal RAM address position. i.e. end of X rectangle
	writeCommandData(0x0045,y0);			// Vertical RAM address start position i.e start of Y rectangle
	writeCommandData(0x0046,y1);			// Vertical RAM address end position i.e end of Y rectangle
	writeCommandData(0x004e,x0);			// Set GDDRAM X address counter i.e. start at xo
	writeCommandData(0x004f,y0);			// Set GDDRAM Y address counter i.e. starrt at y0
	writeCommand (0x0022);					// RAM Data read/write;
}

/*
 *  ======== clearXY ========
 *  Resets the array addresses for X and Y
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void clearXY()
{
	if (orient==PORTRAIT)
		setXY(0, 0, X_CONST, Y_CONST);    //reset XY position vectors
	else
		setXY(0, 0, Y_CONST, X_CONST);   //reset XY position vectors
}

/*
 *  ======== paintScreen ========
 *  Paints the entire screen according to input colour
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void paintScreen(unsigned int color){
	int i,j;
	clearXY();

    for(i=0;i<320;i++){

	  for (j=0;j<240;j++){

         writeData(color);   //iterate through all X-Y pixels and paint specificed colour

	  }
	}
}

/*
 *  ======== paintRegion ========
 *  Paints the a specified screen according to input colour and region boundaries
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void paintRegion(unsigned int  x1, unsigned int y1, unsigned int  x2, unsigned int y2, unsigned int color)
{
	int i,j;
	setXY(x1,y1,x2,y2);

    for(i=x1;i<x2+1;i++)
	 {
	  for (j=y1;j<y2+1;j++)
	   	{
         writeData(color);   //iterate through selected X-Y pixel ranges and paint specificed colour
	    }

	  }
}


/*
 *  ======== lcdClear ========
 *  Clears the LCD (i.e. it sees a red door and paints it black)
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void lcdClear()
{
    unsigned int i,j;
    clearXY();
	for(i=0;i<320;i++)
	{
	    for(j=0;j<240;j++)
		{
          	writeData(BLACK);  // black is default clear colour.  Iterate through all and paint black
		}
	}

}

/**********************************************
Touchscreen functions zone
**********************************************/


/*
 *  ======== touchInit ========
 *  Sends command and data bytes to touchscreen for initial configuration for touch operation
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void touchInit(void)
{

	GPIOPinTypeGPIOOutput(T_CLK_PORT, T_CLK_PIN);
	GPIOPinTypeGPIOOutput(T_CS_PORT, T_CS_PIN);
	GPIOPinTypeGPIOOutput(T_DIN_PORT, T_DIN_PIN);
	GPIOPinTypeGPIOInput(T_DOUT_PORT, T_DOUT_PIN);
	GPIOPinTypeGPIOInput(T_IRQ_PORT, T_IRQ_PIN);


	GPIOPinWrite(T_CS_PORT, T_CS_PIN, T_CS_PIN);
	GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, T_CLK_PIN);
	GPIOPinWrite(T_DIN_PORT, T_DIN_PIN, T_DIN_PIN);
	GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, T_CLK_PIN);
}


/*
 *  ======== touchWriteData ========
 *  Writes data to touch input pins
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void touchWriteData(unsigned char data){
	unsigned char temp;
	unsigned char nop;
	unsigned char count;

	temp=data;
	GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, 0);  //set T_CLK pin to 0

	for(count=0; count<8; count++)	{
		if(temp & 0x80)
			GPIOPinWrite(T_DIN_PORT, T_DIN_PIN, T_DIN_PIN);
		else
			GPIOPinWrite(T_DIN_PORT, T_DIN_PIN, 0);
		temp = temp << 1;
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, 0);  //set T_CLK pin to 0
		nop++;
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, T_CLK_PIN);  //set T_CLK pin to 1
		nop++;
	}
}


/*
 *  ======== touchReadData ========
 *  Reads data to touch output pins
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
unsigned int touchReadData()
{
	unsigned char nop;
	unsigned int data = 0;
	unsigned char count;
	for(count=0; count<12; count++)
	{
		data <<= 1;
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, T_CLK_PIN);
		nop++;
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, 0);  //set T_CLK pin to 0
		nop++;
		if (GPIOPinRead(T_DOUT_PORT, T_DOUT_PIN))
			data++;
	}
	return(data);
}

/*
 *  ======== touchRead ========
 *  Reads X and Y data from touchscreen
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
void touchRead()
{
	unsigned long tx=0;
	unsigned long ty=0;
	unsigned int i = 0;

	GPIOPinWrite(T_CS_PORT, T_CS_PIN, 0);

	for (i=0; i < PREC_TOUCH_CONST; i++)
	{
		touchWriteData(0x90);
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, T_CLK_PIN);
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, 0);  //set T_CLK pin to 0
		ty+=touchReadData();

		touchWriteData(0xD0);
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, T_CLK_PIN);
		GPIOPinWrite(T_CLK_PORT, T_CLK_PIN, 0);  //set T_CLK pin to 0
		tx+=touchReadData();
	}

	GPIOPinWrite(T_CS_PORT, T_CS_PIN, T_CS_PIN);
//	if(orient == PORTRAIT){
		TP_X=tx/PREC_TOUCH_CONST;
		TP_Y=ty/PREC_TOUCH_CONST;
//	} else {
//		TP_X=ty/PREC_TOUCH_CONST;
//		TP_Y=tx/PREC_TOUCH_CONST;
//	}
}


/*
 *  ======== touchDataAvailable ========
 *  Checks to see if touch interrupt has triggered
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
unsigned int touchDataAvailable()
{
  unsigned int avail;
  avail = !GPIOPinRead(T_IRQ_PORT, T_IRQ_PIN);
  return avail;
}


/*
 *  ======== touchGetX ========
 *  Converts touch X data read into pixel coordinate
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
int touchGetX()
{
	int value;
	if(orient == PORTRAIT){
		value = ((TP_X-PixOffsX)/PixSizeX);   	//convert reading to pixel coordinate
	} else {
		value = ((TP_Y-PixOffsY)/PixSizeY); 	//convert reading to pixel coordinate
	}
	if (value < 0)
		value = 0;
	return value;
}


/*
 *  ======== touchGetY ========
 *  Converts touch Y data read into pixel coordinate
 *
 *
 *  Adapted from demonstration code provided by ILead
 *  http://wiki.iteadstudio.com/ITDB02-3.2S_Arduino_Shield_V2
 */
int touchGetY()
{
	int value;
	if(orient == PORTRAIT){
		value = ((TP_Y-PixOffsY)/PixSizeY); //convert reading to pixel coordinate
	} else {
		value = ((TP_X-PixOffsX)/PixSizeX); //convert reading to pixel coordinate
	}
	if (value < 0)
		value = 0;
	return value;
}


/**********************************************
Bitmap functions zone
**********************************************/


/*
 *  ======== drawBitmap ========
 *  Draws a bitmap stored in data on the touchscreen
 *
 *  x and y are the top left coordinates
 *  sx and sy are bottom right coordinates
 *  scale is for scale ***doesn't quite work***
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void drawBitmap(int x, int y, int sx, int sy, unsigned short* data, int scale)
{
	unsigned int col;
	int tx, ty, tc, tsx, tsy;

	if (scale==1) {
		if (orient==PORTRAIT) {
			GPIOPinWrite(CS_PORT, CS_PIN, 0);  //set CS to low
			setXY(x, y, x+sx-1, y+sy-1);
			for (tc=0; tc<(sx*sy); tc++){
				col=pgm_read_word(&data[tc]);    //read from progmem
				writeData(col);
			}
			GPIOPinWrite(T_CS_PORT, T_CS_PIN, T_CS_PIN);
		} else {
			GPIOPinWrite(CS_PORT, CS_PIN, 0);  //set CS to low
			for (ty=0; ty<sy; ty++){

				setXY(x, y+ty, x+sx-1, y+ty);
				for (tx=sx-1; tx>=0; tx--){
					col=pgm_read_word(&data[(ty*sx)+tx]); //read from progmem
					writeData(col);
				}
			}
			GPIOPinWrite(T_CS_PORT, T_CS_PIN, T_CS_PIN);
		}
	} else {
		if (orient==PORTRAIT){
			GPIOPinWrite(CS_PORT, CS_PIN, 0);  //set CS to low
			for (ty=0; ty<sy; ty++)	{
				setXY(x, y+(ty*scale), x+((sx*scale)-1), y+(ty*scale)+scale);
				for (tsy=0; tsy<scale; tsy++)
					for (tx=0; tx<sx; tx++) {
						col=pgm_read_word(&data[(ty*sx)+tx]);
						for (tsx=0; tsx<scale; tsx++)
							writeData(col);
					}
			}
			GPIOPinWrite(T_CS_PORT, T_CS_PIN, T_CS_PIN);
		}
		else
		{
			GPIOPinWrite(CS_PORT, CS_PIN, 0);  //set CS to low
			for (ty=0; ty<sy; ty++)	{
				for (tsy=0; tsy<scale; tsy++){
					setXY(x, y+(ty*scale)+tsy, x+((sx*scale)-1), y+(ty*scale)+tsy);
					for (tx=sx-1; tx>=0; tx--)	{
						col=pgm_read_word(&data[(ty*sx)+tx]);
						for (tsx=0; tsx<scale; tsx++)
							writeData(col);
					}
				}
			}
			GPIOPinWrite(T_CS_PORT, T_CS_PIN, T_CS_PIN);
		}
	}
	clearXY();
}




/**********************************************
Shape functions zone
**********************************************/

/*
 *  ======== drawHorizontalLine ========
 *  Draws a horizontal line on the screen
 *
 *  x and y are the start coordinates, l is the length
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void drawHorizontalLine(unsigned int  x, unsigned int y, unsigned int l)
{
	int i;

	setXY(x, y, x+l, y);

    for(i=0;i<l+1;i++) {
    	writeData(forecolour);     //as per other paint functions, draw pixels as per iteration
	 }
    clearXY();
}

/*
 *  ======== drawVerticalLine ========
 *  Draws a vertical line on the screen
 *
 *  x and y are the start coordinates, l is the length
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void drawVerticalLine(unsigned int  x, unsigned int y, unsigned int l)
{
	int i;

	setXY(x, y, x, y+l);

    for(i=0;i<l+1;i++) {
    	writeData(forecolour); //as per other paint functions, draw pixels as per iteration
	 }
    clearXY();
}


/*
 *  ======== drawRectangle ========
 *  Draws a rectangle on the screen
 *
 *  x1 and y2 are the top left coordinates, x1 and y2 are the bottom right coordinates,
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void drawRectangle(unsigned int  x1, unsigned int y1, unsigned int  x2, unsigned int y2)
{
	if (x1>x2)
	{
		swap(&x1, &x2);
	}
	if (y1>y2)
	{
		swap(&y1, &y2);
	}

	//Draw rectangle
	drawHorizontalLine(x1, y1, x2-x1);
	drawHorizontalLine(x1, y2, x2-x1);
	drawVerticalLine(x1, y1, y2-y1);
	drawVerticalLine(x2, y1, y2-y1);

}

/**********************************************
Text functions zone
**********************************************/

/*
 *  ======== printChar ========
 *  Draws a character on the screen
 *
 *  x1 and y2 are the start coordinates, c is character to be printed
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void printChar(char c, int x, int y)
{
	char i,ch;
	unsigned j;
	int k;
	unsigned temp;

	GPIOPinWrite(CS_PORT, CS_PIN, 0);  //set CS to low

	if (orient==PORTRAIT){

		setXY(x,y,x+currentFont.x_size-1,y+currentFont.y_size-1);

		temp=((c-currentFont.offset)*((currentFont.x_size/8)*currentFont.y_size))+4;
		for(j=0;j<((currentFont.x_size/8)*currentFont.y_size);j++)	{
			ch=pgm_read_byte(&currentFont.font[temp]);
			for(i=0;i<8;i++)
			{
				if((ch&(1<<(7-i)))!=0){
					writeData(forecolour);
				} else 	{
					writeData(backcolour);
				}
			}
			temp++;
		}
	}
	else
	{
		temp=((c-currentFont.offset)*((currentFont.x_size/8)*currentFont.y_size))+4;
		for(j=0;j<((currentFont.x_size/8)*currentFont.y_size);j+=(currentFont.x_size/8)){

			setXY(x,y+(j/(currentFont.x_size/8)),x+currentFont.x_size-1,y+(j/(currentFont.x_size/8)));

			for (k=(currentFont.x_size/8)-1; k>=0; k--)	{

				ch=pgm_read_byte(&currentFont.font[temp+k]);

				for(i=0;i<8;i++) {

					if((ch&(1<<i))!=0)	{
						writeData(forecolour);
					} else {
						writeData(backcolour);
					}
				}
			}
			temp+=(currentFont.x_size/8);
		}
	}

	GPIOPinWrite(CS_PORT, CS_PIN, CS_PIN);  //set CS to low
	clearXY();
}

/*
 *  ======== print ========
 *  Draws a character string on the screen
 *
 *  x1 and y2 are the start coordinates, st is string address to be printed
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void print(char *st, int x, int y)
{
	int stl, i;

	stl = strlen(st);

	if (orient==PORTRAIT) {
		if (x==RIGHT)
			x=(X_CONST+1)-(stl*currentFont.x_size);
		if (x==CENTER)
			x=((X_CONST+1)-(stl*currentFont.x_size))/2;
	} else {
		if (x==RIGHT)
			x=(Y_CONST+1)-(stl*currentFont.x_size);
		if (x==CENTER)
			x=((Y_CONST+1)-(stl*currentFont.x_size))/2;
	}

	for (i=0; i<stl; i++)
		printChar(*st++, x + (i*(currentFont.x_size)), y);

}

/*
 *  ======== setFont ========
 *  Defines the global text font
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void setFont(uint8_t* font) {
	currentFont.font=font;
	currentFont.x_size=pgm_read_byte(&currentFont.font[0]);
	currentFont.y_size=pgm_read_byte(&currentFont.font[1]);
	currentFont.offset=pgm_read_byte(&currentFont.font[2]);
	currentFont.numchars=pgm_read_byte(&currentFont.font[3]);
}

/*
 *  ======== getFont ========
 *  Returns the global font
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
uint8_t* getFont() {
	return currentFont.font;
}


/*
 *  ======== getFontXsize ========
 *  Returns the global font x size
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
uint8_t getFontXsize() {
	return currentFont.x_size;
}

/*
 *  ======== getFontYsize ========
 *  Returns the global font y size
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
uint8_t getFontYsize() {
	return currentFont.y_size;
}

/*
 *  ======== printChar ========
 *  Draws a character on the screen
 *
 *  x1 and y2 are the start coordinates, c is character to be printed
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void updateStatus() {
	char str[18];
	if(connected_flag == 1){  //if connected display IP address
		print("Connected: 192.168.137.8", 40, 15);
	} else {
		paintRegion(40, 10, 235,  30, BLACK);
	}

	switch(currentScreen){
		case _main: {

			//Update coupling
			switch (currentCoupling){
				case DC:  	print("Coupling Mode: DC", 130, 50); break;
				case AC:	print("Coupling Mode: AC", 130, 50); break;

				default:
					break;
			}
			//Update vertical divisions
			switch (vDiv){
				case _20mV:  print("Vert. Range: 20mV/div ", 130, 80); break;
				case _50mV:	 print("Vert. Range: 50mV/div ", 130, 80); break;
				case _100mV: print("Vert. Range: 100mV/div", 130, 80); break;
				case _200mV: print("Vert. Range: 200mV/div", 130, 80); break;
				case _500mV: print("Vert. Range: 500mV/div", 130, 80); break;
				case _1V: 	 print("Vert. Range: 1V/div   ", 130, 80); break;
				default:
					break;
			}
			//Update horizontal divisions
			switch (hDiv){
				case _1us: 		print("Hori. Range: 1us/div  ", 130, 110); break;
				case _2us:		print("Hori. Range: 2us/div  ", 130, 110); break;
				case _5us: 		print("Hori. Range: 5us/div  ", 130, 110); break;
				case _10us: 	print("Hori. Range: 10us/div ", 130, 110); break;
				case _20us: 	print("Hori. Range: 20us/div ", 130, 110); break;
				case _50us: 	print("Hori. Range: 50us/div ", 130, 110); break;
				case _100us: 	print("Hori. Range: 100us/div", 130, 110); break;
				case _200us: 	print("Hori. Range: 200us/div", 130, 110); break;
				case _500us: 	print("Hori. Range: 500us/div", 130, 110); break;
				case _1ms: 		print("Hori. Range: 1ms/div  ", 130, 110); break;
				case _2ms: 		print("Hori. Range: 2ms/div  ", 130, 110); break;
				case _5ms: 		print("Hori. Range: 5ms/div  ", 130, 110); break;
				case _10ms: 	print("Hori. Range: 10ms/div ", 130, 110); break;
				case _20ms:	 	print("Hori. Range: 20ms/div ", 130, 110); break;
				case _50ms: 	print("Hori. Range: 50ms/div ", 130, 110); break;
				case _100ms: 	print("Hori. Range: 100ms/div", 130, 110); break;
				case _200ms: 	print("Hori. Range: 200ms/div", 130, 110); break;
				case _500ms: 	print("Hori. Range: 500ms/div", 130, 110); break;
				case _1s:  		print("Hori. Range: 1s/div   ", 130, 110); break;
				default:
					break;
			}
			//Update function generator status
			switch (currentfunctionStatus){
				case ON: 		print("Function Generator: On ", 130, 200); break;
				case OFF:  		print("Function Generator: Off", 130, 200); break;
				default:
					break;
			}
			//Update horizontal divisions
			switch (currentTriggerState){
				case armed: 		print("Trigger State: Armed    ", 130, 150); break;
				case triggered:  	print("Trigger State: Trig'd   ", 130, 150); break;
				case stop:  		print("Trigger State: Stopped  ", 130, 150); break;
				default:
					break;
			}
			break;
		}

		case _functionGen: {

			switch (currentfunctionStatus){
				case ON:  	print("Function Generator: On ", 130, 50); break;
				case OFF: 	print("Function Generator: Off", 130, 50); break;

				default:
					break;
			}

			switch (currentWaveform){
				case _sine:  	print("Waveform: Sine        ", 130, 110); break;
				case square:	print("Waveform: Square      ", 130, 110); break;
				case triangle: 	print("Waveform: Triangle    ", 130, 110); break;
				case ramp: 		print("Waveform: Ramp        ", 130, 110); break;
				case noise: 	print("Waveform: Noise       ", 130, 110); break;

				default:
					break;
			}

			switch (currentfunctionPPV){
				case _100mVpp: 	print("Voltage P-P: 100mVpp", 130, 80); break;
				case _200mVpp: 	print("Voltage P-P: 200mVpp", 130, 80); break;
				case _500mVpp:	print("Voltage P-P: 500mVpp", 130, 80); break;
				case _1Vpp: 	print("Voltage P-P: 1mVpp  ", 130, 80); break;
				case _2Vpp: 	print("Voltage P-P: 2Vpp   ", 130, 80); break;

				default:
					break;
			}
			snprintf(str, 18, "Freq. (Hz): %d", currentFrequency);
			print(str, 130, 170);
			snprintf(str, 18, "Volt. Off:  %d", currentVOffset);
			print(str, 130, 140);
			break;
		}

			case _trigger: {

				switch (tMode){
					case Auto: 	 print("Trig. Mode: Auto  ", 130, 50);  break;
					case Normal: print("Trig. Mode: Normal", 130, 50);  break;
					case Single: print("Trig. Mode: Single", 130, 50);  break;
					default:
						break;
				}

				switch (tType){
					case Rising: 	print("Trig. Type.: Rising ", 130, 80);  break;
					case Falling: 	print("Trig. Type.: Falling", 130, 80);  break;
					case Level: 	print("Trig. Type.: Level  ", 130, 80);  break;
					default:
						break;
				}

				switch (currentTriggerState){
					case armed: 		print("Trigger State: Armed    ", 130, 150); break;
					case triggered:  	print("Trigger State: Trig'd   ", 130, 150); break;
					case stop:  		print("Trigger State: Stopped  ", 130, 150); break;
					default:
						break;
				}

				snprintf(str, 20, "Trig. Thresh.: %4u", triggerThreshold);
			    print(str, 130, 110);
			    break;
		}

		default:
			break;
	}

}


/**********************************************
Button functions zone
**********************************************/



/*
 *  ======== addButton ========
 *  creates a button object at the specified location
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
int addButton(uint16_t x, uint16_t y, uint16_t width, uint16_t height, unsigned short *data, uint16_t flags)
{
	while (((buttons[btcnt].flags & BUTTON_UNUSED) == 0) && (btcnt<MAX_BUTTONS))
		btcnt++;

	if (btcnt == MAX_BUTTONS)
		return -1;
	else
	{
		buttons[btcnt].pos_x  = x;
		buttons[btcnt].pos_y  = y;
		buttons[btcnt].width  = width;
		buttons[btcnt].height = height;
		buttons[btcnt].flags  = flags | BUTTON_BITMAP;
		buttons[btcnt].label  = NULL;
		buttons[btcnt].data   = data;
		return btcnt;
	}
}


/*
 *  ======== deleteAllButtons ========
 *  deletes all button objects and sets button array structs to zero
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void deleteAllButtons()
{
	int i;
	btcnt = 0;

	increaseBrightnessButton = 0;  //reset button values to avoid conflicts on screen changes
	decreaseBrightnessButton = 0;
	powerButton = 0;
	couplingButton = 0;
	verticalRangeButton = 0;
	horizontalRangeButton = 0;
	triggerSettingsButton = 0;
	functionGenButton = 0;
	triggerTypeButton = 0;
	triggerModeButton = 0;
	triggerThresholdButton = 0;
	forceTriggerButton  = 0;
	rearmTriggerButton  = 0;
	functionStatusButton = 0;
	functionPPVButton = 0;
	functionWaveformButton = 0;
	voltageOffsetButton = 0;
	frequencyButton = 0;
	backButton = 0;

	for (i=0;i<MAX_BUTTONS;i++)
	{
		buttons[i].pos_x=0;
		buttons[i].pos_y=0;
		buttons[i].width=0;
		buttons[i].height=0;
		buttons[i].flags=BUTTON_UNUSED;
		buttons[i].label="";
	}
}


/*
 *  ======== drawButton ========
 *  Draws the specified button object on the screen
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void drawButton(int buttonID)
{
	if (buttons[buttonID].flags & BUTTON_BITMAP)
	{
		drawBitmap(buttons[buttonID].pos_x, buttons[buttonID].pos_y, buttons[buttonID].width, buttons[buttonID].height, buttons[buttonID].data, 1);
	}

}


/*
 *  ======== drawButtons ========
 *  Draws the all button objects on the screen
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
void drawButtons()
{
	int i;
	for (i=0;i<btcnt+1;i++)
	{
		touchcount[i] = 0;
		if ((buttons[i].flags & BUTTON_UNUSED) == 0)
			drawButton(i);
	}
}


/*
 *  ======== checkButtons ========
 * Check all button objects on the screen to see if touch has been registered
 *
 *
 *  Ported to C using UTFT Libary (C) by Henning Karlsen
 *  http://www.rinkydinkelectronics.com/library.php?id=51
 */
int checkButtons()
{
    if (touchDataAvailable() == 1)
    {
    	touchRead();
		int		result = -1;
		int 	i;
		int		touch_x = touchGetX();
		int		touch_y = touchGetY();

		/* Debug Touch Location
        setXY(touch_x,touch_y,touch_x,touch_y);
        writeData(0xFFFF);
        */

		for (i=0;i<btcnt+1;i++)
		{
			if (((buttons[i].flags & BUTTON_UNUSED) == 0) && ((buttons[i].flags & BUTTON_DISABLED) == 0) && (result == -1))
			{
				if ((touch_x >= buttons[i].pos_x) && (touch_x <= (buttons[i].pos_x + buttons[i].width)) && (touch_y >= buttons[i].pos_y) && (touch_y <= (buttons[i].pos_y + buttons[i].height)))
				{
					result = i;

				}
			}
		}

		while (touchDataAvailable() == 1) {};

		return result;
	}
	else
		return -1;
}

/**********************************************
Touchscreen Screens functions zone
**********************************************/


void mainScreen(){
    deleteAllButtons();
    currentScreen = _main;
    //Initialise brightness control buttons
    increaseBrightnessButton = addButton( 240,  10, 20,  20, plus, 0);	//increase brightness
    drawBitmap(265, 10, 20, 20, bright, 1);			// Brightness icon
    decreaseBrightnessButton = addButton( 290, 10, 20,  20, minus, 0); //decrease brightness

    powerButton = addButton( 10,  10, 20,  20, power, 0); // cycle screen


    couplingButton = addButton( 10,  40, 100,  27, coupling, 0); // coupling button
    verticalRangeButton = addButton( 10,  70, 100,  27, vrange, 0); // vertical range button
    horizontalRangeButton = addButton( 10,  100, 100,  27, hrange, 0); // horizontal range button

    triggerSettingsButton = addButton( 10,  140, 100,  27, triggersettings, 0); // trigger mode button
    functionGenButton = addButton( 10,  190, 100,  27, functiongen, 0); // function generator button
   // triggerTypeButton = addButton( 10,  200, 100,  27, triggertype, 0); // trigger mode button

    drawRectangle( 120, 40, 315, 227);

    drawButtons();
}

void functionGenScreen(){
    deleteAllButtons();

    currentScreen = _functionGen;

    //Initialise brightness control buttons
    increaseBrightnessButton = addButton( 240,  10, 20,  20, plus, 0);	//increase brightness
    drawBitmap(265, 10, 20, 20, bright, 1);			// Brightness icon
    decreaseBrightnessButton = addButton( 290, 10, 20,  20, minus, 0); 	//decrease brightness

    powerButton = addButton( 10,  10, 20,  20, power, 0); // cycle screen

    functionStatusButton 	= addButton( 10,  40, 100,  27, funcOnOff, 0); 	// function generator on/off
    functionPPVButton 		= addButton( 10,  70, 100,  27, funcppv, 0); 	// function generator p-p voltage
    functionWaveformButton  = addButton( 10,  100, 100,  27, wavetype, 0); 	// function generator cycle waveform
    voltageOffsetButton 	= addButton( 10,  130, 100,  27, voffset, 0); 	// function generator voltage offset
    frequencyButton 		= addButton( 10,  160, 100,  27, frequency, 0); // function generator voltage offset
    backButton 				= addButton( 10,  200, 100, 27, back, 0); 		// back

    drawRectangle( 120, 40, 315, 227);

    drawButtons();
}

void triggerSettingScreen(){
    deleteAllButtons();

    currentScreen = _trigger;

    //Initialise brightness control buttons
    increaseBrightnessButton = addButton( 240,  10, 20,  20, plus, 0);	//increase brightness
    drawBitmap(265, 10, 20, 20, bright, 1);			// Brightness icon
    decreaseBrightnessButton = addButton( 290, 10, 20,  20, minus, 0); 	//decrease brightness

    powerButton = addButton( 10,  10, 20,  20, power, 0); // cycle screen

    triggerModeButton 		= addButton( 10,  40, 100,  27, triggermode, 0); 			// trigger mode
    triggerTypeButton 		= addButton( 10,  70, 100,  27, triggertype, 0); 			// trigger type
    triggerThresholdButton	= addButton( 10,  100, 100, 27, triggerthesh, 0); 			// trigger threshold
    forceTriggerButton 		= addButton( 10,  130, 100, 27, forcetrigger, 0); 			// force trigger
    rearmTriggerButton 		= addButton( 10,  160, 100, 27, rearmtrigger, 0); 			// rearm trigger
    backButton 				= addButton( 10,  200, 100, 27, back, 0); 					// back

    drawRectangle( 120, 40, 315, 227);
    drawButtons();
}


/**********************************************
Touchscreen Tasks Zone
**********************************************/
Void touchscreenFxn(UArg arg0, UArg arg1){

    setFont(Sinclair_S);

    mainScreen();


    while(1){

        /* Get access to resource */

    //DEBUG:  Check to see where button presses are registered
    //		touchRead();
    //		i = touchGetX();
    //	    j = touchGetY();
    //	    setXY(i,j,i,j);
    //	    writeData(0xFFFF);

     if(dutyCycles == DUTY0){
    	 if (touchDataAvailable() == 1){
    		Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);
 	 		dutyCycles = DUTY100;
 	 		Semaphore_post(semHandle);
    	 }
     } else {

		 pressed_button = checkButtons();

		 if (pressed_button==increaseBrightnessButton){
			 brightness++;

		 } else if(pressed_button==decreaseBrightnessButton) {
				Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);
				if(dutyCycles == DUTY0){
					dutyCycles = DUTY0;
				} else {
					dutyCycles--;
				}
				Semaphore_post(semHandle);
			System_printf("Decrease\n");

		 } else if(pressed_button==powerButton) {
			Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);
			 if(dutyCycles == DUTY0){
				dutyCycles = DUTY100;
			 } else {
				dutyCycles = DUTY0;
			 }
			Semaphore_post(semHandle);
			System_printf("Power\n");

		 } else if(pressed_button==couplingButton) {
			System_printf("DC\n");
			if(currentCoupling == 1){
				currentCoupling = 0;
			} else {
				currentCoupling++;
			}
			//Send new setting to software
			setting_change_flag = 1;
			changeSettingCommand = _channel_coupling;
			changeSettingValue = currentCoupling;

		 } else if(pressed_button==verticalRangeButton) {
			System_printf("Vrange\n");
			setting_change_flag = 1;
			if(vDiv == 5){
				vDiv = 0;
			} else {
				vDiv++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _vertical_range;
			changeSettingValue = vDiv;

		 } else if(pressed_button==horizontalRangeButton) {
			System_printf("Hrange\n");
			if(hDiv == 18){
				hDiv = 0;
			} else {
				hDiv++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _horizontal_range;
			changeSettingValue = hDiv;

		 } else if(pressed_button==triggerSettingsButton) {
			System_printf("Trigger Settings\n");
			lcdClear();
			triggerSettingScreen();

		 } else if(pressed_button==functionGenButton) {
			lcdClear();
			functionGenScreen();

		 } else if(pressed_button==triggerModeButton) {
			System_printf("Trigger Mode\n");
			if(tMode == 2){
				tMode = 0;
			} else {
				tMode++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _trigger_mode;
			changeSettingValue = tMode;

		 } else if(pressed_button==triggerThresholdButton) {
				System_printf("Trigger Thresh\n");
				if((triggerThreshold + 128) >= 4096){
					triggerThreshold = 0;
				} else {
					triggerThreshold += 128;
				}
				setting_change_flag = 1;
				changeSettingCommand = _trigger_threshold;
				changeSettingValue = triggerThreshold;

		 } else if(pressed_button==triggerTypeButton) {
			System_printf("Trigger Type\n");
			if(tType == 2){
				tType = 0;
			} else {
				tType++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _trigger_type;
			changeSettingValue = tType;

		 } else if(pressed_button==forceTriggerButton) {
			System_printf("Force Trigger\n");
			currentTriggerState = triggered;

		 } else if(pressed_button==rearmTriggerButton) {
			System_printf("Rearm Trigger\n");
			currentTriggerState = armed;

		 } else if(pressed_button==functionStatusButton) {
			System_printf("Function On/Off\n");
			if(currentfunctionStatus == 1){
				currentfunctionStatus = 0;
			} else {
				currentfunctionStatus++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _funcgen_output;
			changeSettingValue = currentfunctionStatus;

		 } else if(pressed_button==functionPPVButton) {
			System_printf("PP Voltage\n");
			if(currentfunctionPPV == 4){
				currentfunctionPPV = 0;
			} else {
				currentfunctionPPV++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _funcgen_p2p;
			changeSettingValue = currentfunctionPPV;

		 } else if(pressed_button==functionWaveformButton) {
			System_printf("WaveType\n");
			if(currentWaveform == 4){
				currentWaveform = 0;
			} else {
				currentWaveform++;
			}
			setting_change_flag = 1;
			changeSettingCommand = _funcgen_waveform;
			changeSettingValue = currentWaveform;

		 } else if(pressed_button==voltageOffsetButton) {
			System_printf("Voltage Offset\n");
			if(currentVOffset == 4000){
				currentVOffset = 0;
			} else {
				currentVOffset += 5;
			}
			setting_change_flag = 1;
			changeSettingCommand = _funcgen_offset;
			changeSettingValue = currentVOffset;

		 } else if(pressed_button==frequencyButton) {
			System_printf("Frequency\n");
			if(currentFrequency == 25000){
				currentFrequency = 0;
			} else {
				currentFrequency++;
			}
			changeSettingCommand = _funcgen_freq;
			changeSettingValue = currentFrequency;

		 }  else if(pressed_button==backButton) {
			System_printf("Back\n");
			lcdClear();
			mainScreen();
		}
		updateStatus();
     }

    Task_sleep(1000);
    System_flush();
    }

}

/**********************************************
Brightness Task Zone
**********************************************/
Void pwmBrightnessFxn(UArg arg0, UArg arg1)
{

	int status = 0;

	dutyCycles = DUTY75;  // default duty

	GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_3);

    while (1) {
    	Semaphore_pend(semHandle,BIOS_WAIT_FOREVER);
    	switch(dutyCycles){
    		case DUTY0:
    			//Semaphore_post(semHandle);
				GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, 0x00); //always low
				Semaphore_post(semHandle);
				Task_sleep(100);
    			break;
    		case DUTY25:
				if(status == 1){
					GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, 0x00);
					Semaphore_post(semHandle);
					Task_sleep(75);
					status = 0;
				} else {
					GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, GPIO_PIN_3);
					Semaphore_post(semHandle);
					Task_sleep(25);
					status = 1;
				}
    			break;
    		case DUTY50:
				if(status == 1){
					GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, 0x00);
					Semaphore_post(semHandle);
					Task_sleep(50);
					status = 0;
				} else {
					GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, GPIO_PIN_3);
					Semaphore_post(semHandle);
					Task_sleep(50);
					status = 1;
				}
    			break;
    		case DUTY75:
				if(status == 1){
					GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, 0x00);
					Semaphore_post(semHandle);
					Task_sleep(20);
					status = 0;
				} else {
					GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, GPIO_PIN_3);
					Semaphore_post(semHandle);
					Task_sleep(70);
					status = 1;
				}
				break;
    		case DUTY100:
				GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, GPIO_PIN_3);  //always high
				Semaphore_post(semHandle);
				Task_sleep(100);
    			break;
    	}

    }
}


