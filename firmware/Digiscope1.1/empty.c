/*
 * This preamble is required by TI to use their functions.
 *
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */


#include <stdbool.h>
#include <stdint.h>
#include "stdlib.h"

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>

/* TI-RTOS Header files */
// #include <ti/drivers/EMAC.h>
#include <ti/drivers/GPIO.h>
// #include <ti/drivers/I2C.h>
// #include <ti/drivers/SDSPI.h>
// #include <ti/drivers/SPI.h>
// #include <ti/drivers/UART.h>
// #include <ti/drivers/USBMSCHFatFs.h>
// #include <ti/drivers/Watchdog.h>
// #include <ti/drivers/WiFi.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
//#include "driverlib/sysctl.h"
//#include "driverlib/sysctl.c"
#include "driverlib/gpio.h"


/* ADC Libraries*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_adc.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/adc.h"
#include "driverlib/udma.h"
#include "driverlib/fpu.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/uart.h"
#include "driverlib/systick.h"

/* Board Header file */
#include "Board.h"

//#include "Global.h"


//Default task stack size
#define TASKSTACKSIZE   512


/* Prototypes */
Void touchInit(void);							//touchscreen initialisation function
Void lcdClear(void);							//LCD clear function
Void lcdInit(void);								//LCD initialisation function

Void touchscreenFxn(UArg arg0, UArg arg1);		//touchscreen task function
Void adcFxn(UArg arg0, UArg arg1);				//ADC task function
Void triggeringFxn(UArg arg0, UArg arg1);		//Triggering task function
Void pwmBrightnessFxn(UArg arg0, UArg arg1);	//brightness task function

Void masterTaskFxn(UArg arg0, UArg arg1);		//SPI Master task function

void uDMASetup(void);							//uDMA initialisation function
void ADCSetup(void);							//ADC initialisation function


/* Task Structs */
Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

Task_Struct task1Struct;
Char task1Stack[TASKSTACKSIZE*100];

Task_Struct task2Struct;
Char task2Stack[TASKSTACKSIZE];

Task_Struct task3Struct;
Char task3Stack[TASKSTACKSIZE];

//Semaphore object for resource locking
extern Semaphore_Struct semStruct;
extern Semaphore_Handle semHandle;


/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 *
 *  Adapted from example code provided with TI-RTOS
 *
 *  (c) Texas Instruments
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{
    while (1) {
        Task_sleep((unsigned int)arg0);
        GPIO_toggle(Board_LED0);
    }
}


/*
 *  ======== main ========
 */
int main(void)
{
    /* Construct BIOS Objects */
    Task_Params taskParams;
    Task_Params taskParams1;
    Task_Params taskParams2;
    Task_Params taskParams3;
    Semaphore_Params semParams;

    /* Call board init functions */
    Board_initGeneral();		//Enable board
    Board_initEMAC();    		//Enable Ethernet functions
    Board_initGPIO();			//Enable GPIO functions
    Board_initSPI();			//Enable SPI functions

    //Initialise ADC Pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);					//enable ADC0
	while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)));

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);				//enable GPIOK peripheral.  Should be enabled but ADC doesn't work without this line
	while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOK)));

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);				//DEBUG: enable GPIOP peripheral.
	while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOP)));

	SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);					//enable UDMA
	while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_UDMA)));

	GPIOPinTypeADC(GPIO_PORTK_BASE, GPIO_PIN_0);				//set Pin K0 to be the ADC input
	GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_0);			//DEBUG: set Pin P0 for output.  This is to check that the ADC function is working and calculate time

	IntMasterEnable();			// master enable interrupts.  This should done automatically once BIOS_start() is called but for some reason, it needs this line
	IntEnable(INT_UDMAERR);		// enable UDMA error interrup

	ADCSetup();					//run ADC init
	uDMASetup();				//run UDMA init

	ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_ALWAYS, 3); // ADC TRIGGER MODE. I have this here as a software trigger, 'trigger always'

    // Call Touchscreen initialization functiosn
    lcdInit();   	//init LCD
    touchInit();	//init Touchscreen

    lcdClear();  //Reset LCD Screen

    // Construct a Semaphore object to be use as a resource lock, inital count 1
    Semaphore_Params_init(&semParams);
    Semaphore_construct(&semStruct, 1, &semParams);

    // Obtain instance handle
    semHandle = Semaphore_handle(&semStruct);


    /*##################################################################################
     * 							OTHER TASKS
     ###################################################################################*/

    /* Construct heartBeat Task  thread
    Task_Params_init(&taskParams);
    taskParams.arg0 = 1000;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    taskParams.priority = 1;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL); */

    /*##################################################################################
     * 							TOUCHSCREEN TASKS
     ###################################################################################*/

    /* Construct touchscreen Task  thread */
    Task_Params_init(&taskParams1);
    taskParams1.arg0 = 1000;
    taskParams1.stackSize = TASKSTACKSIZE*10;   //Give it some extra room to accomodate icons
    taskParams1.stack = &task1Stack;
    taskParams1.priority = 3;
    Task_construct(&task1Struct, (Task_FuncPtr)touchscreenFxn, &taskParams1, NULL);

    /*##################################################################################
     * 							ADC TASKS
     ###################################################################################*/

	/* Construct ADC Task thread */
    Task_Params_init(&taskParams2);
    taskParams2.arg0 = 1000;
    taskParams2.stackSize = TASKSTACKSIZE;
    taskParams2.stack = &task2Stack;
    taskParams2.priority = 4;
    Task_construct(&task2Struct, (Task_FuncPtr)adcFxn, &taskParams2, NULL);


    /*##################################################################################
     * 							SPI TASKS
     ###################################################################################*/

    /* Construct master/slave Task threads */
  /*  Task_Params_init(&taskParams);
    taskParams.priority = 1;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)masterTaskFxn, &taskParams, NULL);

    taskParams.stack = &task1Stack;
    taskParams.priority = 2;
    Task_construct(&task1Struct, (Task_FuncPtr)slaveTaskFxn, &taskParams, NULL);*/

    /*##################################################################################
     * 							BRIGHTNESS TASKS
     ###################################################################################*/

    /* Construct Brightness Task thread */
    Task_Params_init(&taskParams);
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.priority = 5;   // high priority due to pwm cycles shouldn't affect the sampling as there are long sleep periods
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)pwmBrightnessFxn, &taskParams, NULL);

    /*##################################################################################
	 * 							TRIGGERING TASKS
     ###################################################################################*/

	/* Construct Brightness Task thread */
	Task_Params_init(&taskParams3);
	taskParams.stackSize = TASKSTACKSIZE;
	taskParams.priority = 4;   // Triggering priority
	taskParams.stack = &task3Stack;
	Task_construct(&task3Struct, (Task_FuncPtr)triggeringFxn, &taskParams3, NULL);


    /*##################################################################################
     * 							Scheduler Initalisation
     ###################################################################################*/

     /* Turn on user LED */
    GPIO_write(Board_LED0, Board_LED_ON);

    System_printf("Starting the example\nSystem provider is set to SysMin. "
                  "Halt the target to view any SysMin contents in ROV.\n");
    /* SysMin will only print to the console when you call flush or exit */
    System_flush();

    /* Start BIOS */
    BIOS_start();

    return (0);
}
