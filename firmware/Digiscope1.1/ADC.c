/*
 * This preamble is required by TI to use their functions.
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *    ======== ADC.c ========
 *
 *	This file contains the functions that handle the ADC sampling and uDMA transfer of signal aquisition.
 *
 *  Created on: 20 May 2016
 *      Author: s4319057
 */

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "stdlib.h"

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_adc.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/adc.h"
#include "driverlib/udma.h"
#include "driverlib/fpu.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/uart.h"
#include "driverlib/systick.h"

#include <ti/sysbios/hal/Hwi.h>

#define DIV 2

//definition for trigger state
typedef enum
	{
		armed,
		triggered,
		stop,
	} triggerState;


//definition for horizontal divisions state
typedef enum {
		  _1us,
		  _2us,
		  _5us,
		  _10us,
		  _20us,
		  _50us,
		  _100us,
		  _200us,
		  _500us,
		  _1ms,
		  _2ms,
		  _5ms,
		  _10ms,
		  _20ms,
		  _50ms,
		  _100ms,
		  _200ms,
		  _500ms,
		  _1s
		} horizontal_division;

// Integers for measurings pings and pongs
static uint32_t PingMemXFerCount = 0;
static uint32_t PongMemXFerCount = 0;

static uint32_t InterruptCount = 0;// uDMA Transfer Counters

static uint32_t DMAErrorCount = 0;  //DEBUG: number of DMA errors encountered
static uint32_t statusINT = 0;  // Interrupt status


extern triggerState currentTriggerState;   //current trigger state
extern horizontal_division hDiv;			//current horizontal divisions

extern int sample_ready_flag;				//flag to indicate samples are ready
extern int sample_sent_flag;				//flag to indicate samples have been sent
extern int samplecount;						//sample counter

extern uint16_t samples[25088];				//buffer to store ADC samples

int index = 0;		//memory index for uDMA transfer buffer

//Semaphore
extern Semaphore_Struct semStruct;
extern Semaphore_Handle semHandle;

// uDMA Control Table
#pragma DATA_ALIGN(ControlTable, 1024);
uint8_t ControlTable[1024];

// PACKET_SIZE = FILTER_SIZE for an unbroken sequence of samples
#define PACKET_SIZE (256)  // Transfer size
#define FILTER_SIZE (256)  // Size of temporary sampled sequence -> this is digitally filtered then stored
static uint16_t SampleStore[256] = {0};

// Library Error
#ifdef DEBUG
void __error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

/*
 *  ======== uDMAErrorHandler ========
 *  Interrupt handler for ADC
 *
 *  This function clears uDMA error interrupt and maintains an error count
 *
 */
void uDMAErrorHandler(void)
{
	uDMAErrorStatusClear();	// clear interrupt
	DMAErrorCount++;	// Increment number of errors
}

/*
 *  ======== ADCIntHandle ========
 *  Interrupt handler for ADC
 *
 *  This function facilitates the ping pong uDMA transfer of ADC data and stores in the samples variable
 *
 *  Adapted from forum post on TI website by David Hubbard
 *  https://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/t/331374
 */
void ADCIntHandle(void)
{
	uint32_t statusDMA = 1;  // DMA status - we are testing to see if this is UDMA_MODE_STOP, signifying a completed transfer

	// Designate we have been in interrupt
	InterruptCount++;

	//Check to see if we've hit the maximum number of samples we can store in our array
	if(samplecount > 25088){
		sample_ready_flag = 1;			//enable TCP sample send flag
		samplecount = 0;				//reset sample count
		index=0;						//reset array index
		Hwi_disableInterrupt(INT_ADC0SS0);		//disable interrupts
	} else if(samplecount == 0){
		index = 0;
	}


	statusINT = ADCIntStatus(ADC0_BASE, 0, true);  	// Get the interrupt status
	ADCIntClearEx(ADC0_BASE, statusINT); 			// clear the interrupt


	////////// Ping entered one interrupt, pong entered the next. Ping resets whilst pong transfers and vice versa ///////////

		// Get the uDMA mode PING TRANSFER
		statusDMA = uDMAChannelModeGet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT);  // Check for a completed transfer.  uDMA inits not right if this is not being entered
		if ((statusDMA == UDMA_MODE_STOP)){
			PingMemXFerCount++;  //DEBUG:  Counter for ping transfer

			// Every time a completed transfer is made, the uDMA channel must be reset.  As FILTER_SIZE = PACKET_SIZE, reset channel to transfer to start of sample + current indexing
			uDMAChannelTransferSet((UDMA_CHANNEL_ADC0| UDMA_PRI_SELECT), UDMA_MODE_PINGPONG,
					(void *)(ADC0_BASE + ADC_O_SSFIFO0), &samples[0+index*PACKET_SIZE/*StorePosition*/], PACKET_SIZE);

			index++;	//increment index
			samplecount += PACKET_SIZE;  //increment total number of packets

			uDMAChannelEnable(UDMA_CHANNEL_ADC0);  //re-enable UDMA channel
			return;
		}

		// PONG TRANSFER
		statusDMA = uDMAChannelModeGet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT);  // Check for a completed transfer.  uDMA inits not right if this is not being entered
		if (statusDMA == UDMA_MODE_STOP) {
			PongMemXFerCount++; //DEBUG:  Counter for pong transfer.

			//Set uDMA transfer to incremented array
			uDMAChannelTransferSet((UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT), UDMA_MODE_PINGPONG,
					(void *)(ADC0_BASE + ADC_O_SSFIFO0), &samples[0+index*PACKET_SIZE/*StorePosition*/], PACKET_SIZE);

			index++;//increment index
			samplecount += PACKET_SIZE;			   //increment total number of packets

			uDMAChannelEnable(UDMA_CHANNEL_ADC0);  //re-enable UDMA channel
			return;
		}
}

/*
 *  ======== ADCSetup ========
 *  ADC Setup Function
 *
 *  This function setups up the TIVA for ADC sequence sampling
 *
 *  Adapted from forum post on TI website by David Hubbard
 *  https://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/t/331374
 */
void ADCSetup(void) {
	ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 15);  // 32 MHz clock SAMPLING CLOCK. should give 2MSPS

	// Setup sequencer SS0 - 8 length FIFO sequencer
	// Each FIFO entry (8) set to sample CH16 i.e the input pin
	ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_PROCESSOR, 3);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 1, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 2, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 3, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 4, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 5, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 6, ADC_CTL_CH16);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 7, ADC_CTL_CH16 | ADC_CTL_IE | ADC_CTL_END);
	ADCSequenceEnable(ADC0_BASE, 0);

}

/*
 *  ======== uDMASetup ========
 *  uDMA Setup Function
 *
 *  This function setups up the TIVA for uDMA sequence sampling transfer
 *
 *  Adapted from forum post on TI website by David Hubbard
 *  https://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/t/331374
 */
void uDMASetup(void) {

	uDMAEnable();
	uDMAControlBaseSet(ControlTable);
	ADCSequenceDMAEnable(ADC0_BASE, 0);
	// Set ping to start, single and burst transfer
	uDMAChannelAttributeDisable(UDMA_CHANNEL_ADC0, UDMA_ATTR_ALTSELECT | UDMA_ATTR_REQMASK);    //disable unnecessary bits

	uDMAChannelAttributeEnable(UDMA_CHANNEL_ADC0, UDMA_ATTR_USEBURST);							//enable burst transfer

	// Set control and transfer param for primary and alternative control structs
	uDMAChannelControlSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16 | UDMA_ARB_256);
	uDMAChannelControlSet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT, UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16 | UDMA_ARB_256);
	uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_MODE_PINGPONG, (void *)(ADC0_BASE + ADC_O_SSFIFO0), &SampleStore[0], PACKET_SIZE);
	uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT, UDMA_MODE_PINGPONG, (void *)(ADC0_BASE + ADC_O_SSFIFO0), &SampleStore[0], PACKET_SIZE);

	ADCIntEnable(ADC0_BASE, 0);   //enable ADC interrupts
	IntEnable(INT_ADC0SS0);		 //enable INT_ADC0SS0 interrupt specifically

	ADCIntEnableEx(ADC0_BASE, ADC_INT_DMA_SS0);// //enable ADC_INT_DMA_SS0 external interrupt

	uDMAChannelEnable(UDMA_CHANNEL_ADC0);  //enable uDMA
}


/*
 *  ======== adcFxn ========
 *  ADC Task Setup Function
 *
 *  This function handles the flags related to the ADC sampling rate, sample clock, interrupt enables and global flags
 *
 */
Void adcFxn(UArg arg0, UArg arg1) {

	while(1){

		//wait for semaphore to ensure sole access
		Semaphore_pend(semHandle, BIOS_WAIT_FOREVER);

		//check the horizontal division settings and change clock speed to match appropirate divisions
		switch(hDiv){
				case _1us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _2us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _5us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _10us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _20us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _50us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _100us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _200us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _500us:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _1ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 30/DIV); // Set to 32MHz
					break;
				case _2ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 60/DIV); // Set to 16MHz
					break;
				case _5ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 120/DIV); // Set to 8MHz
					break;
				case _10ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 240/DIV); // Set to 4MHz
					break;
				case _20ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 480/DIV); // Set to 2MHz
					break;
				case _50ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 1200/DIV); // Set to 800kHz
					break;
				case _100ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 2400/DIV); // Set to 400kHz
					break;
				case _200ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 4800/DIV); // Set to 200kHz
					break;
				case _500ms:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 12000/DIV); // Set to 80kHz
					break;
				case _1s:
					ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_FULL, 24000/DIV); // Set to 40kHz
					break;
			}
			Semaphore_post(semHandle);   //release semaphore to allow access to hDiv by other tasks

			Task_sleep(1);		//Add a sleep function of 1uS to allow other tasks to run


			//if sample is ready, sent the sample sent flag to 0.  i.e. the sample is ready to send
			if(sample_ready_flag == 1){
				sample_sent_flag = 0;
			}

			//if the sample sent flag is set or the sample ready flag is 0, allow interrupts to occur
			if(sample_sent_flag == 1 || sample_ready_flag == 0){
				Hwi_enableInterrupt(INT_ADC0SS0);
			}
	}
}
