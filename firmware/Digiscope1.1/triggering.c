/* This preamble is required by TI to use their functions.
 *
 *
 * Copyright (c) 2014-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    ======== triggering.c ========
 *
*	This file contains the functions that handle the trigger functionality
 *
 *  Created on: May 1, 2016
 *      Author: Jeff
 */

#include <stdbool.h>
#include <stdint.h>
#include "stdlib.h"

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>


#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
//#include "driverlib/sysctl.h"
//#include "driverlib/sysctl.c"
#include "driverlib/gpio.h"


/* Board Header file */
#include "Board.h"

typedef enum
	{
		armed,
		triggered,
		stop,
	} triggerState;

typedef enum {
	  Auto,
	  Normal,
	  Single
	} trigger_mode;

typedef enum
	{
	  Rising,
	  Falling,
	  Level
	} trigger_type;

extern triggerState currentTriggerState;
extern trigger_mode tMode;
extern trigger_type tType;
//extern horizontal_division hDiv;

extern int samplecount;
extern uint32_t samples[25000];
extern int triggerThreshold;


Void triggeringFxn(UArg arg0, UArg arg1) {

	while(1){

		switch(tMode) {
			case Auto: {
				switch(tType){
					case Rising:
						//Check to see if the samples vary slightly against each other and trigger threshold
						if((samples[12540]> samples[12541]) && (samples[12540]>triggerThreshold)){
							currentTriggerState = triggered;
						} else {
							currentTriggerState = armed;
							samplecount = 0;  //reset samples
						}
						break;
					case Falling:
						//Check to see if the samples vary slightly against each other and trigger threshold
						if((samples[12540] < samples[12544]) && (samples[12540]<triggerThreshold)){
							currentTriggerState = triggered;
						} else {
							currentTriggerState = armed;
							samplecount = 0;  //reset samples
						}
						break;
					case Level:
						//Check to see if the samples vary slightly against each other and trigger threshold
						if(((samples[12540] - samples[12540]) < 10) && (samples[12540] - triggerThreshold) < 10 ){
							currentTriggerState = triggered;
						}else {
							currentTriggerState = armed;
							samplecount = 0;  //reset samples
						}
						break;
				}
			}

			case Normal: {
				switch(tType){
					case Rising:
						//Check to see if the samples vary slightly against each other and trigger threshold
						if((samples[12540] > samples[12544]) && (samples[12540] > triggerThreshold)){
							currentTriggerState = triggered;
						} else {
							currentTriggerState = armed;
							samplecount = 0;  //reset samples
						}
						break;
					case Falling:
						//Check to see if the samples vary slightly against each other and trigger threshold
						if((samples[12540] < samples[12544]) && (samples[12540] < triggerThreshold)){
							currentTriggerState = triggered;
						} else {
							currentTriggerState = armed;
							samplecount = 0;  //reset samples
						}
						break;
					case Level:
						//Check to see if the samples vary slightly against each other and trigger threshold
						if(((samples[12540] - samples[12540]) < 10) && (samples[12540] - triggerThreshold) < 10 ){
							currentTriggerState = triggered;
						}else {
							currentTriggerState = armed;
							samplecount = 0;  //reset samples
						}
						break;
				}
			}
		}
		Task_sleep(1);
	}
}





	/*
	 * Trigger Conditions
	 *
	 * Rising edge = activated if voltage crosses the trigger threshold from below to above
	 * Falling edge = activated if voltage crosses the trigger threshold from above to below
	 * Level = activated if voltage crosses the trigger threshold
	 *
	 */

	/*
	 * Use semaphores to simulate state machine
	 *
	 */

	/*
	 * Automatic
	 *
	 *
	 *

	 *
	 */


	/*switch(tMode) {
	//	case Auto:
	//		switch(tType)
	//			case Rising:
	//				if(samples[24999] > triggerThreshold)
		case Normal:
			switch(tType){
				case Rising:
					if((samples[24999]> samples[24995]) && (samples[24999]>triggerThreshold)){
							currentTriggerState =
					}
					break;
				case Falling:
					break;
				case Level
			}*/


	/*
	 * Single
	 *
	 *
	 * if (STATE == ARMED){
	 * 	if( Has trigger condition occured == TRUE) {
	 * 		STATE = TRIGGERED;
	 * 		Aquire samples;
	 * 		STATE = STOPPED
	 * 		if(has user manually rearmed trigger == TRUE {
	 * 			STATE = ARMED
	 * 		}
	 * 	}
	 *}
	 *
	 *
	 */


	/*
	 * Normal
	 *
	 *
	 * if (STATE == ARMED){
	 * 	if( Has trigger condition occured == TRUE) {
	 * 		STATE = TRIGGERED;
	 * 		Aquire samples;
	 * 		STATE = ARMED
	 * 	}
	 *}
	 *
	 *
	 */
