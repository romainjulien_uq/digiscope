/* This preamble is required by TI to use their functions.
 *
 *
 * Copyright (c) 2014-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *    ======== tcpEcho.c ========
 *
 *	This file contains the functions that handle the ethernet communications through the use of the NDK toolkit functions provided by TI.
 *
 *	This is adpted from part of the TI-RTOS examples and thus parts such as the NDK function and TCPHandler are copyright of TI
 *
 *  Contains BSD sockets code.
 *  (c) Texas Instruments
 */

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "stdlib.h"

#include <xdc/std.h>
#include <stdio.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/GPIO.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
//#include "driverlib/sysctl.h"
//#include "driverlib/sysctl.c"
#include "driverlib/gpio.h"


/* NDK BSD support */
#include <sys/socket.h>

/* Example/Board Header file */
#include "Board.h"

//#include "Global.h"

#define TCPPACKETSIZE 1024
#define NUMTCPWORKERS 1

#define SETTING 0
#define SAMPLE 1


/*****************************************
 * 		Function Generator Enums
 ****************************************/

//Enumeration for function generator waveform
typedef enum
	{
		_sine,
		square,
		triangle,
		ramp,
		noise
	} functionWavetype;

//Enumeration for function generator peak to peak voltage
typedef enum
	{
		_100mVpp,
		_200mVpp,
		_500mVpp,
		_1Vpp,
		_2Vpp
	} functionPPV;

//Enumeration for function generator status. i.e. on or off
typedef enum
	{
		ON,
		OFF
	} functionStatus;


/*****************************************
 * 		Trigger Enums
 ****************************************/

//Enumeration for trigger state
typedef enum
	{
		armed,
		triggered,
		stop,
	} triggerState;

//Enumeration for trigger mode
typedef enum {
	  Auto,
	  Normal,
	  Single
	} trigger_mode;

//Enumeration for trigger type
typedef enum
	{
	  Rising,
	  Falling,
	  Level
	} trigger_type;


/*****************************************
 * 		Signal Enums
 ****************************************/

//Enumeration for channel
typedef enum {
		  CHA,
		  CHB
		} channel;

//Enumeration for coupling
typedef enum {
		  AC,
		  DC
		} couplingType;

//Enumeration for vertical divisions
typedef enum {
		  _20mV,
		  _50mV,
		  _100mV,
		  _200mV,
		  _500mV,
		  _1V,
		  _2V
		} vertical_divisions;

//Enumeration for horizontal divisions
typedef enum {
		  _1us,
		  _2us,
		  _5us,
		  _10us,
		  _20us,
		  _50us,
		  _100us,
		  _200us,
		  _500us,
		  _1ms,
		  _2ms,
		  _5ms,
		  _10ms,
		  _20ms,
		  _50ms,
		  _100ms,
		  _200ms,
		  _500ms,
		  _1s
		} horizontal_division;

//Enumeration for sampling resolution
typedef enum {
		  _8_Bit,
		  _12_Bit
		} resolution;

/*****************************************
 * 		Change Setting Enum
 ****************************************/

//Enumeration for setting change table. i.e. if one of these enums is current, code will change the setting
typedef enum {
		  _device_status,		//0
		  _sampling_rate,		//1
		  _channel_offset,		//2
		  _vertical_range,		//3
		  _horizontal_range,	//4
		  _trigger_mode,		//5
		  _trigger_type,		//6
		  _trigger_threshold,	//7
		  _force_trigger,		//8
		  _rearm_trigger,		//9
		  _no_of_samples,		//10
		  _funcgen_output,		//11
		  _funcgen_waveform,	//12
		  _funcgen_p2p,			//13
		  _funcgen_offset,		//14
		  _funcgen_freq,		//15
		  _bit_mode,			//16
		  _channel_coupling,	//17
		  _idle					//18
		} setting;


// Setting Global Variables
extern setting changeSetting;			//current setting
extern int changeSettingValue;		// setting value to change
extern char changeSettingCommand;		// the setting type to change

//Function Generator Variables
extern functionWavetype currentWaveform;		//current function gen waveform
extern functionPPV currentfunctionPPV;			//current function gen peak to peak voltage
extern functionStatus currentfunctionStatus;	//current function gen status. i.e on or off
extern int currentFrequency;
extern int currentVOffset;

//Signal Global Variables
extern channel currentChannel;				//current channel selected
extern resolution currentResolution;		//current ADC resolution
extern vertical_divisions vDiv;				//current vertical divisions setting
extern horizontal_division hDiv;			//current horizontal divisions setting
extern couplingType currentCoupling;			//current coupling (DC/AC)
extern int samplecount;					//current sample count.  Initially set to 0
extern int DCOffset;					//current DC offset
extern unsigned int numberOfSamples;		//current number of samples aquired

//Triggering Global Variables
extern triggerState currentTriggerState;			//current trigger state.  Default ARMED
extern trigger_mode tMode;								//current trigger mode
extern trigger_type tType;								//current trigger type
extern int triggerThreshold;  //set default to be approxiately 3/4 of total voltage range


//Control Flags for ethernet communications
extern int connected_flag; //initially not connected
extern int setting_change_flag;
extern int setting_sent_flag;
extern int sample_sent_flag;
extern int sample_ready_flag;
extern uint16_t samples[25088];

/*
 *  ======== tcpWorker ========
 *  Task to handle TCP connection. Can be multiple Tasks running
 *  this function.
 */
Void tcpWorker(UArg arg0, UArg arg1)
{
    int  clientfd = (int)arg0;	//DEBUG: ID of worker instance
    int  bytesRcvd = 0;			//Used to check if we have received data from software
    int  bytesSent;				//DEBUG:  Check we are sending sufficient bytes

    char buffer[TCPPACKETSIZE] = {0};	//

    int i,j;
    int valueReceived;


    //Number of samples currently set
    unsigned int NumSamples = 50000;  //default is 25000 samples

    //Current time factor setting
    uint32_t timeFactor = 1;

    //DEBUG:  Configure this pin as an output to so we can measure the transfer time
    GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_1);

    //DEBUG:  Confirm we are in softeare
    System_printf("tcpWorker: start clientfd = 0x%x\n", clientfd);

    //If we get to this point, we have created a worker and are thus connected to the software
    connected_flag = 1;

   while (1){

	   bytesRcvd = recv(clientfd, buffer, TCPPACKETSIZE, MSG_DONTWAIT);

	   //receive commands from software
	   if(bytesRcvd > 0){

		   //DEBUG:  how many bytes are we getting
		   //System_printf("No. of Bytes: %i\n",bytesRcvd);

		   //check that first byte is a setting byte
		   if(buffer[0] == 0){

			   //set command byte to change setting enum.  It should be the first byte
			   changeSetting = buffer[1];
			   valueReceived = buffer[5] | buffer[4]<<8 | buffer[3]<< 16 | buffer[2]<< 24;


			   //Receive data and check what command is being sent
			   switch(changeSetting){
				 case _device_status:			//0
					 currentTriggerState = valueReceived;					//update channel
					 System_printf("Device Status: %i\n",bytesRcvd);
					 break;

				 case _sampling_rate:			//1
					 System_printf("Sampling Rate: %i\n",bytesRcvd);
					 //Basically do nothing, we aren't allowing the user the directly change the sample rate
					 break;

				 case _channel_offset:		//2
					 DCOffset = valueReceived;							//update DC offset
					 System_printf("Channel Offset: %i\n",bytesRcvd);
					 break;

				 case _vertical_range:		//3
					 vDiv = valueReceived;								//update vertical divisions
					 System_printf("Vertical Range: %i\n",bytesRcvd);
					 break;

				 case _horizontal_range:		//4
					 hDiv = valueReceived;								//update horizontal range
					 System_printf("Horizontal Range: %i\n",bytesRcvd);

					 break;
				 case _trigger_mode:			//5
					 tMode = valueReceived;								//update trigger mode
					 System_printf("Trigger Mode: %i\n",bytesRcvd);
					 break;

				 case _trigger_type:			//6
					 tType = valueReceived;								//update trigger type
					 System_printf("Trigger Type: %i\n",bytesRcvd);
					 break;

				 case _trigger_threshold:		//7
					 triggerThreshold = valueReceived;					//update trigger threshold
					 System_printf("Trigger Threshold: %i\n",bytesRcvd);
					 break;

				 case _force_trigger:			//8
					 //placeholder for force trigger					//update force trigger flag
					 System_printf("Force Trigger: %i\n",bytesRcvd);
					 break;

				 case _rearm_trigger:			//9
					 hDiv = valueReceived;								//update rearm trigger flag
					 System_printf("Rearm Trigger: %i\n",bytesRcvd);
					 break;

				 case _no_of_samples:			//10
					 numberOfSamples = valueReceived;							//update number of samples
					 System_printf("No of Samples: %i\n", numberOfSamples);
					 break;

				 case _funcgen_output:		//11
					 currentfunctionStatus = valueReceived;								//update horizontal range
					 System_printf("Function Generator On/Off: %i\n",bytesRcvd);
					 break;

				 case _funcgen_waveform:		//12
					 currentWaveform = valueReceived;								//update horizontal range
					 System_printf("Function Generator Waveform: %i\n",bytesRcvd);
					 break;

				 case _funcgen_p2p:			//13
					 currentfunctionPPV = valueReceived;								//update horizontal range
					 System_printf("Function Generator P-P Voltage: %i\n",bytesRcvd);
					 break;

				 case _funcgen_offset:		//14
					 currentVOffset = valueReceived;								//update horizontal range
					 System_printf("Function Generator Offset: %i\n",bytesRcvd);
					 break;

				 case _funcgen_freq:			//15
					 currentFrequency = valueReceived;								//update horizontal range
					 System_printf("Function Generator Frequency: %i\n",bytesRcvd);
					 break;

				 case _bit_mode:				//16
					 currentResolution = valueReceived;								//update horizontal range
					 System_printf("Bit Mode: %i\n",bytesRcvd);
					 break;

				 case _channel_coupling:		//17
					 currentCoupling = valueReceived;								//update horizontal range
					 System_printf("Channel Coupling: %i\n",bytesRcvd);
					 break;

				 case _idle:					//18
					 System_printf("No Command: %i\n",bytesRcvd);
					 break;

				 default:
					 System_printf("Invalid Command: %i\n",bytesRcvd);
					 break;
			   }

			   //DEBUG: Flush stream
			   System_flush();

		   }  //otherwise ignore packet
	   }

	   if(setting_change_flag == 1){

		   //DEBUG:  set setting and valuesto change for testing
		  // changeSettingCommand = _channel_coupling;
		  // changeSettingValue = 0x1;

		   //set up command packet
			buffer[0] = SETTING; 						// setting - setting
			buffer[1] = changeSettingCommand;			// command buffer -- set by changing command
			buffer[2] = changeSettingValue << 24; 		// command value
			buffer[3] = changeSettingValue << 16;		// command value
			buffer[4] = changeSettingValue << 8; 		// command value
			buffer[5] = changeSettingValue; 			// command value

			//send command packet
			bytesSent = send(clientfd, buffer, TCPPACKETSIZE, 0);
			setting_change_flag = 0;
			setting_sent_flag = 1;
	   }


	   if(sample_ready_flag == 1){

		   if(hDiv < 10){
			   timeFactor = 9;
		   } else {
			   timeFactor = hDiv;
		   }

			//set up command packet
			buffer[0] = SAMPLE; 				// setting - sample
			buffer[1] = 1;  					// resolution - 12-bit
			buffer[2] = 0; 						// Ch A
			buffer[3] = numberOfSamples >> 24; 		// sample Highest Nibble
			buffer[4] = numberOfSamples >> 16; 		//
			buffer[5] = numberOfSamples >> 8; 		//
			buffer[6] = numberOfSamples; 			// sample Lowest Nibble
			buffer[7] = 0;						// horizontal division Highest Nibble
			buffer[8] = 0;						//
			buffer[9] = 0;						//
			buffer[10] = timeFactor;			// horizontal division Highest Nibble
			buffer[11] = 0;						// vertical division Highest Nibble
			buffer[12] = 0;						//
			buffer[13] = 0;						//
			buffer[14] = vDiv;					// vertical division Highest Nibble



			//send command packet
			bytesSent = send(clientfd, buffer, TCPPACKETSIZE, 0);


			//DEBUG: Set pin high at the start of the burst transfer
			GPIOPinWrite(GPIO_PORTG_BASE, GPIO_PIN_1, GPIO_PIN_1);

			//burst send 50000 bytes
			for(i=0;i<49;i++) {

			    /*	Loop Logic
			      			buffer[0] = sample [0] >> 8;  // upper byte
			    			buffer[1] = sample [0];		  // lower byte

			    			buffer[2] = sample [1] >> 8;
			    		    buffer[3] = sample [1];

			    		    buffer[4] = sample [2] >> 8;
			    		    buffer[5] = sample [2];
			    		    .
							.
							.
							buffer[1022] = sample [511] >> 8;
			    		    buffer[1023] = sample [511]; */

	    		for(j=0;j<1024;j+=2){

					buffer[j]=samples[(i*512 + j/2)] >> 8;  //third byte is upper since samples is 32 bit
					buffer[j+1]=samples[i*512 + j/2];    	//forth byte is lower since samples is 32 bit

	    		}
	    		//Send once buffer is loaded with samples
	    		bytesSent = send(clientfd, buffer, TCPPACKETSIZE, 0);

			}

			GPIOPinWrite(GPIO_PORTG_BASE, GPIO_PIN_1, 0x0); // Turn off pin for end transmission
			sample_ready_flag = 0;
			sample_sent_flag = 1;

	    }

	   //DEBUG:  Check correct number of bytes sent
        if (bytesSent < 0 || bytesSent != 1024) {
            System_printf("Error: send failed.\n");
            break;
        }

        Task_sleep(10);

    }

    System_printf("tcpWorker stop clientfd = 0x%x\n", clientfd);
    System_flush();
    connected_flag = 0;
    close(clientfd);
}

/*
 *  ======== tcpHandler ========
 *  Creates new Task to handle new TCP connections.
 */
Void tcpHandler(UArg arg0, UArg arg1)
{
    int                status;
    int                clientfd;
    int                server;
    struct sockaddr_in localAddr;
    struct sockaddr_in clientAddr;
    int                optval;
    int                optlen = sizeof(optval);
    socklen_t          addrlen = sizeof(clientAddr);
    Task_Handle        taskHandle;
    Task_Params        taskParams;
    Error_Block        eb;

    server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (server == -1) {
        System_printf("Error: socket not created.\n");
        goto shutdown;
    }


    memset(&localAddr, 0, sizeof(localAddr));
    localAddr.sin_family = AF_INET;
    localAddr.sin_addr.s_addr = htonl(INADDR_ANY);  // hardcode address to 192.168.137.8  INADDR_ANY
    localAddr.sin_port = htons(arg0);

    status = bind(server, (struct sockaddr *)&localAddr, sizeof(localAddr));
    if (status == -1) {
        System_printf("Error: bind failed.\n");
        goto shutdown;
    }



    status = listen(server, NUMTCPWORKERS);
    if (status == -1) {
        System_printf("Error: listen failed.\n");
        goto shutdown;
    }

    optval = 1;
    if (setsockopt(server, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
        System_printf("Error: setsockopt failed\n");
        goto shutdown;
    }

    while ((clientfd =
            accept(server, (struct sockaddr *)&clientAddr, &addrlen)) != -1) {

        System_printf("tcpHandler: Creating thread clientfd = %d\n", clientfd);

        /* Init the Error_Block */
        Error_init(&eb);

        /* Initialize the defaults and set the parameters. */
        Task_Params_init(&taskParams);
        taskParams.arg0 = (UArg)clientfd;
        taskParams.stackSize = 2048;
        taskParams.priority = 8;
        taskHandle = Task_create((Task_FuncPtr)tcpWorker, &taskParams, &eb);
        if (taskHandle == NULL) {
            System_printf("Error: Failed to create new Task\n");
            close(clientfd);
        }

        /* addrlen is a value-result param, must reset for next accept call */
        addrlen = sizeof(clientAddr);
    }

    System_printf("Error: accept failed.\n");

shutdown:
    if (server > 0) {
        close(server);
    }
}

