package Connection;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import Controller.DigiscopePaneController;

/**
 * The connection handler is responsible for opening the connection with the device. When a
 * connection is open, a new thread starts to listen for messages from the device.
 */
public class ConnectionHandler {
    // The application controller
    private DigiscopePaneController controller;

    // device port
    private final int PORT = 1000;

    // stream to send messages to device
    private DataOutputStream outputStream;

    private static ConnectionHandler ourInstance = new ConnectionHandler();

    public static ConnectionHandler getInstance() {
        return ourInstance;
    }

    private ConnectionHandler() {
    }

    /**
     * Open a connection with the device and open an input and output stream for communication.
     *
     * @param ip The ip address of the device.
     * @return An error message, or null if the connection is successful.
     */
    public String openConnection(String ip) {
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(ip);
        } catch (UnknownHostException hostException) {
            return "IP address is not valid";
        }

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ipAddress, PORT), 1000);
            outputStream = new DataOutputStream(socket.getOutputStream());
            InputStream inputStream = socket.getInputStream();
            new Thread(new InputReader(inputStream, controller)).start();
        } catch (SocketTimeoutException ste) {
            return "Socket connection timed out";
        } catch (Exception e) {
            return "Can't start connection with device";
        }

        return null;
    }

    /**
     * Send an array of byte to the device.
     *
     * @param msg An array of byte.
     */
    public void send(byte[] msg) {
        if (outputStream != null) {
            try {
                outputStream.write(msg);
                System.out.println("Message sent to device");
            } catch (IOException ioe) {
                System.out.println("Couldn't send message to device");
            }
        } else {
            System.out.println("Output stream is not open");
        }
    }

    /**
     * Pass a reference to the controller instance. Needed for the InputReader.
     *
     * @param controller The application controller.
     */
    public void setController(DigiscopePaneController controller) {
        this.controller = controller;
    }
}
