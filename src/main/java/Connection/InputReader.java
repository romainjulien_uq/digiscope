package Connection;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import Controller.DigiscopePaneController;
import Settings.HorizontalDivision;
import Settings.VerticalDivision;

/**
 * The InputReader is a thread continuously listening for inputs from the device.
 *
 * A 'header' message can either: - contain changes in settings in the following format -> 2 bytes
 * representing an index of the SettingsEnum class, indicates the setting that has  changed -> 4
 * bytes representing the new value of the setting
 *
 * - contain information about a set of samples and indicates that the next 49 packets will contain
 * samples.
 */
public class InputReader implements Runnable {

    // A flag indicating that the next messages will contain samples
    private boolean receivingSamples = false;

    // The countdown indicates how many messages are still to be received to have a full set
    // of samples
    private int countDown = 49;

    // Informations from the samples 'header' message: channel, number of samples, time division
    // and bit mode (8 or 12)
    private String channel;
    private int nbrSamples;
    private HorizontalDivision timeDivision;
    private VerticalDivision voltageDivision;
    private int resolutionMode;

    // Combine the samples from the 49 packets
    private ArrayList<Double> samples;

    // The input stream to read for inputs
    private InputStream inputStream;

    // Buffer to store new messages/packets
    private byte[] buffer;

    // The application controller
    private DigiscopePaneController controller;

    public InputReader(InputStream is, DigiscopePaneController controller) {
        inputStream = is;
        this.controller = controller;
    }

    @Override
    public void run() {
        try {
            while (true) {
                buffer = new byte[1024];
                int count = inputStream.read(buffer);
                if (!receivingSamples) {
                    byte type = buffer[0];
                    if (type == 0X00) { // settings
                        int[] msg = convertSettingMessage(buffer);
                        controller.receivedSettingMessage(msg);
                    } else if (type == 0X01) { // samples
                        readHeaderSamples(buffer);
                        samples = new ArrayList<>();
                        receivingSamples = true;
                    }
                } else { // a sample header was received and we are currently waiting for 49 packets
                    convertSamplesMessage(buffer);
                    countDown--;
                    if (countDown == 0) {
                        controller.receivedSamples(samples, timeDivision, channel);
                        samples = new ArrayList<>();
                        countDown = 49;
                        receivingSamples = false;
                    }
                }
            }
        } catch (IOException ioe) {
            //
        }
    }

    /**
     * The header message contain: - The bit resolution. - The channel corresponding to the samples.
     * - The expected number of samples. - The time it took to acquire the set of samples (in
     * microsecond)
     *
     * @param data the header message.
     */
    private void readHeaderSamples(byte[] data) {
        byte resolution = data[1];
        if (resolution == 0X00) { // 8 bits, ie 1 byte
            resolutionMode = 8;
        } else { // 12 bits, ie 2 bytes
            resolutionMode = 12;
        }
        System.out.println("Resolution: " + resolutionMode + " bits");

        byte channelByte = data[2];
        if (channelByte == 0X00) {
            channel = "A";
        } else {
            channel = "B";
        }
        System.out.println("Channel: " + channel);

        ByteBuffer bbNbrSamples = ByteBuffer.allocate(4);
        bbNbrSamples.order(ByteOrder.BIG_ENDIAN);
        bbNbrSamples.put(data[3]);
        bbNbrSamples.put(data[4]);
        bbNbrSamples.put(data[5]);
        bbNbrSamples.put(data[6]);
        nbrSamples = bbNbrSamples.getInt(0);
        System.out.println("Number of samples: " + nbrSamples);

        ByteBuffer bbTime = ByteBuffer.allocate(4);
        bbTime.order(ByteOrder.BIG_ENDIAN);
        bbTime.put(data[7]);
        bbTime.put(data[8]);
        bbTime.put(data[9]);
        bbTime.put(data[10]);
        int timeDivisionIndex = bbTime.getInt(0);
        timeDivision = HorizontalDivision.values()[timeDivisionIndex];
        System.out.println("Time division: " + timeDivision.getValue() + " us");

        ByteBuffer bbVoltage = ByteBuffer.allocate(4);
        bbVoltage.order(ByteOrder.BIG_ENDIAN);
        bbVoltage.put(data[11]);
        bbVoltage.put(data[12]);
        bbVoltage.put(data[13]);
        bbVoltage.put(data[14]);
        int voltageDivisionIndex = bbVoltage.getInt(0);
        voltageDivision = VerticalDivision.values()[voltageDivisionIndex];
        System.out.println("Voltage division: " + voltageDivision.getValue() + " mV");
    }

    /**
     * A setting message contains 6 bytes, this method converts the bytes to two integers
     * representing the setting (index in SettingsEnum) and the new value for the setting.
     *
     * @param data the message received
     * @return an array of int, first one is the setting, second one is the value.
     */
    private int[] convertSettingMessage(byte[] data) {
        int[] settingMessage = new int[2];
        try {

            byte[] valBytes = {0x00, data[1]};
            short commandShort = ByteBuffer.wrap(valBytes).order(ByteOrder.BIG_ENDIAN).getShort();

            ByteBuffer bb = ByteBuffer.allocate(4);
            bb.order(ByteOrder.BIG_ENDIAN);
            bb.put(data[2]);
            bb.put(data[3]);
            bb.put(data[4]);
            bb.put(data[5]);
            int value = bb.getInt(0);

            settingMessage[0] = (int) commandShort;
            settingMessage[1] = value;
        } catch (Exception e) {
            //
        }
        return settingMessage;
    }

    /**
     * Convert a packet containing bytes to: - values between 0 and 4095, this value is then
     * converted to a voltage between -5V and 5V 0 means -5V, 4095 means 5V, 2048 means 0V... etc
     *
     * @param data the packet is an array of 1024 bytes
     */
    private void convertSamplesMessage(byte[] data) {
        if (resolutionMode == 8) { // 1 sample is 1 byte
            for (int i = 0; i < 1024; i++) {
                try {

                    // return if we already received the expected number of samples
                    if (samples.size() == nbrSamples) {
                        System.out.println("STOP READING SAMPLES! samples array size is " + samples.size());
                        return;
                    }

                    byte[] valBytes = {0x00, data[i + 1]};
                    double sample = (double) ByteBuffer.wrap(valBytes).order(ByteOrder.BIG_ENDIAN).getShort();

                    double shift = (10.0 * sample) / 4095.0;
                    double sampleVoltage = -5.0 + shift;

                    samples.add(sampleVoltage);

                } catch (ArrayIndexOutOfBoundsException obe) {
                    System.out.println("index out of bound, ignoring last value");
                }
            }
        } else { // 1 sample is 2 bytes
            for (int i = 0; i < 1024; i++) {
                try {

                    // return if we already received the expected number of samples
                    if (samples.size() == nbrSamples) {
                        System.out.println("STOP READING SAMPLES! samples array size is " + samples.size());
                        return;
                    }

                    byte[] valBytes = {data[i], data[i + 1]};
                    double sample = (double) ByteBuffer.wrap(valBytes).order(ByteOrder.BIG_ENDIAN).getShort();

                    double shift = (10.0 * sample) / 4095.0;
                    double sampleVoltage = -5.0 + shift;

                    samples.add(sampleVoltage);

                    // double increment
                    i++;
                } catch (ArrayIndexOutOfBoundsException obe) {
                    System.out.println("index out of bound, ignoring last value");
                }
            }
        }
    }
}
