package Oscilloscope;

import java.util.ArrayList;
import java.util.Collections;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * The class responsible for generating the values for the math channel
 */
public class MathChannel {

    // The equation is stored as an array, like [A, +, 1, /, B]
    private ArrayList<String> equation = null;

    // The current set of voltages for channel A
    private ArrayList<Double> channelA = null;

    // The current set of voltages for channel B
    private ArrayList<Double> channelB = null;

    // The current set of voltages for the filter channel
    private ArrayList<Double> channelFilter = null;

    // The number of voltages that should be calculated for the math channel
    private int sizeArray = 0;

    public MathChannel() {
    }

    /**
     * Get the voltages values for the math channel. Make use of the javascript engine to do the
     * maths, the javascript engine handles basic operations and order of operations such as
     * brackets.
     *
     * @return An array of voltages.
     */
    public ArrayList<Double> getValuesConverted() {
        ArrayList<Double> values = new ArrayList<>();

        // The javascript engine
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine js = mgr.getEngineByName("JavaScript");

        for (int i = 0; i < sizeArray; i++) {
            ArrayList<String> temp = new ArrayList<>(equation);

            // Inject the voltages values from the different input channels
            if (temp.contains("A")) {
                Collections.replaceAll(temp, "A", String.valueOf(channelA.get(i)));
            }
            if (temp.contains("B")) {
                Collections.replaceAll(temp, "B", String.valueOf(channelB.get(i)));
            }
            if (temp.contains("F")) {
                Collections.replaceAll(temp, "F", String.valueOf(channelFilter.get(i)));
            }

            // The javascript engine does not calculate powers, the while loop below computes
            // the powers 'a^b' and replaces the value in the equation array 'temp'

            // The index of the power sign in the equation array
            int h;
            while ((h = temp.indexOf("^")) != -1) { // For each powers
                // compute the a value in a^b,
                // note that a can be a formula inside parenthesis. eg a=(2/6)
                Double a;

                // The starting index of a
                int s = h - 1;
                if (temp.get(s).equals(")")) { // a is between parenthesis
                    s--;
                    // sub is the formula inside parenthesis
                    String sub = "";
                    while (!temp.get(s).equals("(")) {
                        sub = temp.get(s) + sub;
                        s--;
                    }
                    try {
                        a = Double.valueOf(js.eval(sub).toString());
                    } catch (ScriptException se) {
                        a = null;
                        System.out.println("Cannot evaluate sub: " + sub);
                    }
                } else {
                    a = Double.valueOf(temp.get(h - 1));
                }

                // compute the a value in a^b, b can be a formula inside parenthesis
                Double b;

                // The ending index of b
                int e = h + 1;
                if (temp.get(e).equals("(")) { // b is between parenthesis
                    e++;
                    // sub is the formula inside parenthesis
                    String sub = "";
                    while (!temp.get(e).equals(")")) {
                        sub = sub + temp.get(e);
                        e++;
                    }
                    try {
                        sub = sub.replaceAll("--", "+");
                        b = Double.valueOf(js.eval(sub).toString());
                    } catch (ScriptException se) {
                        b = null;
                        System.out.println("Cannot evaluate sub: " + sub);
                    }
                } else {
                    b = Double.valueOf(temp.get(h + 1));
                }

                if (a != null && b != null) {
                    // Compute the power
                    String computedPower = String.valueOf(Math.pow(a, b));

                    // Remove the formula for the power and replace it by the computed value
                    int k = 0;
                    while (k <= (e - s)) {
                        temp.remove(s);
                        k++;
                    }
                    temp.add(s, computedPower);
                }
            }

            // The formula to be calculated
            String expression = "";
            for (int j = 0; j < temp.size(); j++) {
                String current = temp.get(j);
                expression += current;
            }

            // Use the javascript engine to calculate the value, at that point the formula contains
            // only numbers, parenthesis and operations signs +,-,/, and *
            try {
                expression = expression.replaceAll("--", "+");
                Double val = Double.valueOf(js.eval(expression).toString());
                values.add(val);
            } catch (ScriptException se) {
                System.out.println("Cannot evaluate expression: " + expression);
            }
        }

        return values;
    }

    /**
     * Determine if the math channel can be generated based on the input channels in the equation
     * and if values have been received for this channel. It is also necessary that all channels
     * have the same number of values.
     *
     * @return true if the voltages for the filter channel can be computed, and false otherwise.
     */
    public boolean canGenerateChannel() {
        int sizeA = 0;
        int sizeB = 0;
        int sizeFilter = 0;

        if (equation == null) {
            return false;
        }

        if (equation.contains("A")) {
            if (channelA == null) {
                return false;
            } else {
                sizeA = channelA.size();
            }
        }

        if (equation.contains("B")) {
            if (channelB == null) {
                return false;
            } else {
                sizeB = channelB.size();
            }
        }

        if (equation.contains("F")) {
            if (channelFilter == null) {
                return false;
            } else {
                sizeFilter = channelFilter.size();
            }
        }

        if (equation.contains("A") && equation.contains("B") && equation.contains("F")) {
            return sizeA == sizeB && sizeA == sizeFilter;
        }
        if (equation.contains("A") && equation.contains("B")) {
            return sizeA == sizeB;
        }
        if (equation.contains("A") && equation.contains("F")) {
            return sizeA == sizeFilter;
        }
        if (equation.contains("B") && equation.contains("F")) {
            return sizeB == sizeFilter;
        }

        return true;
    }


    /* ---------------------------------------------------------------------------------------
    ---------------------------------- SETTERS -----------------------------------------------
    --------------------------------------------------------------------------------------- */

    /**
     * Method called when a user input a new equation.
     *
     * @param equation The new equation, as an array of strings.
     * @return true if the equation only contains physical channels, and false otherwise.
     */
    public boolean setEquation(ArrayList<String> equation) {
        this.equation = equation;
        if ((equation.contains("A") || equation.contains("B")) && !equation.contains("F")) {
            return true;
        }
        return false;
    }

    public void setChannelA(ArrayList<Double> channelA) {
        this.channelA = channelA;
        sizeArray = channelA.size();
    }

    public void setChannelB(ArrayList<Double> channelB) {
        this.channelB = channelB;
        sizeArray = channelB.size();
    }

    public void setChannelFilter(ArrayList<Double> channelFilter) {
        this.channelFilter = channelFilter;
        sizeArray = channelFilter.size();
    }

    public void setSizeArray(int size) {
        this.sizeArray = size;
    }


    /* ---------------------------------------------------------------------------------------
    ---------------------------------- GETTERS -----------------------------------------------
    --------------------------------------------------------------------------------------- */

    public ArrayList<String> getEquation() {
        return equation;
    }
}
