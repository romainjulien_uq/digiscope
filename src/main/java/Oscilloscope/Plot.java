package Oscilloscope;

import org.apache.commons.lang3.ArrayUtils;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.*;

import Measurements.Measurements;
import Samples.Samples;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;
import style.Style;

/**
 * The class responsible for plotting the channels
 */
public class Plot extends JComponent {

    // A flag indicating if the plot should be cleared
    private boolean clear = false;

    // The width of the canvas
    private double width;

    // The height of the canvas
    private double height;

    // Indicates the row number for this channel in the measurements table
    private int channelRowNumber;

    // The  color to plot this channel
    private Color plotColor;

    // The voltage interval displayed in this plot (10*verticalDiv)
    private double intervalInMVolts;

    // The time interval displayed in this plot (10*horizontalDiv)
    private double intervalInMs;

    // The offset for this channel, can only be != 0 for the math and filter channels
    private double voltageOffset;

    // The y values to be plotted
    private ArrayList<Double> yValues = null;

    // The x values to be plotted
    private ArrayList<Double> xValues = null;

    // The index of the trigger in the yValues array
    private int triggerIndex;

    public Plot(int width, int height, Color plotColor, int rowNumber) {
        this.width = (double) width + 20;
        this.height = (double) height;
        this.plotColor = plotColor;
        this.channelRowNumber = rowNumber;
    }

    /**
     * Plot the channel by linearly interpolating between samples. Plot the yValues array against
     * the xValues array.
     *
     * @param g The graphics of this component
     */
    public void paint(Graphics g) {
        if (yValues == null) {
            return;
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(plotColor);

        // The origin is set to x: left, y: middle
        g2d.translate(0, height / 2);

        if (!clear) {
            // To plot the last value of the array to the far right, we compute a
            // right-shifting value
            double last = ((width * (xValues.get(xValues.size() - 1))) / intervalInMs);
            int shift = (int) (width - last - 24);

            Double minVoltage = null;
            Double maxVoltage = null;
            Double sumVoltages = 0.0;
            int counterSumVoltages = 0;

            for (int i = 0; i < xValues.size() - 1; i++) {
                // Convert the x and y values to position on the screen and add the offset
                int x1 = (int) ((width * (xValues.get(i))) / intervalInMs);
                int y1 = (int) (
                        (height * (yValues.get(i) + voltageOffset) * 1000.0) / intervalInMVolts);
                int x2 = (int) ((width * (xValues.get(i + 1))) / intervalInMs);
                int y2 = (int) (
                        (height * (yValues.get(i + 1) + voltageOffset) * 1000.0) / intervalInMVolts);

                int halfHeight = (int) height / 2;

                // MEASUREMENTS
                // Only consider values displayed on screen
                if (x2 + shift > 0) {

                    // Calculate Minimum voltage
                    if (minVoltage == null) {
                        if (y2 >= -halfHeight && y2 <= halfHeight) {
                            minVoltage = yValues.get(i + 1) + voltageOffset;
                        }
                    } else if ((yValues.get(i + 1) + voltageOffset) < minVoltage) {
                        if (y2 >= -halfHeight && y2 <= halfHeight) {
                            minVoltage = yValues.get(i + 1) + voltageOffset;
                        }
                    }

                    // Calculate Maximum voltage
                    if (maxVoltage == null) {
                        if (y2 >= -halfHeight && y2 <= halfHeight) {
                            maxVoltage = yValues.get(i + 1) + voltageOffset;
                        }
                    } else if ((yValues.get(i + 1) + voltageOffset) > maxVoltage) {
                        if (y2 >= -halfHeight && y2 <= halfHeight) {
                            maxVoltage = yValues.get(i + 1) + voltageOffset;
                        }
                    }

                    // Calculate Average voltage
                    if (y2 >= -halfHeight && y2 <= halfHeight) {
                        sumVoltages += yValues.get(i + 1) + voltageOffset;
                        counterSumVoltages++;
                    }
                }

                g2d.drawLine(x1 + shift, -y1, x2 + shift, -y2);

                // Display the trigger as a vertical orange line
                if (i == triggerIndex) {
                    g2d.setColor(Style.COLOR_DARK_ORANGE);
                    g2d.drawLine(x1 + shift, -halfHeight, x1 + shift, halfHeight);
                    g2d.setColor(plotColor);
                }
            }

            // The format to display the measurements
            DecimalFormat decimalFormatVolt = new DecimalFormat("0.00 V");
            DecimalFormat decimalFormatHertz = new DecimalFormat("0.00 HZ");


            // Update the measurements in the table
            if (minVoltage != null) {
                String minVoltageString = decimalFormatVolt.format(minVoltage);
                Measurements.getInstance().updateMeasurement(minVoltageString, channelRowNumber, 1);
            }

            if (maxVoltage != null) {
                String maxVoltageString = decimalFormatVolt.format(maxVoltage);
                Measurements.getInstance().updateMeasurement(maxVoltageString, channelRowNumber, 2);
            }

            if (maxVoltage != null && minVoltage != null) {
                Double p2p = maxVoltage - minVoltage;
                String p2pString = decimalFormatVolt.format(p2p);
                Measurements.getInstance().updateMeasurement(p2pString, channelRowNumber, 3);
            }

            if (counterSumVoltages != 0) {
                Double averageVoltage = sumVoltages / counterSumVoltages;
                String averageVoltageString = decimalFormatVolt.format(averageVoltage);
                Measurements.getInstance().updateMeasurement(
                        averageVoltageString, channelRowNumber, 4);
            }

            Measurements.getInstance().updateMeasurement(
                    decimalFormatHertz.format(getCurrentFrequency()), channelRowNumber, 6);
        }
    }


    /**
     * Apply Fast Fourier Transform to get the frequency of a set of samples.
     *
     * @return The frequency, in Hertz
     */
    private double getCurrentFrequency() {

        int numberSamples = yValues.size();

        ArrayList<Double> data = new ArrayList<>();
        ArrayList<Double> magnitude = new ArrayList<>();

        // Inject Zero after each values
        for (int i = 0; i < numberSamples; i++) {
            data.add(yValues.get(i));
            data.add(0.0);
        }

        double[] dataTransformed = ArrayUtils.toPrimitive(data.toArray(new Double[data.size()]));

        // fast fourier transform
        (new DoubleFFT_1D(numberSamples)).complexForward(dataTransformed);


        for (int i = 0; i < numberSamples; i++) {
            double realValue = dataTransformed[2*i];
            double imaginaryValue = dataTransformed[2*i + 1];
            magnitude.add(Math.sqrt(Math.pow(realValue, 2) + Math.pow(imaginaryValue, 2)));
        }

        double maxMagnitude = Double.NEGATIVE_INFINITY;
        int maxIndex = 0;

        // choose index max frequency index
        for (int i = 1; i < numberSamples; i++) {
            if (magnitude.get(i) > maxMagnitude) {
                maxMagnitude = magnitude.get(i);
                maxIndex = i;
            }
        }
        if (maxIndex > (yValues.size() / 2)) {
            maxIndex = yValues.size() - maxIndex;
        }

        return maxIndex * 1800000 / numberSamples;
    }

    /**
     * Method called when a change in oscilloscope settings have been made, or a new set of samples
     * was received and the plot needs to be redrawn.
     *
     * @param voltageOffset The channel offset (math or filter only)
     */
    public void redraw(double voltageOffset) {
        this.voltageOffset = voltageOffset;
        this.intervalInMs = (double) CanvasControls.getInstance().getTimeDivision() * 10;
        this.intervalInMVolts = (double) CanvasControls.getInstance().getVoltageDivision() * 10;
        this.repaint();
    }

    /**
     * Method called when a new set of sample is received, only applies to channels A & B.
     *
     * @param samples The new set of samples
     */
    public void setSamples(Samples samples) {
        xValues = samples.getTime();
        yValues = samples.getVoltage();
        triggerIndex = samples.getTriggerIndex();
    }

    /**
     * Method called when the y values have been computed for the math or filter channel.
     *
     * @param voltages The new voltages computed
     */
    public void setVoltages(ArrayList<Double> voltages) {
        this.yValues = voltages;
    }

    /**
     * Method called when the y values have been computed for the math or filter channel.
     *
     * @param times The x values for the graph
     */
    public void setTime(ArrayList<Double> times) {
        this.xValues = times;
    }

    /**
     * Set a flag indicating that this plot should be cleared on the next redraw.
     */
    public void setClearTrue() {
        this.clear = true;
        this.repaint();
    }

    /**
     * Set a flag indicating that this plot should NOT be cleared on the next redraw.
     */
    public void setClearFalse() {
        this.clear = false;
    }


    /**
     * @return the xValues (the time corresponding to the yValues.
     */
    public ArrayList<Double> getTime() {
        return this.xValues;
    }
}
