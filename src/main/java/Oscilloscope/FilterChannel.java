package Oscilloscope;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The class responsible for generating the values for the filter channel.
 */
public class FilterChannel {

    // The input channel for the filter channel
    private String channelSelected = "A";

    // The current set of voltages for channel A
    private ArrayList<Double> channelA = null;

    // The current set of voltages for channel B
    private ArrayList<Double> channelB = null;

    // The current set of voltages for the Math Channel
    private ArrayList<Double> channelMath = null;

    // The type of filter, IIR or FIR
    private String type = null;

    // The coefficients of the first column in the filter file
    private ArrayList<Double> firstColumnFile;

    // The coefficients of the second column in the filter file
    private ArrayList<Double> secondColumnFile;

    public FilterChannel() {

    }

    /**
     * Get the voltages values of the filter channel.
     *
     * @return An array of voltages values.
     */
    public ArrayList<Double> getValuesConverted() {
        ArrayList<Double> inputChannel = null;
        if (channelSelected.equals("A")) {
            inputChannel = new ArrayList<>(channelA);
        } else if (channelSelected.equals("B")) {
            inputChannel = new ArrayList<>(channelB);
        } else if (channelSelected.equals("Math")) {
            inputChannel = new ArrayList<>(channelMath);
        }

        if (type.equals("IIR")) {
            return getIIRFilter(inputChannel, firstColumnFile, secondColumnFile);
        } else {
            return getFIRFilter(inputChannel, firstColumnFile);
        }
    }

    /**
     * Apply the formula for FIR filters to the given array of voltages.
     *
     * @param input The voltages to convert (from channel A, B or Math)
     * @return An array of voltages
     */
    public ArrayList<Double> getFIRFilter(ArrayList<Double> input, ArrayList<Double> firstColumnCoeffs) {
        ArrayList<Double> values = new ArrayList<>();

        for (int i = 0; i < input.size(); i++) {
            Double val = 0.0;
            for (int f = 1; f <= firstColumnCoeffs.size(); f++) {
                if (i - f < 0) {
                    val += 0.0;
                } else {
                    val += firstColumnCoeffs.get(f - 1) * input.get(i - f);
                }
            }
            values.add(val);
        }
        return values;
    }

    /**
     * Apply the formula for IIR filters to the given array of voltages.
     *
     * @param input The voltages to convert (from channel A, B or Math)
     * @return An array of voltages
     */
    private ArrayList<Double> getIIRFilter(ArrayList<Double> input,
                                           ArrayList<Double> firstColumnCoeffs,
                                           ArrayList<Double> secondColumnCoeffs) {
        ArrayList<Double> values = new ArrayList<>();

        for (int i = 0; i < input.size(); i++) {
            Double a = 0.0;

            for (int f = 0; f < firstColumnCoeffs.size(); f++) {
                if (i - f < 0) {
                    a += 0.0;
                } else {
                    a += firstColumnCoeffs.get(f) * input.get(i - f);
                }
            }

            Double b = 0.0;

            for (int f = 1; f < secondColumnCoeffs.size(); f++) {
                if (i - f < 0) {
                    b += 0.0;
                } else {
                    b += secondColumnCoeffs.get(f) * values.get(i - f);
                }
            }

            Double val = (a - b) / secondColumnCoeffs.get(0);

            values.add(val);
        }
        return values;
    }

    /**
     * Determine if the filter channel can be generated based on the current channel selected as
     * input and if values have been received for this channel.
     *
     * @return true if the voltages for the filter channel can be computed, and false otherwise.
     */
    public boolean canGenerateChannel() {
        if (type == null) {
            return false;
        }

        if (channelSelected.equals("A")) {
            return channelA != null;
        } else if (channelSelected.equals("B")) {
            return channelB != null;
        } else if (channelSelected.equals("Math")) {
            return channelMath != null;
        }

        // Should never get here
        return false;
    }

    /**
     * Method called when a file containing filter coefficients have been selected by the user.
     *
     * This method determine what type of filter this is (IIR or FIR) and stores the coefficients in
     * arrays. Also check if the file is invalid.
     *
     * @return null is the file is invalid or the type of filter if it is.
     */
    public String readFile(File file) {
        type = null;

        firstColumnFile = new ArrayList<>();
        secondColumnFile = new ArrayList<>();

        ArrayList<String> first = new ArrayList<>();
        ArrayList<String> second = new ArrayList<>();

        String filePath = file.getPath();
        try {
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String line;
            // Read all lines in file
            while ((line = in.readLine()) != null) {
                List<String> columns = Arrays.asList(line.split(","));

                first.add(columns.get(0).trim());
                if (columns.size() == 2) {
                    second.add(columns.get(1).trim());
                }
            }
        } catch (IOException e) {
            return null;
        }

        for (int i = 0; i < first.size(); i++) {
            Double val = Double.valueOf(first.get(i));
            firstColumnFile.add(val);
        }

        if (second.size() != 0) {
            if (first.size() != second.size()) {
                return null;
            }

            for (int i = 0; i < second.size(); i++) {
                Double val = Double.valueOf(second.get(i));
                secondColumnFile.add(val);
            }

            type = "IIR";
        } else {
            type = "FIR";
        }

        return type;
    }

    /* ---------------------------------------------------------------------------------------
    ---------------------------------- SETTERS -----------------------------------------------
    --------------------------------------------------------------------------------------- */

    public void setChannelA(ArrayList<Double> channelA) {
        this.channelA = channelA;
    }

    public void setChannelB(ArrayList<Double> channelB) {
        this.channelB = channelB;
    }

    public void setChannelMath(ArrayList<Double> channelFilter) {
        this.channelMath = channelFilter;
    }

    public void setChannelSelected(String selection) {
        this.channelSelected = selection;
    }

    /* ---------------------------------------------------------------------------------------
    ---------------------------------- GETTERS -----------------------------------------------
    --------------------------------------------------------------------------------------- */

    public String getChannelSelected() {
        return channelSelected;
    }
}

