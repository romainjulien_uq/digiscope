package Oscilloscope;

import org.apache.commons.math3.util.Precision;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import Measurements.Measurements;
import Samples.Samples;
import style.Style;

/**
 * Copyright 2016 Romain JULIEN
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This class encapsulates the 4 plots for the channels and the grid.
 * It is responsible for redrawing the canvas when settings are changed or new samples are received.
 */
public class OscilloscopeCanvas extends JPanel {

    // The plot of channel A
    private Plot plotA;

    // The plot of channel B
    private Plot plotB;

    // The plot of the math channel
    private Plot plotMath;

    // The plot of the filter channel
    private Plot plotFilter;

    // The class to compute the math channel
    private MathChannel mathChannel;

    // The class to compute the filter channel
    private FilterChannel filterChannel;

    public OscilloscopeCanvas() {
        super();

        mathChannel = new MathChannel();
        filterChannel = new FilterChannel();

        setLayout(null);

        // The size of the canvas is proportional to the size of the screen
        int canvasWidth = Style.getWindowWidth() - 380;
        canvasWidth = (int) Precision.round((double) canvasWidth, -1);
        int canvasHeight = Style.getWindowHeight() - 339;
        canvasHeight = (int) Precision.round((double) canvasHeight, -1);

        Border loweredBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
        Border insets = BorderFactory.createEmptyBorder(20, 20, 20, 20);
        setBorder(BorderFactory.createCompoundBorder(insets, loweredBorder));
        setBackground(Style.COLOR_DARK_GREY);
        setPreferredSize(new Dimension(canvasWidth + 40, canvasHeight + 40));
        setMaximumSize(new Dimension(canvasWidth + 40, canvasHeight + 40));
        setAlignmentX(Component.LEFT_ALIGNMENT);

        plotA = new Plot(canvasWidth, canvasHeight, Style.COLOR_CHANNEL_A, 1);
        plotA.setBounds(21, 21, canvasWidth - 2, canvasHeight - 2);
        add(plotA);

        plotB = new Plot(canvasWidth, canvasHeight, Style.COLOR_CHANNEL_B, 2);
        plotB.setBounds(21, 21, canvasWidth - 2, canvasHeight - 2);
        add(plotB);

        plotMath = new Plot(canvasWidth, canvasHeight, Style.COLOR_CHANNEL_MATH, 3);
        plotMath.setBounds(21, 21, canvasWidth - 2, canvasHeight - 2);
        add(plotMath);

        plotFilter = new Plot(canvasWidth, canvasHeight, Style.COLOR_CHANNEL_FILTER, 4);
        plotFilter.setBounds(21, 21, canvasWidth - 2, canvasHeight - 2);
        add(plotFilter);

        Grid grid = new Grid(canvasWidth, canvasHeight);
        grid.setBounds(21, 21, canvasWidth - 2, canvasHeight - 2);
        add(grid);
    }

    /**
     * Method called when a new equation is set for the math channel.
     * Returns a boolean indicating if the math channel can be used as input for the filter channel.
     *
     * @param equation The new equation, as an array of strings
     * @return true if the math channel can be selected as input for the filter channel, and false
     * otherwise.
     */
    public boolean setEquation(ArrayList<String> equation) {
        boolean selectableForFilter = mathChannel.setEquation(equation);
        redrawCanvas();
        return selectableForFilter;
    }

    /**
     * Method called when a file is selected for the filter channel. Pas it on to the filter channel
     * class to extract the coefficients.
     *
     * Returns the type of filter, IIR or FIR.
     *
     * @param file The file selected by the user.
     * @return
     */
    public String filterFileSelected(File file) {
        String type = filterChannel.readFile(file);
        redrawCanvas();
        return type;
    }

    /**
     * Method called when the user has selected a new input for the filter channel.
     *
     * @param selection The input channel for the filter channel.
     */
    public void setInputFilterChannel(String selection) {
        filterChannel.setChannelSelected(selection);
        redrawCanvas();
    }

    /**
     * Method called when a new set of samples has been received. Set the samples to the channels.
     *
     * @param samples The new set of samples.
     * @param channel The associated channel.
     */
    public void setSamples(Samples samples, String channel) {
        if (channel.equals("A")) {
            plotA.setSamples(samples);
            mathChannel.setChannelA(samples.getVoltage());
            filterChannel.setChannelA(samples.getVoltage());
        } else if (channel.equals("B")) {
            plotB.setSamples(samples);
            mathChannel.setChannelB(samples.getVoltage());
            filterChannel.setChannelB(samples.getVoltage());
        }
        redrawCanvas();
    }

    /**
     * This method is responsible for deciding if a channel should be plotted, or cleared.
     */
    public void redrawCanvas() {
        boolean filterChannelIsSet = false;
        boolean mathChannelIsSet = false;

        // Plot channel A if it is selected visible
        if (CanvasControls.getInstance().isChannelASelected()) {
            plotA.setClearFalse();
            plotA.redraw(0.0);
        } else {
            plotA.setClearTrue();
            Measurements.getInstance().clearRow(1);
        }

        // Plot channel B if it is selected visible
        if (CanvasControls.getInstance().isChannelBSelected()) {
            plotB.setClearFalse();
            plotB.redraw(0.0);
        } else {
            plotB.setClearTrue();
            Measurements.getInstance().clearRow(2);
        }

        // If the math channel depends on the filter channel, set the filter channel
        if (mathChannel.getEquation() != null && mathChannel.getEquation().contains("F") && filterChannel.canGenerateChannel()) {
            mathChannel.setChannelFilter(filterChannel.getValuesConverted());
            setFilterChannel();
            filterChannelIsSet = true;
        }

        // Plot math channel if set to visible and can be generated
        if (CanvasControls.getInstance().isChannelMathSelected() && mathChannel.canGenerateChannel()) {
            setMathChannel();
            mathChannelIsSet = true;
            plotMath.setClearFalse();
            plotMath.redraw(CanvasControls.getInstance().getChannelMathOffset());
        } else if (!CanvasControls.getInstance().isChannelMathSelected() || !mathChannel.canGenerateChannel()) {
            plotMath.setClearTrue();
            Measurements.getInstance().clearRow(3);
        }

        // If the filter channel has the math channel as input, set the math channel
        if (filterChannel.getChannelSelected().equals("Math") && mathChannel.canGenerateChannel()) {
            filterChannel.setChannelMath(mathChannel.getValuesConverted());
            if (!mathChannelIsSet) {
                setMathChannel();
            }
        }

        // Plot filter channel if set to visible and can be generated
        if (CanvasControls.getInstance().isChannelFilterSelected() && filterChannel.canGenerateChannel()) {
            if (!filterChannelIsSet) {
                setFilterChannel();
            }
            plotFilter.setClearFalse();
            plotFilter.redraw(CanvasControls.getInstance().getChannelFilterOffset());
        } else if (!CanvasControls.getInstance().isChannelFilterSelected() || !filterChannel.canGenerateChannel()) {
            plotFilter.setClearTrue();
            Measurements.getInstance().clearRow(4);
        }
    }

    /**
     * Set the values for the math channel, ie x and y axis
     */
    private void setMathChannel() {
        ArrayList<String> equation = mathChannel.getEquation();
        if (equation.contains("A")) {
            ArrayList<Double> timeA = plotA.getTime();
            mathChannel.setSizeArray(timeA.size());
            plotMath.setTime(timeA);
        } else if (equation.contains("B")) {
            ArrayList<Double> timeB = plotB.getTime();
            mathChannel.setSizeArray(timeB.size());
            plotMath.setTime(timeB);
        } else if (equation.contains("F")) {
            ArrayList<Double> timeF = plotFilter.getTime();
            mathChannel.setSizeArray(timeF.size());
            plotMath.setTime(timeF);
        }
        plotMath.setVoltages(mathChannel.getValuesConverted());
    }

    /**
     * Set the values for the filter channel, ie the correct time/x axis
     */
    private void setFilterChannel() {
        plotFilter.setVoltages(filterChannel.getValuesConverted());
        String selection = filterChannel.getChannelSelected();
        if (selection.equals("A")) {
            plotFilter.setTime(plotA.getTime());
        } else if (selection.equals("B")) {
            plotFilter.setTime((plotB.getTime()));
        } else if (selection.equals("Math")) {
            plotFilter.setTime(plotMath.getTime());
        }
    }
}
