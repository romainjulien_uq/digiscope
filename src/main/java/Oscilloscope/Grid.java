package Oscilloscope;

import java.awt.*;

import javax.swing.*;

import style.Style;

/**
 * The grid displayed in the background of the Oscilloscope canvas. The grid is made of 10 rows and
 * 10 columns.
 */
public class Grid extends JComponent {

    // The width of the grid
    private int width;

    // The height of the grid
    private int height;

    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Paint 9 dashed horizontal lines to make a grid of 10 rows. Paint 9 dashed vertical lines to
     * make a grid of 10 columns.
     *
     * The middle horizontal and vertical lines are solid to indicate the 0 value.
     *
     * @param g The graphics object
     */
    public void paint(Graphics g) {
        //creates a copy of the Graphics instance
        Graphics2D g2dDashed = (Graphics2D) g.create();

        //set the stroke of the copy, not the original
        Stroke dashed = new BasicStroke(0.1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
        g2dDashed.setStroke(dashed);
        g2dDashed.setColor(Style.COLOR_GREY);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Style.COLOR_GREY);

        // draw the rows
        int rowHeight = height / 10;
        for (int i = 1; i < 11; i++) {
            if (i == 5) {
                g2d.drawLine(0, i * rowHeight, width, i * rowHeight);
            } else {
                g2dDashed.drawLine(0, i * rowHeight, width, i * rowHeight);
            }
        }

        // draw the columns
        int rowWidth = width / 10;
        for (int i = 1; i < 11; i++) {
            if (i == 5) {
                g2d.drawLine(i * rowWidth, 0, i * rowWidth, height);
            } else {
                g2dDashed.drawLine(i * rowWidth, 0, i * rowWidth, height);
            }
        }

        g2dDashed.dispose();
    }
}

