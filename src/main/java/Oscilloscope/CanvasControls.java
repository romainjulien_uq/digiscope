package Oscilloscope;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;

import Controller.DigiscopePaneController;
import Settings.ChannelCouplingEnum;
import Settings.HorizontalDivision;
import Settings.VerticalDivision;
import style.ClickLabel;
import style.Style;

/**
 * This class contains the channel controls such as visibility, offset, etc. It also contains
 * controls for the math and filter channel.
 */
public class CanvasControls {

    // The application controller
    private DigiscopePaneController controller;

    // The main panel
    private JPanel controlPanel;

    // The HorizontalDivisionEnum index indicating the time division, starts at 1ms
    private int indexRangeTimeDivision = 9;
    // The text representation of the horizontal division, with units
    private JLabel horizontalRangeLabel;

    // The VerticaldDivisionEnum index indicating the vertical division, starts at 1V
    private int indexRangeVoltageDivision = 5;
    // The text representation of the vertical division, with units
    private JLabel verticalRangeLabel;

    // Flags indicating if the channel should be displayed
    private boolean channelASelected = true;
    private boolean channelBSelected = false;
    private boolean channelMathSelected = false;
    private boolean channelFilterSelected = false;

    // The possible channel couplings
    private JComboBox channelCouplingCB;

    // The Math and Filter channel offsets, implemented in the software
    private double channelMathOffset = 0;
    private double channelFilterOffset = 0;

    // The physical channels offset
    private JTextField chABOffsetTF;

    // The math channel offset
    private JTextField chMathOffsetTF;

    // The filter channel offset
    private JTextField chFilterOffsetTF;

    // The possible channel inputs for the filter channel
    private JComboBox channelChoicesCB;

    // A flag indicating if the samples should be ran through the bandpass filter
    private boolean bandpassFilterActive = false;

    // A flag avoiding a setting coming from the device triggers a change to be sent to the device.
    private boolean setFromDevice = false;

    private static CanvasControls ourInstance = new CanvasControls();

    public static CanvasControls getInstance() {
        return ourInstance;
    }

    private CanvasControls() {
        controlPanel = new JPanel();
        BoxLayout layout = new BoxLayout(controlPanel, BoxLayout.Y_AXIS);
        controlPanel.setLayout(layout);
        controlPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        controlPanel.setOpaque(false);

        addcontrolPanelComponents();
    }

    /* ---------------------------------------------------------------------------------------
    ------------------------------------- HELPER METHODS -------------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Method called after a new time division has been set. Get the value (in micro seconds) of the
     * new time division and format it with the appropriate unit.
     *
     * @return The time division to be displayed to the user.
     */
    private String getTimeDivisionDisplay() {
        int timeDivision = getTimeDivision();
        String display;
        if (timeDivision > 0 && timeDivision < 1000) {
            display = timeDivision + "us";
        } else if (timeDivision >= 1000 && timeDivision < 1000000) {
            display = (timeDivision / 1000) + "ms";
        } else {
            display = "1s";
        }
        return display;
    }

    /**
     * Method called after a new voltage division has been set. Get the value (in milli volts) of
     * the new voltage division and format it with the appropriate unit.
     *
     * @return The voltage division to be displayed to the user.
     */
    private String getVoltageDivisionDisplay() {
        int voltageDivision = getVoltageDivision();
        String display = "";
        if (voltageDivision > 0 && voltageDivision < 1000) {
            display = voltageDivision + "mv";
        } else {
            display = (voltageDivision / 1000) + "v";
        }
        return display;
    }

    /**
     * Method called when the user wants to select a file for the filter channel.
     *
     * @return The file selected by the user
     */
    private File chooseFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(null);
        File file = fileChooser.getSelectedFile();
        return file;
    }

    /**
     * Method called when the user input a new equation for the math channel. Add each element of
     * the formula to the array and substitute the values e and pi to their respective values.
     *
     * @param input The formula input by the user.
     * @return null if the equation contains invalid elements, and the equation as an array
     * otherwise
     */
    private ArrayList<String> getEquationArray(String input) {
        ArrayList<String> equationArray = new ArrayList<>();

        // returns null if the equation contains invalid elements
        Pattern validCharacters = Pattern.compile("[ABF\\d.\\+\\-\\*\\/\\^pie\\(\\)]+");
        Matcher match = validCharacters.matcher(input);
        if (!match.matches()) {
            return null;
        }

        // retunns null if the first characters of the equation are operators
        Pattern invalidCharsFirstLetter = Pattern.compile("[\\+\\*\\/\\^]");
        String first = String.valueOf(input.charAt(0));
        match = invalidCharsFirstLetter.matcher(first);
        if (match.matches()) {
            return null;
        }

        // A flag indicating if the next element in the formula should be an operator
        boolean needOpsNext = false;
        // increment when finding '(', decrement when finding ')'
        int openedBrackets = 0;

        // Possible operators
        Pattern operators = Pattern.compile("[\\+\\-\\*\\/\\^]");
        // valid characters that can be added to the array
        Pattern single = Pattern.compile("[ABFe]");
        // digit pattern
        Pattern digit = Pattern.compile("\\d");
        // decimal number pattern
        Pattern decimalNumberBeginningOfLine = Pattern.compile("^(\\d+(\\.\\d+)?)(.+)?");

        int i = 0;
        while (i < input.length()) {
            String current = String.valueOf(input.charAt(i));
            if (needOpsNext) { // We are expecting an operator next
                match = operators.matcher(current);
                if (match.matches()) {
                    equationArray.add(current);
                    needOpsNext = false;
                    i++;
                } else if (current.equals(")")) { // add bracket to array, still expecting an op
                    equationArray.add(current);
                    openedBrackets--;
                    i++;
                } else {
                    return null;
                }
            } else { // We are expecting a number, or a single char next
                match = single.matcher(current);
                if (match.matches()) { // straightforward
                    if (current.equals("e")) {
                        current = "2.71828";
                    }
                    equationArray.add(current);
                    needOpsNext = true;
                    i++;
                } else if (current.equals("-")) { // - sign
                    if (equationArray.size() != 0) {
                        if (equationArray.get(equationArray.size() - 1).equals("-")) {
                            equationArray.set((equationArray.size() - 1), "+");
                            i++;
                            continue;
                        }
                    }
                    equationArray.add(current);
                    i++;
                } else if (current.equals("(")) { // add bracket to array, still not expecting an op
                    equationArray.add(current);
                    openedBrackets++;
                    i++;
                } else if (current.equals("p")) { // pi
                    if (i != input.length() - 1) {
                        String pi = input.substring(i, i + 2);
                        if (pi.equals("pi")) {
                            equationArray.add("3.14159");
                            needOpsNext = true;
                            i += 2;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                } else { // should be a number
                    match = digit.matcher(current);
                    if (match.matches()) {
                        String temp = input.substring(i);
                        match = decimalNumberBeginningOfLine.matcher(temp);
                        if (match.matches()) {
                            String num = match.group(1);
                            equationArray.add(num);
                            needOpsNext = true;
                            i += num.length();
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
            }
        }

        // returns null if the last element of the equation is an operator
        String last = equationArray.get(equationArray.size() - 1);
        match = operators.matcher(last);
        if (match.matches()) {
            return null;
        }

        // check if some brackets were left opened
        if (openedBrackets != 0) {
            return null;
        }

        return equationArray;
    }

    /* ---------------------------------------------------------------------------------------
    ---------------------------------- HELPER LAYOUT METHODS ---------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Init and layout the control pane component.
     */
    private void addcontrolPanelComponents() {
        JLabel title = new JLabel("Digiscope");
        title.setFont(Style.FONT_TITLE_BOLD);

        JPanel channelCoupling = new JPanel();
        BoxLayout layoutCC = new BoxLayout(channelCoupling, BoxLayout.X_AXIS);
        channelCoupling.setLayout(layoutCC);
        channelCoupling.setOpaque(false);

        initChannelCouplingPaneComponents(channelCoupling);

        JPanel horizontalDivisionPane = new JPanel();
        BoxLayout layoutHR = new BoxLayout(horizontalDivisionPane, BoxLayout.X_AXIS);
        horizontalDivisionPane.setLayout(layoutHR);
        horizontalDivisionPane.setOpaque(false);

        initHorizontalDivisionPaneComponents(horizontalDivisionPane);

        JPanel verticalDivisionPane = new JPanel();
        BoxLayout layoutVR = new BoxLayout(verticalDivisionPane, BoxLayout.X_AXIS);
        verticalDivisionPane.setLayout(layoutVR);
        verticalDivisionPane.setOpaque(false);

        initVerticalDivisionPaneComponents(verticalDivisionPane);

        JLabel cvTitle = new JLabel("Visible Channels ");
        cvTitle.setFont(Style.FONT_TEXT);

        JPanel channelVisibilityPane = new JPanel();
        BoxLayout layoutCV = new BoxLayout(channelVisibilityPane, BoxLayout.X_AXIS);
        channelVisibilityPane.setLayout(layoutCV);
        channelVisibilityPane.setOpaque(false);

        initChannelVisibilityPaneComponents(channelVisibilityPane);

        JLabel coTitle = new JLabel("Channel offset ");
        coTitle.setFont(Style.FONT_TEXT);

        JPanel channelOffsetPane = new JPanel();
        BoxLayout layoutCO = new BoxLayout(channelOffsetPane, BoxLayout.X_AXIS);
        channelOffsetPane.setLayout(layoutCO);
        channelOffsetPane.setOpaque(false);

        initChannelOffsetPaneComponents(channelOffsetPane);

        JLabel mcTitle = new JLabel("Math Channel");
        mcTitle.setFont(Style.FONT_TITLE);

        JPanel mathChannelPane = new JPanel();
        BoxLayout layoutE = new BoxLayout(mathChannelPane, BoxLayout.X_AXIS);
        mathChannelPane.setLayout(layoutE);
        mathChannelPane.setOpaque(false);

        initMathChannelPaneComponents(mathChannelPane);

        JLabel fcTitle = new JLabel("Filter Channel");
        fcTitle.setFont(Style.FONT_TITLE);

        JPanel filterChannelPane = new JPanel();
        BoxLayout layoutFE = new BoxLayout(filterChannelPane, BoxLayout.X_AXIS);
        filterChannelPane.setLayout(layoutFE);
        filterChannelPane.setOpaque(false);

        initFilterChannelInputPaneComponents(filterChannelPane);

        JPanel filterType = new JPanel();
        BoxLayout layoutFT = new BoxLayout(filterType, BoxLayout.X_AXIS);
        filterType.setLayout(layoutFT);
        filterType.setOpaque(false);

        JLabel ftTitle = new JLabel("Filter Type: ");
        ftTitle.setFont(Style.FONT_TEXT);
        JLabel filterTypeLabel = new JLabel("No file selected");
        filterTypeLabel.setFont(Style.FONT_TEXT);
        JButton loadFilterFileB = new JButton("Load file");
        loadFilterFileB.setFont(Style.FONT_TEXT);
        loadFilterFileB.addActionListener(e -> {
            // Returns the type of filter (IIR or FIR)
            String type = controller.filterFileSelected(chooseFile());
            if (type != null) {
                filterTypeLabel.setText(type);
            }
        });

        filterType.add(ftTitle);
        filterType.add(Box.createHorizontalStrut(10));
        filterType.add(filterTypeLabel);
        filterType.add(Box.createHorizontalStrut(10));
        filterType.add(loadFilterFileB);

        // When this checkbox is selected, all received samples are ran through the bandpass filter
        JCheckBox BandpassFilterActiveCB = new JCheckBox("Bandpass Filter Active");
        BandpassFilterActiveCB.setFont(Style.FONT_TEXT);
        BandpassFilterActiveCB.addItemListener(e ->
                bandpassFilterActive = (e.getStateChange() == ItemEvent.SELECTED));

        controlPanel.add(Box.createVerticalStrut(20));
        title.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(title);

        controlPanel.add(Box.createVerticalStrut(10));
        JSeparator separatorTitle = new JSeparator(SwingConstants.HORIZONTAL);
        separatorTitle.setForeground(Style.COLOR_GREY);
        controlPanel.add(separatorTitle);

        controlPanel.add(Box.createVerticalStrut(5));
        horizontalDivisionPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(horizontalDivisionPane);
        controlPanel.add(Box.createVerticalStrut(5));
        verticalDivisionPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(verticalDivisionPane);
        controlPanel.add(Box.createVerticalStrut(5));
        channelCoupling.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(channelCoupling);
        controlPanel.add(Box.createVerticalStrut(10));
        cvTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(cvTitle);
        controlPanel.add(Box.createVerticalStrut(3));
        channelVisibilityPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(channelVisibilityPane);
        controlPanel.add(Box.createVerticalStrut(10));
        coTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(coTitle);
        controlPanel.add(Box.createVerticalStrut(3));
        channelOffsetPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(channelOffsetPane);

        controlPanel.add(Box.createVerticalStrut(10));
        JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
        separator.setForeground(Style.COLOR_GREY);
        controlPanel.add(separator);

        controlPanel.add(Box.createVerticalStrut(5));
        mcTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(mcTitle);
        controlPanel.add(Box.createVerticalStrut(5));
        mathChannelPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(mathChannelPane);

        controlPanel.add(Box.createVerticalStrut(10));
        JSeparator separator2 = new JSeparator(SwingConstants.HORIZONTAL);
        separator2.setForeground(Style.COLOR_GREY);
        controlPanel.add(separator2);

        controlPanel.add(Box.createVerticalStrut(5));
        fcTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(fcTitle);
        controlPanel.add(Box.createVerticalStrut(5));
        filterChannelPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(filterChannelPane);
        controlPanel.add(Box.createVerticalStrut(5));
        filterType.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(filterType);

        controlPanel.add(Box.createVerticalStrut(10));
        JSeparator separator3 = new JSeparator(SwingConstants.HORIZONTAL);
        separator3.setForeground(Style.COLOR_GREY);
        controlPanel.add(separator3);

        controlPanel.add(Box.createVerticalStrut(5));
        BandpassFilterActiveCB.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPanel.add(BandpassFilterActiveCB);
    }

    /**
     * Init and layout the channel coupling controls.
     *
     * If the channel coupling is set to AC, the offset set by the user is ignored/removed.
     *
     * @param channelCoupling The panel containing the control for the channel coupling.
     */
    private void initChannelCouplingPaneComponents(JPanel channelCoupling) {
        JLabel ccTitle = new JLabel("Channel coupling ");
        ccTitle.setFont(Style.FONT_TEXT);

        channelCouplingCB = new JComboBox(ChannelCouplingEnum.getAllValues());
        channelCouplingCB.setMinimumSize(new Dimension(90, channelCouplingCB.getPreferredSize().height));
        channelCouplingCB.setPreferredSize(new Dimension(90, channelCouplingCB.getPreferredSize().height));
        channelCouplingCB.setMaximumSize(new Dimension(90, channelCouplingCB.getPreferredSize().height));
        channelCouplingCB.setFont(Style.FONT_TEXT);
        channelCouplingCB.addActionListener(e -> {
            int index = channelCouplingCB.getSelectedIndex();
            ChannelCouplingEnum coupling = ChannelCouplingEnum.values()[index];

            switch (coupling) {
                case AC: // clear offset text fields and disable them
                    chABOffsetTF.setEnabled(false);
                    chMathOffsetTF.setEnabled(false);
                    chFilterOffsetTF.setEnabled(false);
                    chABOffsetTF.setText("");
                    chMathOffsetTF.setText("");
                    channelMathOffset = 0.0;
                    chFilterOffsetTF.setText("");
                    channelFilterOffset = 0.0;
                    controller.channelSettingsChanged();
                    break;
                case DC:
                    chABOffsetTF.setEnabled(true);
                    chMathOffsetTF.setEnabled(true);
                    chFilterOffsetTF.setEnabled(true);
                    break;
            }

            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            controller.sendToDevice((short) 17, index);
        });

        channelCoupling.add(ccTitle);
        channelCoupling.add(Box.createHorizontalStrut(10));
        channelCoupling.add(channelCouplingCB);
    }

    /**
     * Init and layout the horizontal division pane components. The time division is set by clicking
     * on left and right arrows, changing to the previous or next value as listed in the
     * HorizontalDivisionEnum.
     *
     * @param horizontalDivisionPane The panel containing the controls for the horizontal division.
     */
    private void initHorizontalDivisionPaneComponents(JPanel horizontalDivisionPane) {
        JLabel hrTitle = new JLabel("Time division ");
        hrTitle.setFont(Style.FONT_TEXT);

        ClickLabel leftHR = new ClickLabel("<", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT);
        leftHR.setFont(Style.FONT_BIG_ICON);
        leftHR.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (indexRangeTimeDivision == 0) {
                    return;
                }
                indexRangeTimeDivision += -1;
                horizontalRangeLabel.setText(getTimeDivisionDisplay());
                controller.channelSettingsChanged();

                controller.sendToDevice((short) 4, indexRangeTimeDivision);
            }
        });

        horizontalRangeLabel = new JLabel("1ms", SwingConstants.CENTER);
        horizontalRangeLabel.setFont(Style.FONT_TEXT);
        horizontalRangeLabel.setMinimumSize(new Dimension(50, horizontalRangeLabel.getPreferredSize().height));
        horizontalRangeLabel.setPreferredSize(new Dimension(50, horizontalRangeLabel.getPreferredSize().height));
        horizontalRangeLabel.setMaximumSize(new Dimension(50, horizontalRangeLabel.getPreferredSize().height));

        ClickLabel rightHR = new ClickLabel(">", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT);
        rightHR.setFont(Style.FONT_BIG_ICON);
        rightHR.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (indexRangeTimeDivision == HorizontalDivision.getAllValues().length - 1) {
                    return;
                }
                indexRangeTimeDivision += 1;
                horizontalRangeLabel.setText(getTimeDivisionDisplay());
                controller.channelSettingsChanged();

                controller.sendToDevice((short) 4, indexRangeTimeDivision);
            }
        });

        horizontalDivisionPane.add(hrTitle);
        horizontalDivisionPane.add(Box.createHorizontalStrut(34));
        horizontalDivisionPane.add(leftHR);
        horizontalDivisionPane.add(Box.createHorizontalStrut(5));
        horizontalDivisionPane.add(horizontalRangeLabel);
        horizontalDivisionPane.add(Box.createHorizontalStrut(5));
        horizontalDivisionPane.add(rightHR);
    }

    /**
     * Init and layout the vertical division pane components. The voltage division is set by
     * clicking on left and right arrows, changing to the previous or next value as listed in the
     * VerticalDivisionEnum.
     *
     * @param verticalDivisionPane The panel containing the controls for the vertical division.
     */
    private void initVerticalDivisionPaneComponents(JPanel verticalDivisionPane) {
        JLabel vrTitle = new JLabel("Voltage division ");
        vrTitle.setFont(Style.FONT_TEXT);
        ClickLabel leftVR = new ClickLabel("<", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT);
        verticalRangeLabel = new JLabel("1v", SwingConstants.CENTER);
        verticalRangeLabel.setFont(Style.FONT_TEXT);
        verticalRangeLabel.setMinimumSize(new Dimension(50, verticalRangeLabel.getPreferredSize().height));
        verticalRangeLabel.setPreferredSize(new Dimension(50, verticalRangeLabel.getPreferredSize().height));
        verticalRangeLabel.setMaximumSize(new Dimension(50, verticalRangeLabel.getPreferredSize().height));
        ClickLabel rightVR = new ClickLabel(">", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT);


        leftVR.setFont(Style.FONT_BIG_ICON);
        leftVR.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (indexRangeVoltageDivision == 0) {
                    return;
                }
                indexRangeVoltageDivision += -1;
                verticalRangeLabel.setText(getVoltageDivisionDisplay());
                controller.channelSettingsChanged();

                controller.sendToDevice((short) 3, indexRangeVoltageDivision);
            }
        });

        rightVR.setFont(Style.FONT_BIG_ICON);
        rightVR.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (indexRangeVoltageDivision == VerticalDivision.getAllValues().length - 1) {
                    return;
                }
                indexRangeVoltageDivision += 1;
                verticalRangeLabel.setText(getVoltageDivisionDisplay());
                controller.channelSettingsChanged();

                controller.sendToDevice((short) 3, indexRangeVoltageDivision);
            }
        });

        verticalDivisionPane.add(vrTitle);
        verticalDivisionPane.add(Box.createHorizontalStrut(20));
        verticalDivisionPane.add(leftVR);
        verticalDivisionPane.add(Box.createHorizontalStrut(5));
        verticalDivisionPane.add(verticalRangeLabel);
        verticalDivisionPane.add(Box.createHorizontalStrut(5));
        verticalDivisionPane.add(rightVR);
    }

    /**
     * Init and layout the channel visibility pane components.
     *
     * A channel is made visible by ticking a check box. A is initially selected.
     *
     * @param channelVisibilityPane The panel containing the controls for the channel visibility.
     */
    private void initChannelVisibilityPaneComponents(JPanel channelVisibilityPane) {
        JCheckBox chAVisibleCB = new JCheckBox("A");
        chAVisibleCB.setFont(Style.FONT_TEXT);
        chAVisibleCB.setSelected(true);
        chAVisibleCB.addItemListener(e -> {
            channelASelected = e.getStateChange() == ItemEvent.SELECTED;
            controller.channelSettingsChanged();
        });
        JCheckBox chBVisibleCB = new JCheckBox("B");
        chBVisibleCB.setFont(Style.FONT_TEXT);
        chBVisibleCB.addItemListener(e -> {
            channelBSelected = (e.getStateChange() == ItemEvent.SELECTED);
            controller.channelSettingsChanged();
        });
        JCheckBox chMathVisibleCB = new JCheckBox("Math");
        chMathVisibleCB.setFont(Style.FONT_TEXT);
        chMathVisibleCB.addItemListener(e -> {
            channelMathSelected = (e.getStateChange() == ItemEvent.SELECTED);
            controller.channelSettingsChanged();
        });
        JCheckBox chFilterVisibleCB = new JCheckBox("Filter");
        chFilterVisibleCB.setFont(Style.FONT_TEXT);
        chFilterVisibleCB.addItemListener(e -> {
            channelFilterSelected = (e.getStateChange() == ItemEvent.SELECTED);
            controller.channelSettingsChanged();
        });

        channelVisibilityPane.add(chAVisibleCB);
        channelVisibilityPane.add(Box.createHorizontalStrut(15));
        channelVisibilityPane.add(chBVisibleCB);
        channelVisibilityPane.add(Box.createHorizontalStrut(15));
        channelVisibilityPane.add(chMathVisibleCB);
        channelVisibilityPane.add(Box.createHorizontalStrut(15));
        channelVisibilityPane.add(chFilterVisibleCB);
    }

    /**
     * Init and layout the channel offset pane components.
     *
     * The offset can only be set is DC coupling is selected. They are initially disabled.
     *
     * On setting the math and filter channel offset, the math and filter channels are automatically
     * redrawn.
     *
     * @param channelOffsetPane The panel containing the controls for setting the channels offset.
     */
    private void initChannelOffsetPaneComponents(JPanel channelOffsetPane) {
        JLabel chABOffset = new JLabel("A & B");
        chABOffset.setFont(Style.FONT_TEXT);
        JLabel chMathOffset = new JLabel("Math");
        chMathOffset.setFont(Style.FONT_TEXT);
        JLabel chFilterOffset = new JLabel("Filter");
        chFilterOffset.setFont(Style.FONT_TEXT);

        chABOffsetTF = new JTextField("", 2);
        chABOffsetTF.setFont(Style.FONT_TEXT);
        chABOffsetTF.addActionListener(e -> {
            int offset = 0;
            try {
                offset = Integer.parseInt(chABOffsetTF.getText());
            } catch (NumberFormatException nfe) {
                chABOffsetTF.setText("");
                return;
            }

            controller.sendToDevice((short) 2, offset);
        });
        chABOffsetTF.setEnabled(false);

        chMathOffsetTF = new JTextField("", 2);
        chMathOffsetTF.setFont(Style.FONT_TEXT);
        chMathOffsetTF.addActionListener(e -> {
            String offset = chMathOffsetTF.getText();
            if (offset.equals("")) {
                this.channelMathOffset = 0;
                return;
            }
            try {
                this.channelMathOffset = Double.parseDouble(offset);
            } catch (NumberFormatException nfe) {
                chMathOffsetTF.setText("");
                this.channelMathOffset = 0;
            }

            controller.channelSettingsChanged();
        });
        chMathOffsetTF.setEnabled(false);

        chFilterOffsetTF = new JTextField("", 2);
        chFilterOffsetTF.setFont(Style.FONT_TEXT);
        chFilterOffsetTF.addActionListener(e -> {
            String offset = chFilterOffsetTF.getText();
            if (offset.equals("")) {
                this.channelFilterOffset = 0;
                return;
            }
            try {
                this.channelFilterOffset = Double.parseDouble(offset);
            } catch (NumberFormatException nfe) {
                chFilterOffsetTF.setText("");
                this.channelFilterOffset = 0;
            }

            controller.channelSettingsChanged();
        });
        chFilterOffsetTF.setEnabled(false);

        channelOffsetPane.add(chABOffset);
        channelOffsetPane.add(chABOffsetTF);
        channelOffsetPane.add(Box.createHorizontalStrut(10));
        channelOffsetPane.add(chMathOffset);
        channelOffsetPane.add(chMathOffsetTF);
        channelOffsetPane.add(Box.createHorizontalStrut(10));
        channelOffsetPane.add(chFilterOffset);
        channelOffsetPane.add(chFilterOffsetTF);
    }

    /**
     * Init and layout the math channel pane components.
     *
     * When an equation is input by the user, some input validation is done while adding each
     * element of the formula to a string array to ease the processing of the equation  when values
     * for the math channel are calculated.
     *
     * @param mathChannelPane The panel containing the controls for the Math channel
     */
    private void initMathChannelPaneComponents(JPanel mathChannelPane) {
        JLabel eTitle = new JLabel("Equation");
        eTitle.setFont(Style.FONT_TEXT);

        JTextField equationTF = new JTextField("");
        equationTF.setFont(Style.FONT_TEXT);
        equationTF.addActionListener(e -> {
            String eq = equationTF.getText();
            // getEquationArray returns null if the formula input by the user is invalid
            ArrayList<String> equationArray = getEquationArray(eq.replaceAll("\\s", ""));
            if (equationArray != null) {
                equationTF.setText(eq);
                equationTF.setForeground(Color.BLACK);
                // when setting the equation, a boolean is returned indicating if the math
                // channel can be selected as input for the filter channel.
                boolean selectableForFilter = controller.setEquation(equationArray);

                String currentSelection = (String) channelChoicesCB.getSelectedItem();

                if (selectableForFilter && channelChoicesCB.getItemCount() == 3) {
                    // Do nothing
                } else if (selectableForFilter && channelChoicesCB.getItemCount() == 2) {
                    String[] channelChoices = {"A", "B", "Math"};
                    channelChoicesCB.setModel(new DefaultComboBoxModel(channelChoices));
                    channelChoicesCB.setSelectedItem(currentSelection);
                } else {
                    String[] channelChoices = {"A", "B"};
                    channelChoicesCB.setModel(new DefaultComboBoxModel(channelChoices));
                }
            } else {
                equationTF.setForeground(Style.COLOR_ERROR_RED);
            }
        });

        mathChannelPane.add(eTitle);
        mathChannelPane.add(Box.createHorizontalStrut(10));
        mathChannelPane.add(equationTF);
    }

    /**
     * Init and layout the filter channel pane components.
     *
     * When a user selects a file, the file is sent to the controller for validation, extracting the
     * coefficients from the file and return the type of filter (IIR or FIR.
     *
     * @param filterChannelPane The panel containing the controls for the filter channel
     */
    private void initFilterChannelInputPaneComponents(JPanel filterChannelPane) {
        JLabel fiTitle = new JLabel("Filter input");
        fiTitle.setFont(Style.FONT_TEXT);
        String[] channelChoices = {"A", "B"};
        channelChoicesCB = new JComboBox(channelChoices);
        channelChoicesCB.setMinimumSize(new Dimension(90, channelChoicesCB.getPreferredSize().height));
        channelChoicesCB.setPreferredSize(new Dimension(90, channelChoicesCB.getPreferredSize().height));
        channelChoicesCB.setMaximumSize(new Dimension(90, channelChoicesCB.getPreferredSize().height));
        channelChoicesCB.setFont(Style.FONT_TEXT);
        channelChoicesCB.addActionListener(e ->
                controller.setInputFilterChannel((String) channelChoicesCB.getSelectedItem()));

        filterChannelPane.add(fiTitle);
        filterChannelPane.add(Box.createHorizontalStrut(10));
        filterChannelPane.add(channelChoicesCB);
    }

    // --------------------------------------------------------------------------------------//
    // --------------------------------- SETTERS --------------------------------------------//
    // --------------------------------------------------------------------------------------//

    public void setHorizontalDivision(int index) {
        indexRangeTimeDivision = index;
        horizontalRangeLabel.setText(getTimeDivisionDisplay());
    }

    public void setVerticalDivision(int index) {
        indexRangeVoltageDivision = index;
        verticalRangeLabel.setText(getVoltageDivisionDisplay());
    }

    public void setChannelCouplingCB(int index) {
        setFromDevice = true;
        channelCouplingCB.setSelectedIndex(index);
    }

    public void setController(DigiscopePaneController controller) {
        this.controller = controller;
    }

    // --------------------------------------------------------------------------------------//
    // ------------------------------- GETTERS ----------------------------------------------//
    // --------------------------------------------------------------------------------------//


    public int getTimeDivision() {
        return HorizontalDivision.values()[indexRangeTimeDivision].getValue();
    }

    public int getVoltageDivision() {
        return VerticalDivision.values()[indexRangeVoltageDivision].getValue();
    }

    public boolean isChannelFilterSelected() {
        return channelFilterSelected;
    }

    public boolean isChannelASelected() {
        return channelASelected;
    }

    public boolean isChannelBSelected() {
        return channelBSelected;
    }

    public boolean isChannelMathSelected() {
        return channelMathSelected;
    }

    public double getChannelMathOffset() {
        return channelMathOffset;
    }

    public double getChannelFilterOffset() {
        return channelFilterOffset;
    }

    public boolean isBandpassFilterActive() {
        return bandpassFilterActive;
    }

    public JPanel getControlPanel() {
        return controlPanel;
    }

}
