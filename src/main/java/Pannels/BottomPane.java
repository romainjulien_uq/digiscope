package Pannels;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import Connection.ConnectionHandler;
import Controller.DigiscopePaneController;
import Settings.BitModeEnum;
import Settings.DeviceStatusEnum;
import Settings.FuncGenOutputEnum;
import Settings.FuncGenWaveTypeEnum;
import Settings.TriggerModeEnum;
import Settings.TriggerTypeEnum;
import style.ClickLabel;
import style.Style;

/**
 * This panel contains most of the device settings and status (all except coupling, offset,
 * division)
 */
public class BottomPane {

    // The digiscope controller
    private DigiscopePaneController controller;

    // The main panel, contain all components of this view
    private JPanel panel;

    // The title row contains the button to reveal/hide this block
    private JPanel titleRow;

    // The main Pane, where the settings for the trigger, sampling and func. gen. can be changed
    // and sent to the device
    private JPanel controlPane;

    // A control to hide and show the panel
    public ClickLabel hideShowControl;

    // A flag indicating if the panel is currently shown
    private boolean panelIsShown = false;

    // Set to give indication about the connection, or attempt to connect, with the device.
    private JLabel connectionStatus;

    // The device state, possible options are listed in DeviceStatusEnum
    private JLabel deviceStatus;

    // The current sampling rate
    private JLabel samplingRate;

    // The current trigger mode, possible options are listed in TriggerModeEnum
    private JComboBox triggerModeCB;

    // The current trigger type, possible options are listed in TriggerTypeEnum
    private JComboBox triggerTypeCB;

    // The current trigger threshold
    private JTextField triggerThresholdTF;

    // The setting for the function generator output, possible options are listed
    // in FuncGenOutputEnum
    private JComboBox funcGenOutputCB;

    // The setting for the function generator wave type, possible options are listed
    // in FuncGenWaveTypeEnum
    private JComboBox funcGenWaveTypesCB;

    // The setting for the function generator peak-to-peak voltage
    private JTextField funcGenP2PTF;

    // The setting for the function generator offset
    private JTextField funcGenOTF;

    // The setting for the function generator frequency
    private JTextField funcGenFrequencyTF;

    // A flag avoiding a setting coming from the device triggers a change to be sent to the device.
    private boolean setFromDevice = false;

    private static BottomPane ourInstance = new BottomPane();

    public static BottomPane getInstance() {
        return ourInstance;
    }

    public BottomPane() {
        panel = new JPanel();
        BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);
        panel.setBorder(new EmptyBorder(10, 0, 0, 0));
        panel.setOpaque(false);

        addComponents();
    }

    /* ---------------------------------------------------------------------------------------
    ------------------------------------- HELPER METHODS -------------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Take a String as input and attempt to open the connection with the device. Display an error
     * if the connection couldn't be established.
     *
     * @param ip The ip address to connect to.
     */
    private void setIPAddress(String ip) {
        String error = ConnectionHandler.getInstance().openConnection(ip);
        KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

        if (error != null) {
            connectionStatus.setText(error);
        } else {
            connectionStatus.setText("Connected: Yes");
        }
    }

    /* ---------------------------------------------------------------------------------------
    -------------------------------- HELPER LAYOUT METHODS -----------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Init and layout all components of the Bottom panel.
     */
    private void addComponents() {
        Border panelBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

        // Init menu row
        titleRow = new JPanel(new BorderLayout());
        titleRow.setAlignmentX(Component.LEFT_ALIGNMENT);
        titleRow.setBackground(Style.COLOR_DARK_GREY);
        Border menuRowInsets = BorderFactory.createEmptyBorder(0, 5, 0, 5);
        titleRow.setBorder(BorderFactory.createCompoundBorder(panelBorder, menuRowInsets));

        initTitleRowComponents();

        // Init control pane
        controlPane = new JPanel();
        BoxLayout layout = new BoxLayout(controlPane, BoxLayout.X_AXIS);
        controlPane.setLayout(layout);
        controlPane.setPreferredSize(new Dimension(controlPane.getPreferredSize().width, 221));
        controlPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        controlPane.setBackground(Style.COLOR_DARK_GREY);
        Border configRowInsets = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        controlPane.setBorder(BorderFactory.createCompoundBorder(panelBorder, configRowInsets));

        initControlPaneComponents();

        // Only show menu row, control pane is initially hidden
        panel.add(titleRow);
    }

    /**
     * Init and layout the components of the top row.
     */
    private void initTitleRowComponents() {
        JLabel deviceLabel = new JLabel("Device:");
        deviceLabel.setFont(Style.FONT_TEXT_HIGHLIGHT);
        deviceLabel.setForeground(Style.COLOR_LIGHT_ORANGE);

        JTextField IPTextField = new JTextField("");
        IPTextField.setFont(Style.FONT_TEXT);
        IPTextField.setForeground(Style.COLOR_GREY);
        IPTextField.setPreferredSize(new Dimension(200, IPTextField.getPreferredSize().height));
        IPTextField.addActionListener(e -> setIPAddress(IPTextField.getText()));

        connectionStatus = new JLabel("Connected: No");
        connectionStatus.setFont(Style.FONT_TEXT);
        connectionStatus.setForeground(Style.COLOR_LIGHT_ORANGE);
        connectionStatus.setPreferredSize(new Dimension(200, connectionStatus.getPreferredSize().height));
        connectionStatus.setMinimumSize(new Dimension(200, connectionStatus.getPreferredSize().height));
        connectionStatus.setMaximumSize(new Dimension(200, connectionStatus.getPreferredSize().height));

        JLabel deviceStatusLabel = new JLabel("Device status: ");
        deviceStatusLabel.setFont(Style.FONT_TEXT_HIGHLIGHT);
        deviceStatusLabel.setForeground(Style.COLOR_LIGHT_ORANGE);

        deviceStatus = new JLabel(DeviceStatusEnum.ARMED.getValue());
        deviceStatus.setFont(Style.FONT_TEXT);
        deviceStatus.setForeground(Style.COLOR_LIGHT_ORANGE);
        deviceStatus.setPreferredSize(new Dimension(150, deviceStatus.getPreferredSize().height));
        deviceStatus.setMinimumSize(new Dimension(150, deviceStatus.getPreferredSize().height));
        deviceStatus.setMaximumSize(new Dimension(150, deviceStatus.getPreferredSize().height));

        JLabel samplingRateLabel = new JLabel("Current Sampling Rate: ");
        samplingRateLabel.setFont(Style.FONT_TEXT_HIGHLIGHT);
        samplingRateLabel.setForeground(Style.COLOR_LIGHT_ORANGE);

        samplingRate = new JLabel("1000000 S/s");
        samplingRate.setFont(Style.FONT_TEXT);
        samplingRate.setForeground(Style.COLOR_LIGHT_ORANGE);
        samplingRate.setPreferredSize(new Dimension(150, samplingRate.getPreferredSize().height));
        samplingRate.setMinimumSize(new Dimension(150, samplingRate.getPreferredSize().height));
        samplingRate.setMaximumSize(new Dimension(150, samplingRate.getPreferredSize().height));

        JPanel horizontalWrapper = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
        horizontalWrapper.setAlignmentX(FlowLayout.LEFT);
        horizontalWrapper.setOpaque(false);
        horizontalWrapper.add(deviceLabel);
        horizontalWrapper.add(IPTextField);
        horizontalWrapper.add(Box.createHorizontalStrut(5));
        horizontalWrapper.add(connectionStatus);
        horizontalWrapper.add(Box.createHorizontalStrut(10));
        horizontalWrapper.add(deviceStatusLabel);
        horizontalWrapper.add(deviceStatus);
        horizontalWrapper.add(Box.createHorizontalStrut(10));
        horizontalWrapper.add(samplingRateLabel);
        horizontalWrapper.add(samplingRate);

        createHideShowControl();

        titleRow.add(horizontalWrapper, BorderLayout.WEST);
        titleRow.add(hideShowControl, BorderLayout.EAST);

        KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
    }

    /**
     * Init and layout the trigger panel, sampling panel, and function generator panel.
     */
    private void initControlPaneComponents() {
        JPanel triggerPane = new JPanel();
        BoxLayout layout = new BoxLayout(triggerPane, BoxLayout.Y_AXIS);
        triggerPane.setLayout(layout);
        triggerPane.setOpaque(false);

        initTriggerPaneComponents(triggerPane);

        JPanel samplingPane = new JPanel();
        BoxLayout layoutSamplingPane = new BoxLayout(samplingPane, BoxLayout.Y_AXIS);
        samplingPane.setLayout(layoutSamplingPane);
        samplingPane.setOpaque(false);

        initSamplingPaneComponents(samplingPane);

        JPanel funcGenPane = new JPanel();
        BoxLayout layoutFGP = new BoxLayout(funcGenPane, BoxLayout.Y_AXIS);
        funcGenPane.setLayout(layoutFGP);
        funcGenPane.setOpaque(false);

        initFunctionGeneratorPane(funcGenPane);

        controlPane.add(triggerPane);
        JSeparator separatorTrigger = new JSeparator(SwingConstants.VERTICAL);
        separatorTrigger.setForeground(Style.COLOR_GREY);
        controlPane.add(Box.createHorizontalStrut(5));
        controlPane.add(separatorTrigger);
        controlPane.add(samplingPane);
        JSeparator separatorSampling = new JSeparator(SwingConstants.VERTICAL);
        separatorSampling.setForeground(Style.COLOR_GREY);
        controlPane.add(Box.createHorizontalStrut(5));
        controlPane.add(separatorSampling);
        controlPane.add(funcGenPane);

    }

    /**
     * Init and layout the components of the trigger panel. Add action listeners so that any changes
     * in the settings is immediately sent to the device.
     *
     * @param triggerPane The panel containing trigger-related settings.
     */
    private void initTriggerPaneComponents(JPanel triggerPane) {
        JPanel triggerTitleWrapper = new JPanel();
        BoxLayout layoutTTW = new BoxLayout(triggerTitleWrapper, BoxLayout.X_AXIS);
        triggerTitleWrapper.setLayout(layoutTTW);
        triggerTitleWrapper.setOpaque(false);

        JLabel triggerTitle = new JLabel("Trigger");
        triggerTitle.setFont(Style.FONT_TITLE);
        triggerTitle.setForeground(Style.COLOR_LIGHT_GREY);

        JButton forceTriggerB = new JButton("Force Trigger");
        forceTriggerB.setFont(Style.FONT_TEXT);
        forceTriggerB.addActionListener(e -> controller.sendToDevice((short) 8, 0));

        JButton rearmTriggerB = new JButton("Rearm Trigger");
        rearmTriggerB.setFont(Style.FONT_TEXT);
        rearmTriggerB.addActionListener(e -> controller.sendToDevice((short) 9, 0));
        rearmTriggerB.setEnabled(false);

        triggerTitleWrapper.add(triggerTitle);
        triggerTitleWrapper.add(Box.createHorizontalStrut(60));
        triggerTitleWrapper.add(forceTriggerB);
        triggerTitleWrapper.add(Box.createHorizontalStrut(10));
        triggerTitleWrapper.add(rearmTriggerB);
        triggerTitleWrapper.add(Box.createHorizontalStrut(10));

        JPanel triggerModeWrapper = new JPanel();
        BoxLayout layoutTMW = new BoxLayout(triggerModeWrapper, BoxLayout.X_AXIS);
        triggerModeWrapper.setLayout(layoutTMW);
        triggerModeWrapper.setOpaque(false);

        JLabel triggerModeTitle = new JLabel("Trigger Mode ");
        triggerModeTitle.setFont(Style.FONT_TEXT);
        triggerModeTitle.setForeground(Style.COLOR_LIGHT_GREY);
        triggerModeCB = new JComboBox(TriggerModeEnum.getAllValues());
        triggerModeCB.setMinimumSize(new Dimension(100, triggerModeCB.getPreferredSize().height));
        triggerModeCB.setPreferredSize(new Dimension(100, triggerModeCB.getPreferredSize().height));
        triggerModeCB.setMaximumSize(new Dimension(100, triggerModeCB.getPreferredSize().height));
        triggerModeCB.setFont(Style.FONT_TEXT);
        triggerModeCB.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int index = triggerModeCB.getSelectedIndex();
            if (index == -1) return;
            TriggerModeEnum mode = TriggerModeEnum.values()[index];
            if (mode == TriggerModeEnum.SINGLE) {
                rearmTriggerB.setEnabled(true);
            } else {
                rearmTriggerB.setEnabled(false);
            }

            controller.sendToDevice((short) 5, index);
        });

        triggerModeWrapper.add(triggerModeTitle);
        triggerModeWrapper.add(Box.createHorizontalStrut(35));
        triggerModeWrapper.add(triggerModeCB);

        JPanel triggerTypeWrapper = new JPanel();
        BoxLayout layoutTTyW = new BoxLayout(triggerTypeWrapper, BoxLayout.X_AXIS);
        triggerTypeWrapper.setLayout(layoutTTyW);
        triggerTypeWrapper.setOpaque(false);

        JLabel triggerTypeTitle = new JLabel("Trigger Type ");
        triggerTypeTitle.setFont(Style.FONT_TEXT);
        triggerTypeTitle.setForeground(Style.COLOR_LIGHT_GREY);
        triggerTypeCB = new JComboBox(TriggerTypeEnum.getAllValues());
        triggerTypeCB.setMinimumSize(new Dimension(100, triggerTypeCB.getPreferredSize().height));
        triggerTypeCB.setPreferredSize(new Dimension(100, triggerTypeCB.getPreferredSize().height));
        triggerTypeCB.setMaximumSize(new Dimension(100, triggerTypeCB.getPreferredSize().height));
        triggerTypeCB.setFont(Style.FONT_TEXT);
        triggerTypeCB.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int index = triggerModeCB.getSelectedIndex();
            if (index == -1) return;

            controller.sendToDevice((short) 6, index);
        });

        triggerTypeWrapper.add(triggerTypeTitle);
        triggerTypeWrapper.add(Box.createHorizontalStrut(40));
        triggerTypeWrapper.add(triggerTypeCB);

        JPanel triggerThresholdWrapper = new JPanel();
        BoxLayout layoutTThW = new BoxLayout(triggerThresholdWrapper, BoxLayout.X_AXIS);
        triggerThresholdWrapper.setLayout(layoutTThW);
        triggerThresholdWrapper.setOpaque(false);

        JLabel triggerThresholdTitle = new JLabel("Trigger Threshold ");
        triggerThresholdTitle.setFont(Style.FONT_TEXT);
        triggerThresholdTitle.setForeground(Style.COLOR_LIGHT_GREY);
        triggerThresholdTF = new JTextField("");
        triggerThresholdTF.setFont(Style.FONT_TEXT);
        triggerThresholdTF.setMinimumSize(new Dimension(100, triggerTypeCB.getPreferredSize().height));
        triggerThresholdTF.setPreferredSize(new Dimension(100, triggerTypeCB.getPreferredSize().height));
        triggerThresholdTF.setMaximumSize(new Dimension(100, triggerTypeCB.getPreferredSize().height));
        triggerThresholdTF.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int threshold = 0;
            try {
                threshold = Integer.parseInt(triggerThresholdTF.getText());
            } catch (NumberFormatException nfe) {
                triggerThresholdTF.setText("");
                return;
            }

            controller.sendToDevice((short) 7, threshold);
        });

        triggerThresholdWrapper.add(triggerThresholdTitle);
        triggerThresholdWrapper.add(Box.createHorizontalStrut(10));
        triggerThresholdWrapper.add(triggerThresholdTF);

        triggerTitleWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        triggerPane.add(triggerTitleWrapper);
        triggerPane.add(Box.createVerticalStrut(10));
        triggerModeWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        triggerPane.add(triggerModeWrapper);
        triggerPane.add(Box.createVerticalStrut(10));
        triggerTypeWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        triggerPane.add(triggerTypeWrapper);
        triggerPane.add(Box.createVerticalStrut(10));
        triggerThresholdWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        triggerPane.add(triggerThresholdWrapper);
        triggerPane.add(Box.createVerticalStrut(10));
        triggerPane.add(Box.createVerticalStrut(30));
        triggerPane.add(Box.createVerticalStrut(10));
        triggerPane.add(Box.createVerticalStrut(30));
    }

    /**
     * Init and layout the components of the sampling panel. Add action listeners so that any
     * changes in the settings is immediately sent to the device.
     *
     * @param samplingPane The panel containing sampling-related settings.
     */
    private void initSamplingPaneComponents(JPanel samplingPane) {
        JLabel samplingTitle = new JLabel("Sampling");
        samplingTitle.setFont(Style.FONT_TITLE);
        samplingTitle.setForeground(Style.COLOR_LIGHT_GREY);

        JPanel samplingModeWrapper = new JPanel();
        BoxLayout layoutSMW = new BoxLayout(samplingModeWrapper, BoxLayout.X_AXIS);
        samplingModeWrapper.setLayout(layoutSMW);
        samplingModeWrapper.setOpaque(false);

        JLabel samplingModeTitle = new JLabel("Sampling Mode ");
        samplingModeTitle.setFont(Style.FONT_TEXT);
        samplingModeTitle.setForeground(Style.COLOR_LIGHT_GREY);
        JComboBox samplingModeCB = new JComboBox(BitModeEnum.getAllValues());
        samplingModeCB.setMinimumSize(new Dimension(100, samplingModeCB.getPreferredSize().height));
        samplingModeCB.setPreferredSize(new Dimension(100, samplingModeCB.getPreferredSize().height));
        samplingModeCB.setMaximumSize(new Dimension(100, samplingModeCB.getPreferredSize().height));
        samplingModeCB.setFont(Style.FONT_TEXT);
        samplingModeCB.addActionListener(e -> {
            int index = samplingModeCB.getSelectedIndex();
            if (index == -1) return;

            controller.sendToDevice((short) 16, index);
        });
        samplingModeCB.setSelectedIndex(-1);

        samplingModeWrapper.add(samplingModeTitle);
        samplingModeWrapper.add(Box.createHorizontalStrut(90));
        samplingModeWrapper.add(samplingModeCB);

        JPanel samplingNbrWrapper = new JPanel();
        BoxLayout layoutSNW = new BoxLayout(samplingNbrWrapper, BoxLayout.X_AXIS);
        samplingNbrWrapper.setLayout(layoutSNW);
        samplingNbrWrapper.setOpaque(false);

        JLabel nbrSampleTitle = new JLabel("Number Of Samples To Acquire ");
        nbrSampleTitle.setFont(Style.FONT_TEXT);
        nbrSampleTitle.setForeground(Style.COLOR_LIGHT_GREY);
        JTextField nbrSampleTF = new JTextField("");
        nbrSampleTF.setFont(Style.FONT_TEXT);
        nbrSampleTF.setMinimumSize(new Dimension(100, nbrSampleTF.getPreferredSize().height));
        nbrSampleTF.setPreferredSize(new Dimension(100, nbrSampleTF.getPreferredSize().height));
        nbrSampleTF.setMaximumSize(new Dimension(100, nbrSampleTF.getPreferredSize().height));
        nbrSampleTF.addActionListener(e -> {
            int nbrSamples = 0;
            try {
                nbrSamples = Integer.parseInt(nbrSampleTF.getText());
            } catch (NumberFormatException nfe) {
                nbrSampleTF.setText("");
                return;
            }

            controller.sendToDevice((short) 10, nbrSamples);
        });

        samplingNbrWrapper.add(nbrSampleTitle);
        samplingNbrWrapper.add(Box.createHorizontalStrut(10));
        samplingNbrWrapper.add(nbrSampleTF);

        samplingTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        samplingPane.add(samplingTitle);
        samplingPane.add(Box.createVerticalStrut(10));
        samplingModeWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        samplingPane.add(samplingModeWrapper);
        samplingPane.add(Box.createVerticalStrut(10));
        samplingNbrWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        samplingPane.add(samplingNbrWrapper);
        samplingPane.add(Box.createVerticalStrut(10));
        samplingPane.add(Box.createVerticalStrut(30));
        samplingPane.add(Box.createVerticalStrut(10));
        samplingPane.add(Box.createVerticalStrut(30));
        samplingPane.add(Box.createVerticalStrut(10));
        samplingPane.add(Box.createVerticalStrut(30));
    }

    /**
     * Init and layout the components of the function generator panel. Add action listeners so that
     * any changes in the settings is immediately sent to the device.
     *
     * @param funcGenPane The panel containing function generatorn related settings.
     */
    private void initFunctionGeneratorPane(JPanel funcGenPane) {
        JLabel funcGenTitle = new JLabel("Function Generator");
        funcGenTitle.setFont(Style.FONT_TITLE);
        funcGenTitle.setForeground(Style.COLOR_LIGHT_GREY);

        JPanel funcGenOutputWrapper = new JPanel();
        BoxLayout layoutFGOW = new BoxLayout(funcGenOutputWrapper, BoxLayout.X_AXIS);
        funcGenOutputWrapper.setLayout(layoutFGOW);
        funcGenOutputWrapper.setOpaque(false);

        JLabel funcGenOutputTitle = new JLabel("Output ");
        funcGenOutputTitle.setFont(Style.FONT_TEXT);
        funcGenOutputTitle.setForeground(Style.COLOR_LIGHT_GREY);
        funcGenOutputCB = new JComboBox(FuncGenOutputEnum.getAllValues());
        funcGenOutputCB.setMinimumSize(new Dimension(100, funcGenOutputCB.getPreferredSize().height));
        funcGenOutputCB.setPreferredSize(new Dimension(100, funcGenOutputCB.getPreferredSize().height));
        funcGenOutputCB.setMaximumSize(new Dimension(100, funcGenOutputCB.getPreferredSize().height));
        funcGenOutputCB.setFont(Style.FONT_TEXT);
        funcGenOutputCB.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int index = funcGenOutputCB.getSelectedIndex();
            if (index == -1) return;

            controller.sendToDevice((short) 11, index);
        });
        funcGenOutputCB.setSelectedIndex(-1);

        funcGenOutputWrapper.add(funcGenOutputTitle);
        funcGenOutputWrapper.add(Box.createHorizontalStrut(90));
        funcGenOutputWrapper.add(funcGenOutputCB);

        JPanel funcGenWaveTypeWrapper = new JPanel();
        BoxLayout layoutFGWTW = new BoxLayout(funcGenWaveTypeWrapper, BoxLayout.X_AXIS);
        funcGenWaveTypeWrapper.setLayout(layoutFGWTW);
        funcGenWaveTypeWrapper.setOpaque(false);

        JLabel funcGenWaveTypeTitle = new JLabel("Wave Type ");
        funcGenWaveTypeTitle.setFont(Style.FONT_TEXT);
        funcGenWaveTypeTitle.setForeground(Style.COLOR_LIGHT_GREY);
        funcGenWaveTypesCB = new JComboBox(FuncGenWaveTypeEnum.getAllValues());
        funcGenWaveTypesCB.setMinimumSize(new Dimension(100, funcGenWaveTypesCB.getPreferredSize().height));
        funcGenWaveTypesCB.setPreferredSize(new Dimension(100, funcGenWaveTypesCB.getPreferredSize().height));
        funcGenWaveTypesCB.setMaximumSize(new Dimension(100, funcGenWaveTypesCB.getPreferredSize().height));
        funcGenWaveTypesCB.setFont(Style.FONT_TEXT);
        funcGenWaveTypesCB.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int index = funcGenWaveTypesCB.getSelectedIndex();
            if (index == -1) return;

            controller.sendToDevice((short) 12, index);
        });
        funcGenWaveTypesCB.setSelectedIndex(-1);

        funcGenWaveTypeWrapper.add(funcGenWaveTypeTitle);
        funcGenWaveTypeWrapper.add(Box.createHorizontalStrut(70));
        funcGenWaveTypeWrapper.add(funcGenWaveTypesCB);

        JPanel funcGenP2PWrapper = new JPanel();
        BoxLayout layoutFGP2PW = new BoxLayout(funcGenP2PWrapper, BoxLayout.X_AXIS);
        funcGenP2PWrapper.setLayout(layoutFGP2PW);
        funcGenP2PWrapper.setOpaque(false);

        JLabel funcGenP2PTitle = new JLabel("Peak-To-Peak Voltage ");
        funcGenP2PTitle.setFont(Style.FONT_TEXT);
        funcGenP2PTitle.setForeground(Style.COLOR_LIGHT_GREY);
        funcGenP2PTF = new JTextField("");
        funcGenP2PTF.setFont(Style.FONT_TEXT);
        funcGenP2PTF.setMinimumSize(new Dimension(100, funcGenP2PTF.getPreferredSize().height));
        funcGenP2PTF.setPreferredSize(new Dimension(100, funcGenP2PTF.getPreferredSize().height));
        funcGenP2PTF.setMaximumSize(new Dimension(100, funcGenP2PTF.getPreferredSize().height));
        funcGenP2PTF.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int p2p = 0;
            try {
                p2p = Integer.parseInt(funcGenP2PTF.getText());
            } catch (NumberFormatException nfe) {
                funcGenP2PTF.setText("");
                return;
            }

            controller.sendToDevice((short) 13, p2p);
        });

        funcGenP2PWrapper.add(funcGenP2PTitle);
        funcGenP2PWrapper.add(Box.createHorizontalStrut(10));
        funcGenP2PWrapper.add(funcGenP2PTF);

        JPanel funcGenOffsetWrapper = new JPanel();
        BoxLayout layoutFGOffW = new BoxLayout(funcGenOffsetWrapper, BoxLayout.X_AXIS);
        funcGenOffsetWrapper.setLayout(layoutFGOffW);
        funcGenOffsetWrapper.setOpaque(false);

        JLabel funcGenOffsetTitle = new JLabel("Offset ");
        funcGenOffsetTitle.setFont(Style.FONT_TEXT);
        funcGenOffsetTitle.setForeground(Style.COLOR_LIGHT_GREY);
        funcGenOTF = new JTextField("");
        funcGenOTF.setFont(Style.FONT_TEXT);
        funcGenOTF.setMinimumSize(new Dimension(100, funcGenOTF.getPreferredSize().height));
        funcGenOTF.setPreferredSize(new Dimension(100, funcGenOTF.getPreferredSize().height));
        funcGenOTF.setMaximumSize(new Dimension(100, funcGenOTF.getPreferredSize().height));
        funcGenOTF.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int offset = 0;
            try {
                offset = Integer.parseInt(funcGenOTF.getText());
            } catch (NumberFormatException nfe) {
                funcGenOTF.setText("");
                return;
            }

            controller.sendToDevice((short) 14, offset);
        });

        funcGenOffsetWrapper.add(funcGenOffsetTitle);
        funcGenOffsetWrapper.add(Box.createHorizontalStrut(95));
        funcGenOffsetWrapper.add(funcGenOTF);

        JPanel funcGenFrequencyWrapper = new JPanel();
        BoxLayout layoutFGFW = new BoxLayout(funcGenFrequencyWrapper, BoxLayout.X_AXIS);
        funcGenFrequencyWrapper.setLayout(layoutFGFW);
        funcGenFrequencyWrapper.setOpaque(false);

        JLabel funcGenFrequencyTitle = new JLabel("Frequency ");
        funcGenFrequencyTitle.setFont(Style.FONT_TEXT);
        funcGenFrequencyTitle.setForeground(Style.COLOR_LIGHT_GREY);
        funcGenFrequencyTF = new JTextField("");
        funcGenFrequencyTF.setFont(Style.FONT_TEXT);
        funcGenFrequencyTF.setMinimumSize(new Dimension(100, funcGenFrequencyTF.getPreferredSize().height));
        funcGenFrequencyTF.setPreferredSize(new Dimension(100, funcGenFrequencyTF.getPreferredSize().height));
        funcGenFrequencyTF.setMaximumSize(new Dimension(100, funcGenFrequencyTF.getPreferredSize().height));
        funcGenFrequencyTF.addActionListener(e -> {
            if (setFromDevice) {
                setFromDevice = false;
                return;
            }

            int frequency = 0;
            try {
                frequency = Integer.parseInt(funcGenFrequencyTF.getText());
            } catch (NumberFormatException nfe) {
                funcGenFrequencyTF.setText("");
                return;
            }

            controller.sendToDevice((short) 15, frequency);
        });

        funcGenFrequencyWrapper.add(funcGenFrequencyTitle);
        funcGenFrequencyWrapper.add(Box.createHorizontalStrut(75));
        funcGenFrequencyWrapper.add(funcGenFrequencyTF);

        funcGenTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        funcGenPane.add(funcGenTitle);
        funcGenPane.add(Box.createVerticalStrut(10));
        funcGenOutputWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        funcGenPane.add(funcGenOutputWrapper);
        funcGenPane.add(Box.createVerticalStrut(10));
        funcGenWaveTypeWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        funcGenPane.add(funcGenWaveTypeWrapper);
        funcGenPane.add(Box.createVerticalStrut(10));
        funcGenP2PWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        funcGenPane.add(funcGenP2PWrapper);
        funcGenPane.add(Box.createVerticalStrut(10));
        funcGenOffsetWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        funcGenPane.add(funcGenOffsetWrapper);
        funcGenPane.add(Box.createVerticalStrut(10));
        funcGenFrequencyWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
        funcGenPane.add(funcGenFrequencyWrapper);
    }

    /**
     * Create a label to be a control that will allow the user to hide or show this panel by
     * clicking on it.
     */
    private void createHideShowControl() {
        hideShowControl = new ClickLabel(
                "Digiscope Controls", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT);
        hideShowControl.setFont(Style.FONT_TEXT_HIGHLIGHT);
        hideShowControl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (panelIsShown) {
                    hidePanel();
                } else {
                    showPanel();
                }
            }
        });
    }

    /**
     * Hide the control pane, ie remove it from the panel.
     */
    private void hidePanel() {
        panelIsShown = !panelIsShown;
        panel.remove(controlPane);
        panel.updateUI();
    }

    /**
     * Show the control pane, ie add it to the panel.
     */
    private void showPanel() {
        panelIsShown = !panelIsShown;
        panel.add(controlPane);
        panel.updateUI();
    }

    /* ---------------------------------------------------------------------------------------
    ------------------------------------- SETTERS METHODS -------------------------------------
     --------------------------------------------------------------------------------------- */

    public void setDeviceStatus(String status) {
        deviceStatus.setText(status);
    }

    public void setSamplingRate(String rate) {
        samplingRate.setText(rate + " S/s");
    }

    public void setTriggerModeCB(int index) {
        setFromDevice = true;
        triggerModeCB.setSelectedIndex(index);
    }

    public void setTriggerTypeCB(int index) {
        setFromDevice = true;
        triggerTypeCB.setSelectedIndex(index);
    }

    public void setTriggerThresholdTF(String threshold) {
        setFromDevice = true;
        triggerThresholdTF.setText(threshold);
    }

    public void setFuncGenOutputCB(int index) {
        setFromDevice = true;
        funcGenOutputCB.setSelectedIndex(index);
    }

    public void setFuncGenWaveTypesCB(int index) {
        setFromDevice = true;
        funcGenWaveTypesCB.setSelectedIndex(index);
    }

    public void setFuncGenP2PTF(String p2p) {
        setFromDevice = true;
        funcGenP2PTF.setText(p2p);
    }

    public void setFuncGenOTF(String offset) {
        setFromDevice = true;
        funcGenOTF.setText(offset);
    }

    public void setFuncGenFrequencyTF(String frequency) {
        setFromDevice = true;
        funcGenFrequencyTF.setText(frequency);
    }

    public void setController(DigiscopePaneController controller) {
        this.controller = controller;
    }

    /* ---------------------------------------------------------------------------------------
    ------------------------------------- GETTERS METHODS -------------------------------------
     --------------------------------------------------------------------------------------- */

    public double getSamplingRate() {
        String rate = samplingRate.getText().split(" ")[0];
        return Double.valueOf(rate);
    }

    /**
     * @return The panel (contains all components of this view)
     */
    public JPanel getPanel() {
        return panel;
    }
}
