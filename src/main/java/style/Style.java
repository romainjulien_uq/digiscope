package style;

import java.awt.*;

/**
 * The style (ie font, color) to keep the color and font schemes consistent throughout the PanelS.
 */
public class Style {

    // Fonts used
    public static final Font FONT_TITLE_BOLD = new Font("Helvetica Neue", Font.BOLD, 18);
    public static final Font FONT_TITLE = new Font("Helvetica Neue", Font.PLAIN, 14);
    public static final Font FONT_TEXT_HIGHLIGHT = new Font("Helvetica Neue", Font.BOLD, 12);
    public static final Font FONT_TEXT = new Font("Helvetica Neue", Font.PLAIN, 12);
    public static final Font FONT_BIG_ICON = new Font("Helvetica Neue", Font.BOLD, 20);

    // Color used
    public static final Color COLOR_ERROR_RED = new Color(179, 34, 31, 255);
    public static final Color COLOR_DARK_ORANGE = new Color(179, 84, 0, 255);
    public static final Color COLOR_DARK_ORANGE_TRANSPARENT = new Color(179, 84, 0, 100);
    public static final Color COLOR_LIGHT_ORANGE = new Color(237, 174, 99, 255);
    public static final Color COLOR_DARK_GREY = new Color(56, 58, 59, 255);
    public static final Color COLOR_GREY = new Color(80, 81, 83, 255);
    public static final Color COLOR_LIGHT_GREY = new Color(203, 200, 196, 255);

    public static final Color COLOR_CHANNEL_A = new Color(203, 187, 50, 255);
    public static final Color COLOR_CHANNEL_B = new Color(100, 111, 203, 255);
    public static final Color COLOR_CHANNEL_MATH = new Color(87, 203, 89, 255);
    public static final Color COLOR_CHANNEL_FILTER = new Color(203, 76, 90, 255);

    // The computer window size
    private static Dimension effectiveScreenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().
            getMaximumWindowBounds().getSize();

    /**
     * The size of the application window is set to 1920x1080 unless the effective screen size is
     * smaller than this
     *
     * @return int The window width
     */
    public static int getWindowWidth() {
        return Math.min(1920, effectiveScreenSize.width);
    }

    /**
     * The size of the application window is set to 1920x1080 unless the effective screen size is
     * smaller than this
     *
     * @return int The window height
     */
    public static int getWindowHeight() {
        return Math.min(1080, effectiveScreenSize.height);
    }
}
