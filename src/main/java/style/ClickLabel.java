package style;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

/**
 * A ClickLabel is an extension of JLabel to be used as a button.
 */
public class ClickLabel extends JLabel implements MouseListener {

    // Color of the label when pressed
    private Color pressed;

    // Color of the label when released
    private Color released;


    /**
     * Get the text and the colors to use. Set a border if withBorder == true.
     *
     * @param text     The text of the label
     * @param released The color of the label when pressed
     * @param pressed  The color of the label when released
     */
    public ClickLabel(String text, Color released, Color pressed) {
        super(text);
        setForeground(released);

        this.pressed = pressed;
        this.released = released;

        addMouseListener(this);
    }


    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent event) {
        setForeground(this.pressed);
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        setForeground(this.released);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
