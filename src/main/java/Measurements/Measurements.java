package Measurements;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Controller.DigiscopePaneController;
import Settings.HorizontalDivision;
import style.Style;

/**
 * This class is responsible for updating and displaying the measurements in a table.
 *
 * Additionally, and for testing purposes only, this class layout a few test buttons.
 */
public class Measurements {

    // The application controller
    private DigiscopePaneController controller;

    // the main panel
    private JPanel measurementsPanel;

    // The table controller
    private DefaultTableModel tableModel;

    // The columns title
    private Object[] tableColumns = {"", "Minimum Voltage", "Maximum Voltage", "Max. P2P Voltage",
            "Average Voltage", "Standard Deviation", "Frequency"};

    private static Measurements ourInstance = new Measurements();

    public static Measurements getInstance() {
        return ourInstance;
    }

    private Measurements() {
        measurementsPanel = new JPanel();
        BoxLayout layout = new BoxLayout(measurementsPanel, BoxLayout.Y_AXIS);
        measurementsPanel.setLayout(layout);
        measurementsPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        measurementsPanel.setOpaque(false);

        addMeasurementsPanelComponents();
        addTestPanelComponents();
    }

    /**
     * Init and layout the measurement table.
     *
     * Row 1 is channel A, Row 2 is channel B, Row 3 is the Math Channel, Row 4 is the Filter
     * Channel.
     */
    private void addMeasurementsPanelComponents() {
        JTable table = new JTable();

        tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(tableColumns);
        table.setModel(tableModel);

        int canvasWidth = Style.getWindowWidth() - 320;
        table.setPreferredSize(new Dimension(canvasWidth, 150));

        table.setBackground(Style.COLOR_LIGHT_GREY);
        table.setForeground(Style.COLOR_DARK_GREY);
        table.setFont(Style.FONT_TEXT);
        table.setRowHeight(30);
        table.setShowVerticalLines(false);
        table.setGridColor(Style.COLOR_GREY);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        for (int i = 1; i < 7; i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
        }

        tableModel.addRow(tableColumns);
        Object[] row = {"Channel A", "-", "-", "-", "-", "-", "-"};
        tableModel.addRow(row);
        Object[] row2 = {"Channel B", "-", "-", "-", "-", "-", "-"};
        tableModel.addRow(row2);
        Object[] row3 = {"Channel Math", "-", "-", "-", "-", "-", "-"};
        tableModel.addRow(row3);
        Object[] row4 = {"Channel Filter", "-", "-", "-", "-", "-", "-"};
        tableModel.addRow(row4);

        measurementsPanel.add(Box.createVerticalStrut(10));
        table.setAlignmentX(Component.LEFT_ALIGNMENT);
        measurementsPanel.add(table);
    }

    /**
     * Update the measurement table.
     *
     * @param value  the new measurement.
     * @param row    the row to update.
     * @param column the column to update.
     */
    public void updateMeasurement(String value, int row, int column) {
        tableModel.setValueAt(value, row, column);
        measurementsPanel.updateUI();
    }

    /**
     * Clear a row in the table, ie set all columns to '-'.
     *
     * @param row The row to clear.
     */
    public void clearRow(int row) {
        for (int i = 1; i < 7; i++) {
            tableModel.setValueAt("-", row, i);
        }
    }

    /**
     * A reference to the controller instance.
     *
     * @param controller The application controller
     */
    public void setController(DigiscopePaneController controller) {
        this.controller = controller;
    }

    public JPanel getPanel() {
        return measurementsPanel;
    }

    /* ---------------------------------------------------------------------------------------
    --------------------------------- TESTS FOR DEMONSTRATION ---------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * FOR TESTING ONLY. Add a few buttons to the panel to simulate device messages.
     */
    private void addTestPanelComponents() {
        JButton testA = new JButton("Channel A");
        testA.setFont(Style.FONT_TEXT);
        testA.addActionListener(e -> testA());

        JButton testB = new JButton("Channel B");
        testB.setFont(Style.FONT_TEXT);
        testB.addActionListener(e -> testB());

        JButton testBandPass = new JButton("Bandpass");
        testBandPass.setFont(Style.FONT_TEXT);
        testBandPass.addActionListener(e -> testBandpass());

        JPanel testBox = new JPanel();
        BoxLayout layoutTestBox = new BoxLayout(testBox, BoxLayout.X_AXIS);
        testBox.setLayout(layoutTestBox);
        testBox.setOpaque(false);

        testBox.add(testA);
        testBox.add(Box.createHorizontalStrut(5));
        testBox.add(testB);
        testBox.add(Box.createHorizontalStrut(5));
        testBox.add(testBandPass);

        measurementsPanel.add(Box.createVerticalStrut(20));
        testBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        measurementsPanel.add(testBox);
    }

    /**
     * FOR TESTING ONLY. Generate 100 voltages for channel A, sampled over 1ms. Send it to the
     * controller as if it was received from the device.
     */
    private void testA() {
        ArrayList<Double> testValues = getTestValues();
        if (testValues == null) {
            return;
        }
        controller.receivedSamples(testValues, HorizontalDivision.ONE_MS, "A");
    }

    /**
     * FOR TESTING ONLY. Generate 100 voltages for channel B, sampled over 1ms. Send it to the
     * controller as if it was received from the device.
     */
    private void testB() {
        ArrayList<Double> testValues = getTestValues();
        if (testValues == null) {
            return;
        }
        controller.receivedSamples(testValues, HorizontalDivision.ONE_MS, "B");
    }

    /**
     * FOR TESTING ONLY. Generate 100 voltages following a pattern similar to a sine wave.
     *
     * @return A list of voltages.
     */
    private ArrayList<Double> getTestValues() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(null);
        File file = fileChooser.getSelectedFile();
        if (file == null) {
            return null;
        }

        ArrayList<Double> samples = new ArrayList<>();
        List<String> columns;
        String filePath = file.getPath();
        try {
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String line;
            // Read all lines in file
            while ((line = in.readLine()) != null) {
                columns = Arrays.asList(line.split(","));
                samples.add(Double.valueOf(columns.get(1)));
            }
        } catch (IOException e) {}

        return samples;
    }

    private void testBandpass() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(null);
        File file = fileChooser.getSelectedFile();
        if (file == null) {
            return;
        }

        ArrayList<Double> samples = new ArrayList<>();
        List<String> columns = new ArrayList<>();
        String filePath = file.getPath();
        try {
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String line;
            // Read all lines in file
            while ((line = in.readLine()) != null) {
                columns = Arrays.asList(line.split(","));
            }
        } catch (IOException e) {
        }

        try {
            for (int i = 0; i < columns.size(); i++) {
                samples.add(Double.valueOf(columns.get(i)));
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Cant convert number");
        }

        controller.receivedSamples(samples, HorizontalDivision.ONE_MS, "A");
    }
}
