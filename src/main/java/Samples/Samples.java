package Samples;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Oscilloscope.CanvasControls;
import Oscilloscope.FilterChannel;
import Settings.HorizontalDivision;

/**
 * Represents a set of samples.
 */
public class Samples {

    private final ArrayList<Double> BANDPASS_FILTER_COEFFS_BEN = new ArrayList<>(Arrays.asList(
            -0.000050749593363,
            -0.000562650924745492,
            0.000399652456943076,
            0.0059277709533,
            -0.0115872590700971,
            0.0060921237837,
            0.0169738712031866,
            -0.040268955078377,
            0.0315168088499331,
            0.0211843653615942,
            -0.0802840832236953,
            0.081397054472582,
            0.000021498774771,
            -0.10579599135201,
            0.135983378372859,
            -0.047055870348,
            -0.0929731177705378,
            0.160157068700955,
            -0.0929731177705378,
            -0.0470558703475113,
            0.135983378372859,
            -0.10579599135201,
            0.000021498774771,
            0.081397054472582,
            -0.0802840832236953,
            0.021184365362,
            0.0315168088499331,
            -0.040268955078377,
            0.0169738712031866,
            0.00609212378367769,
            -0.0115872590700971,
            0.00592777095330625,
            0.000399652456943076,
            -0.000562650924745492,
            -0.000050749593363
    ));

    private final ArrayList<Double> BANDPASS_FILTER_COEFFS_30 = new ArrayList<>(Arrays.asList(
            -0.000024,
            0.000296,
            0.001004,
            0.002019,
            0.003022,
            0.003502,
            0.002902,
            0.000857,
            -0.002587,
            -0.006837,
            -0.010823,
            -0.013251,
            -0.013009,
            -0.009573,
            -0.003289,
            0.004623,
            0.012342,
            0.017951,
            0.020000,
            0.017951,
            0.012342,
            0.004623,
            -0.003289,
            -0.009573,
            -0.013009,
            -0.013251,
            -0.010823,
            -0.006837,
            -0.002587,
            0.000857,
            0.002902,
            0.003502,
            0.003022,
            0.002019,
            0.001004,
            0.000296,
            -0.000024
    ));

    private final ArrayList<Double> BANDPASS_FILTER_COEFFS = new ArrayList<>(Arrays.asList(
            0.000095,
            -0.000056,
            -0.000331,
            -0.000688,
            -0.001036,
            -0.001243,
            -0.001169,
            -0.000713,
            0.000145,
            0.001309,
            0.002559,
            0.003582,
            0.004038,
            0.003642,
            0.002265,
            0.000000,
            -0.002811,
            -0.005614,
            -0.007743,
            -0.008569,
            -0.007661,
            -0.004927,
            -0.000690,
            0.004328,
            0.009128,
            0.012627,
            0.013907,
            0.012443,
            0.008269,
            0.002017,
            -0.005168,
            -0.011848,
            -0.016592,
            -0.018296,
            -0.016442,
            -0.011246,
            -0.003643,
            0.004895,
            0.012659,
            0.018065,
            0.020000,
            0.018065,
            0.012659,
            0.004895,
            -0.003643,
            -0.011246,
            -0.016442,
            -0.018296,
            -0.016592,
            -0.011848,
            -0.005168,
            0.002017,
            0.008269,
            0.012443,
            0.013907,
            0.012627,
            0.009128,
            0.004328,
            -0.000690,
            -0.004927,
            -0.007661,
            -0.008569,
            -0.007743,
            -0.005614,
            -0.002811,
            0.000000,
            0.002265,
            0.003642,
            0.004038,
            0.003582,
            0.002559,
            0.001309,
            0.000145,
            -0.000713,
            -0.001169,
            -0.001243,
            -0.001036,
            -0.000688,
            -0.000331,
            -0.000056,
            0.000095
    ));

    private ArrayList<Double> time;
    private ArrayList<Double> voltage;
    private int triggerIndex;
    private int timeDivision = 1000 * 10;

    /**
     * Compute an array of time values (in ms) corresponding to each samples to be used to plot a
     * wave of voltages vs time.
     *
     * @param valuesReceived  Voltage values
     * @param timeDivisionEnum  The horizontal division
     */
    public Samples(ArrayList<Double> valuesReceived, HorizontalDivision timeDivisionEnum) {
        timeDivision = timeDivisionEnum.getValue() * 10;

        ArrayList<Double> values;
        if (CanvasControls.getInstance().isBandpassFilterActive()) {
            ArrayList<Double> temp = new ArrayList<>();
            for (int i = 0; i < valuesReceived.size(); i++) {
                temp.add(valuesReceived.get(i) * (Math.sin(2* Math.PI * 1000000 * i / 10000000)));

                if (i != (valuesReceived.size() - 1)) {
                    double start = valuesReceived.get(i);
                    double end = valuesReceived.get(i + 1);
                    double interval = (end - start)/10.0;
                    for (int j = 1; j < 10; j++) {
                        double newVal = start + (interval * j);
                        temp.add(newVal * (Math.sin(2* Math.PI * 1000000 * i / 10000000)));
                    }
                }
            }
            FilterChannel filter = new FilterChannel();
            values = filter.getFIRFilter(temp, BANDPASS_FILTER_COEFFS);

        } else {
            values = valuesReceived;
        }

        double size = values.size();
        triggerIndex = ((int) size) / 2;
        double interval = ((double) this.timeDivision) / size;

        time = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            time.add(i * interval);
        }
        voltage = values;
    }

    /* ---------------------------------------------------------------------------------------
    ---------------------------------- GETTERS -----------------------------------------------
    --------------------------------------------------------------------------------------- */

    public ArrayList<Double> getTime() {
        return time;
    }

    public ArrayList<Double> getVoltage() {
        return voltage;
    }

    public int getTimeFrame() {
        return timeDivision;
    }

    public int getTriggerIndex() {
        return triggerIndex;
    }
}
