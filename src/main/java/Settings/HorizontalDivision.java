package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possible horizontal divisions.
 * Values are in micro seconds.
 */
public enum HorizontalDivision {
    ONE_US(1),
    TWO_US(2),
    FIVE_US(5),
    TEN_US(10),
    TWENTY_US(20),
    FIFTY_US(50),
    ONE_HUNDRED_US(100),
    TWO_HUNDRED_US(200),
    FIVE_HUNDRED_US(500),
    ONE_MS(1000),
    TWO_MS(2000),
    FIVE_MS(5000),
    TEN_MS(10000),
    TWENTY_MS(20000),
    FIFTY_MS(50000),
    ONE_HUNDRED_MS(100000),
    TWO_HUNDRED_MS(200000),
    FIVE_HUNDRED_MS(500000),
    ONE_S(1000000);

    private int value;
    private static List<HorizontalDivision> allValues = Arrays.asList(HorizontalDivision.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The integer associated to this enum
     */
    HorizontalDivision(int value) {
        this.value = value;
    }

    /**
     * @return The integer representation of the enum
     */
    public int getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<Integer> intValues = new ArrayList<>();
        for (HorizontalDivision division : allValues) {
            intValues.add(division.getValue());
        }
        return intValues.toArray();
    }
}
