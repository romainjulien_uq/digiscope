package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possible vertical divisions.
 * Values are in milli volts.
 */
public enum VerticalDivision {
    TWENTY_MV(20),
    FIFTY_MV(50),
    ONE_HUNDRED_MV(100),
    TWO_HUNDRED_MV(200),
    FIVE_HUNDRED_MV(500),
    ONE_V(1000);

    private int value;
    private static List<VerticalDivision> allValues = Arrays.asList(VerticalDivision.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The integer associated to this enum
     */
    VerticalDivision(int value) {
        this.value = value;
    }

    /**
     * @return The integer representation of the enum
     */
    public int getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<Integer> intValues = new ArrayList<>();
        for (VerticalDivision division : allValues) {
            intValues.add(division.getValue());
        }
        return intValues.toArray();
    }
}
