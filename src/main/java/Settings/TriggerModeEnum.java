package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The possible trigger modes.
 */
public enum TriggerModeEnum {
    AUTO("Auto"),
    NORMAL("Normal"),
    SINGLE("Single");

    private String value;
    private static List<TriggerModeEnum> allValues = Arrays.asList(TriggerModeEnum.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    TriggerModeEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<String> stringValues = new ArrayList<>();
        for (TriggerModeEnum mode : allValues) {
            stringValues.add(mode.getValue());
        }
        return stringValues.toArray();
    }
}
