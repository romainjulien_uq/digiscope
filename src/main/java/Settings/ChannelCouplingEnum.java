package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possibles couplings.
 */
public enum ChannelCouplingEnum {
    AC("AC"),
    DC("DC");

    private String value;
    private static List<ChannelCouplingEnum> allValues = Arrays.asList(ChannelCouplingEnum.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    ChannelCouplingEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<String> stringValues = new ArrayList<>();
        for (ChannelCouplingEnum coupling : allValues) {
            stringValues.add(coupling.getValue());
        }
        return stringValues.toArray();
    }
}
