package Settings;

/**
 * Created by romainjulien on 23/04/2016.
 */
public enum SettingsEnum {
    DEVICE_STATUS("Device Status"),
    SAMPLING_RATES("Sampling Rate"),
    CHANNEL_OFFSET("Channel Offset"),
    VERTICAL_RANGE("Vertical Range"),
    HORIZONTAL_RANGE("Horizontal Range"),
    TRIGGER_MODE("Trigger Mode"),
    TRIGGER_TYPE("Trigger Type"),
    TRIGGER_THRESHOLD("Trigger Threshold"),
    FORCE_TRIGGER("Force"),
    REARM_TRIGGER("Rearm"),
    NUMBER_OF_SAMPLES("Number Of Samples"),
    FUNCTION_GENERATOR_OUTPUT("Function Generator Output"),
    FUNCTION_GENERATOR_WAVE_TYPE("Function Generator Wave Type"),
    FUNCTION_GENERATOR_PEAK_TO_PEAK_VOLTAGE("Function Generator Peak-To-Peak Voltage"),
    FUNCTION_GENERATOR_OFFSET("Function Generator Offset"),
    FUNCTION_GENERATOR_FREQUENCY("Function Generator Frequency"),
    BIT_MODE("Bit Mode"),
    CHANNEL_COUPLING("Channel Coupling");

    private String value;

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    SettingsEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }
}
