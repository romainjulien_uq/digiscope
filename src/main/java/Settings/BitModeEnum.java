package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possibles bit modes.
 */
public enum BitModeEnum {
    BIT8("8 Bit"),
    BIT12("12 bit");

    private String value;
    private static List<BitModeEnum> allValues = Arrays.asList(BitModeEnum.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    BitModeEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<String> stringValues = new ArrayList<>();
        for (BitModeEnum bitMode : allValues) {
            stringValues.add(bitMode.getValue());
        }
        return stringValues.toArray();
    }
}
