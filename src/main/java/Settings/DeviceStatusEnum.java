package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possible device status.
 */
public enum DeviceStatusEnum {
    ARMED("Armed"),
    TRIGGERED("Triggered"),
    STOPPED("Stopped");

    private String value;

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    DeviceStatusEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }
}
