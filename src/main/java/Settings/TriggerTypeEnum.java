package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The possible trigger types.
 */
public enum TriggerTypeEnum {
    RISING("Rising"),
    FALLING("Falling"),
    LEVEL("Level");

    private String value;
    private static List<TriggerTypeEnum> allValues = Arrays.asList(TriggerTypeEnum.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    TriggerTypeEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<String> stringValues = new ArrayList<>();
        for (TriggerTypeEnum type : allValues) {
            stringValues.add(type.getValue());
        }
        return stringValues.toArray();
    }
}
