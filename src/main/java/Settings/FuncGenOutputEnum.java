package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possible function generator outputs.
 */
public enum FuncGenOutputEnum {
    OFF("Off"),
    ON("On");

    private String value;
    private static List<FuncGenOutputEnum> allValues = Arrays.asList(FuncGenOutputEnum.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    FuncGenOutputEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<String> stringValues = new ArrayList<>();
        for (FuncGenOutputEnum output : allValues) {
            stringValues.add(output.getValue());
        }
        return stringValues.toArray();
    }
}
