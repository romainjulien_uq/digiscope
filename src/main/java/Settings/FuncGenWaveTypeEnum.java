package Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Possible wave types for the function generator.
 */
public enum FuncGenWaveTypeEnum {
    SINE("Sine"),
    SQUARE("Square"),
    TRIANGLE("Triangle"),
    RAMP("Ramp"),
    NOISE("Noise");

    private String value;
    private static List<FuncGenWaveTypeEnum> allValues = Arrays.asList(FuncGenWaveTypeEnum.values());

    /**
     * The constructor of the enum.
     *
     * @param value: The String associated to this enum
     */
    FuncGenWaveTypeEnum(String value) {
        this.value = value;
    }

    /**
     * @return The string representation of the enum
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return An array containing all possible enum values
     */
    public static Object[] getAllValues() {
        ArrayList<String> stringValues = new ArrayList<>();
        for (FuncGenWaveTypeEnum type : allValues) {
            stringValues.add(type.getValue());
        }
        return stringValues.toArray();
    }
}
