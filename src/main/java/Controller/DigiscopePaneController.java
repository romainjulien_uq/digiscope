package Controller;

import java.awt.*;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import Connection.ConnectionHandler;
import Measurements.Measurements;
import Oscilloscope.CanvasControls;
import Oscilloscope.OscilloscopeCanvas;
import Pannels.BottomPane;
import Samples.Samples;
import Settings.DeviceStatusEnum;
import Settings.HorizontalDivision;
import Settings.SettingsEnum;
import style.Style;

/**
 * The application controller. Pass and manage data to the different classes of the software.
 */
public class DigiscopePaneController {

    // The application main container view
    private JPanel appContainer;

    // The oscilloscope canvas
    private OscilloscopeCanvas canvas;

    public DigiscopePaneController(Container container) {
        container.setBackground(Style.COLOR_DARK_GREY);

        appContainer = new JPanel(new BorderLayout());
        appContainer.setOpaque(false);
        appContainer.setBorder(new EmptyBorder(0, 20, 0, 20));
        container.add(appContainer, BorderLayout.CENTER);

        initGUI();

        // Pass references to the controller
        ConnectionHandler.getInstance().setController(this);
        CanvasControls.getInstance().setController(this);
        BottomPane.getInstance().setController(this);
        Measurements.getInstance().setController(this);
    }

    /**
     * Init the GUI.
     *
     * The top pane contains the canvas to plot the channel and some controls related to the channel
     * display.
     *
     * The bottom pane contains command settings for the device.
     */
    private void initGUI() {
        canvas = new OscilloscopeCanvas();

        JPanel topPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        topPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        topPane.setBackground(Style.COLOR_LIGHT_GREY);

        topPane.add(canvas);
        topPane.add(CanvasControls.getInstance().getControlPanel());
        topPane.add(Measurements.getInstance().getPanel());

        appContainer.add(topPane, BorderLayout.CENTER);
        appContainer.add(BottomPane.getInstance().getPanel(), BorderLayout.SOUTH);
    }

    /**
     * Method called when a channel setting has changed and the channels need to be plotted again.
     * eg: change horizontal division
     */
    public void channelSettingsChanged() {
        canvas.redrawCanvas();
    }


    /* ---------------------------------------------------------------------------------------
    ---------------------------- COMMUNICATION WITH DEVICE METHODS --------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Method called when the input reader received a setting message. Update the new setting.
     *
     * @param settingMessage the setting and its new value.
     */
    public void receivedSettingMessage(int[] settingMessage) {
        SettingsEnum command = SettingsEnum.values()[settingMessage[0]];
        int value = settingMessage[1];

        System.out.println("Command: " + command.getValue() + ", value: " + value);
        switch (command) {
            case DEVICE_STATUS:
                BottomPane.getInstance().setDeviceStatus(DeviceStatusEnum.values()[value].getValue());
                break;
            case SAMPLING_RATES:
                BottomPane.getInstance().setSamplingRate(String.valueOf(value));
                break;
            case VERTICAL_RANGE:
                CanvasControls.getInstance().setVerticalDivision(value);
                break;
            case HORIZONTAL_RANGE:
                CanvasControls.getInstance().setHorizontalDivision(value);
                break;
            case TRIGGER_MODE:
                BottomPane.getInstance().setTriggerModeCB(value);
                break;
            case TRIGGER_TYPE:
                BottomPane.getInstance().setTriggerTypeCB(value);
                break;
            case TRIGGER_THRESHOLD: //0-4095 -> -5 to 5V
                BottomPane.getInstance().setTriggerThresholdTF(String.valueOf(value));
                break;
            case FUNCTION_GENERATOR_OUTPUT:
                BottomPane.getInstance().setFuncGenOutputCB(value);
                break;
            case FUNCTION_GENERATOR_WAVE_TYPE:
                BottomPane.getInstance().setFuncGenWaveTypesCB(value);
                break;
            case FUNCTION_GENERATOR_PEAK_TO_PEAK_VOLTAGE:
                BottomPane.getInstance().setFuncGenP2PTF(String.valueOf(value));
                break;
            case FUNCTION_GENERATOR_OFFSET:
                BottomPane.getInstance().setFuncGenOTF(String.valueOf(value));
                break;
            case FUNCTION_GENERATOR_FREQUENCY:
                BottomPane.getInstance().setFuncGenFrequencyTF(String.valueOf(value));
                break;
            case CHANNEL_COUPLING:
                CanvasControls.getInstance().setChannelCouplingCB(value);
                break;
            default:
                break;
        }
    }

    /**
     * Method called when the input reader received a new set of samples. Pass the samples to the
     * Oscilloscope canvas.
     *
     * @param values    The voltages values
     * @param timeDivision The time it took to acquire this set of samples
     * @param channel   The channel corresponding to the samples
     */
    public void receivedSamples(ArrayList<Double> values, HorizontalDivision timeDivision, String channel) {
        Samples samples = new Samples(values, timeDivision);
        canvas.setSamples(samples, channel);
    }

    /**
     * Method called when a setting has changed and needs to be sent to the device. Prepare a byte
     * array to send to the device. The byte array contain: - An index representing the setting
     * changed. - The new value for the setting.
     *
     * @param command The command index as listed in the SettingsEnum class.
     * @param value   The value for the setting.
     */
    public void sendToDevice(short command, int value) {
        ByteBuffer msgBuffer = ByteBuffer.allocate(6);
        msgBuffer.putShort(command);
        msgBuffer.putInt(value);
        byte[] toSend = msgBuffer.array();
        ConnectionHandler.getInstance().send(toSend);
    }

    /* ---------------------------------------------------------------------------------------
    ---------------------------------- MATH CHANNEL METHODS ----------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Method called when a new equation was input for the math channel. Pass the equation to the
     * oscilloscope canvas.
     *
     * @param equation The equation in the form of an array of strings.
     * @return A boolean indicating if the math channel can be selected as input for the filter
     * channel
     */
    public boolean setEquation(ArrayList<String> equation) {
        return canvas.setEquation(equation);
    }


    /* ---------------------------------------------------------------------------------------
    ----------------------------------FILTER CHANNEL METHODS ----------------------------------
     --------------------------------------------------------------------------------------- */

    /**
     * Method called when a file was selected for the filter channel. Send the file to the canvas to
     * be processed.
     *
     * @param file The selected filter file
     * @return The type of filter, IIR or FIR
     */
    public String filterFileSelected(File file) {
        if (file == null) {
            return null;
        }
        String type = canvas.filterFileSelected(file);
        if (type == null) {
            JOptionPane.showMessageDialog(appContainer, "Invalid File Format");
            return "No file selected";
        } else {
            return type;
        }
    }

    /**
     * Method called when the input for the filter channel has changed.
     *
     * @param inputChannel The new input channel for the filter channel
     */
    public void setInputFilterChannel(String inputChannel) {
        canvas.setInputFilterChannel(inputChannel);
    }

}
