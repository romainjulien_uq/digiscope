import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import Controller.DigiscopePaneController;
import style.Style;

/**
 * ENGG4801 2016 - By Romain JULIEN - Team 34
 */
public class Digiscope {

    /**
     * The entry point of the application.
     */
    private Digiscope() {

        JFrame main = new JFrame();
        // Set title
        main.setTitle("Digiscope");
        // Set size of the application window
        main.setSize(Style.getWindowWidth(), Style.getWindowHeight());

        // Initiate the container and it's children views
        new DigiscopePaneController(main.getContentPane());

        // Clean up when window closes
        main.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });

        main.setVisible(true);
    }

    public static void main(String[] args) {
        new Digiscope();
    }
}
